﻿using System;
using System.Collections.Generic;

namespace ExadelPractice.DTOs
{
    public class ReportDTO
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Patronymic { get; set; }
        public string City { get; set; }
        public string Profession { get; set; }
        public string Status { get; set; }
        public short Experience { get; set; }
        public short EnglishLevel { get; set; }
        public string LinkedIn { get; set; }
        public string SkypeId { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime LastContact { get; set; }
        public string HR { get; set; }
        public string PrimaryEmail { get; set; }
        public ICollection<string> Skills { get; set; }
    }
}