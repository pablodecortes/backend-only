﻿using System;

namespace ExadelPractice.DTOs
{
    public class PositionSkillDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid? ParentId { get; set; }
        public int Level { get; set; }
        public bool IsPrimary { get; set; }
    }
}