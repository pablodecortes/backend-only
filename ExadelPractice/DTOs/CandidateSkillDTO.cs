﻿using System;

namespace ExadelPractice.DTOs
{
    public class CandidateSkillDTO
    {
        public double Experience { get; set; }
        public bool IsPrimary { get; set; }
        public int? LastUsage{ get; set; }
        public int Rating { get; set; }
        public Guid Id { get; set; }
        public bool WasConfirmed { get; set; }
        public string Name { get; set; }

        public bool Equals(CandidateSkillDTO skill)
        {
            return
                skill.Experience == Experience &&
                skill.IsPrimary == IsPrimary &&
                skill.LastUsage == LastUsage &&
                skill.Rating == Rating &&
                skill.Id.Equals(Id) &&
                skill.WasConfirmed == WasConfirmed &&
                skill.Name.Equals(Name);
        }
    }
}