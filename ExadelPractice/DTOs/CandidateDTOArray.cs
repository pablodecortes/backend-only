﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExadelPractice.DTOs
{
    public class CandidateDTOArray
    {
        public ICollection<ReducedCandidateDTO> Candidates { get; set; }
        public long Amount { get; set; }

        public CandidateDTOArray(IList<ReducedCandidateDTO> list)
        {
            Candidates = list;
            Amount = list.Count;
        }

        public CandidateDTOArray() : this(new List<ReducedCandidateDTO>()) { }
    }
}