﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ExadelPractice.DTOs;
using ExadelPractice.Models;

namespace ExadelPractice.DTOs
{
    public class CandidateDTO
    {
        public Guid Id { get; set; }

        public string NameRus { get; set; }
        public string SurnameRus { get; set; }
        public string PatronymicRus { get; set; }
        public string NameEng { get; set; }
        public string SurnameEng { get; set; }
        public ICollection<CandidateSkillDTO> Skills { get; set; }
        public ICollection<EmailDTO> Email { get; set; }
        public string LinkedIn { get; set; }
        public string SkypeId { get; set; }
        public string PhoneNumber { get; set; }
        public ProfessionDTO Profession { get; set; }
        public CandidateCityDTO City { get; set; }
        public double Experience { get; set; }
        public decimal Salary { get; set; }
        public DateTime? LastContact { get; set; }
        public DateTime? NextContact { get; set; }
        public string HR { get; set; }
        public short EnglishLevel { get; set; }
        public ICollection<PositionDTO> Positions { get; set; }
        public ICollection<InterviewDTO> Interviews { get; set; }
        public string Description { get; set; }
        public CandidateStatusDTO Status { get; set; }
    }
}