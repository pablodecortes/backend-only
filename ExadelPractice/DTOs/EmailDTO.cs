﻿using System;

namespace ExadelPractice.DTOs
{
    public class EmailDTO
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public bool IsPrimary { get; set; }

        public bool Equals(EmailDTO email)
        {
            return
                Email == email.Email &&
                IsPrimary == email.IsPrimary;
        }
    }
}