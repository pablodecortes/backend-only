﻿using System;

namespace ExadelPractice.DTOs
{
    public class PositionProfessionDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}