﻿using System;
using System.Collections.Generic;
using ExadelPractice.Models;

namespace ExadelPractice.DTOs
{
    public class ReducedPositionDTO
    {
        public Guid Id { get; set; }
        public string Header { get; set; }
        public string ProjectName { get; set; }
        public DateTime? RequestDate { get; set; }
        public string Description { get; set; }
        public short Experience { get; set; }
        public short EnglishLevel { get; set; }
        public PositionCityDTO City { get; set; }
        public PositionStatusDTO Status { get; set; }
        public PositionProfessionDTO Profession { get; set; }
        public ICollection<PositionSkillDTO> Skills { get; set; }
    }
}