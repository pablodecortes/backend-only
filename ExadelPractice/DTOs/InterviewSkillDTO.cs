﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExadelPractice.DTOs
{
    public class InterviewSkillDTO
    {
        public Guid SkillId { get; set; }
        public string SkillName { get; set; }
        public int DefultRating { get; set; }
        public int ConfRating { get; set; }
        public double Experience { get; set; }
        public bool IsPrimary { get; set; }
        public bool WasConfirmed { get; set; }
    }
}