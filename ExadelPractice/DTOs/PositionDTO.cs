﻿using System;
using System.Collections.Generic;
using ExadelPractice.DTOs;

namespace ExadelPractice.DTOs
{
    public class PositionDTO
    {
        public Guid Id { get; set; }
        public string Header { get; set; }
        public DateTime? ProjectDate { get; set; }
        public DateTime? RequestDate { get; set; }
        public string Description { get; set; }
        public short Experience { get; set; }
        public PositionCityDTO City { get; set; }
        public PositionStatusDTO Status { get; set; }

        public PositionProfessionDTO Profession { get; set; }
        public PositionProjectDTO Project { get; set; }
        public ICollection<PositionSkillDTO> Skills { get; set; }
        public short EnglishLevel { get; set; }
        public ICollection<CandidatePositionDTO> CandidatePositions { get; set; }
        public ICollection<PositionInterviewDTO> PositionInterviews { get; set; }
    }
}