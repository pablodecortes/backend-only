﻿using System;

namespace ExadelPractice.DTOs
{
    public class PositionInterviewDTO
    {
        public Guid Id { get; set; }
        public Guid PositionId { get; set; }
    }
}