﻿using System;

namespace ExadelPractice.DTOs
{
    public class PositionProjectDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}