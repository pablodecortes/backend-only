﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExadelPractice.DTOs
{
    public class CandidateInterviewDTO
    {
        public Guid Id { get; set; }
        public string NameRus { get; set; }
        public string SurnameRus { get; set; }
        public string PatronymicRus { get; set; }
        public string NameEng { get; set; }
        public string SurnameEng { get; set; }
    }
}