﻿using System;

namespace ExadelPractice.DTOs
{
    public class ProfessionDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public bool Equals(ProfessionDTO profession)
        {
            return
                Name == profession.Name &&
                Id == profession.Id;
        }
    }
}