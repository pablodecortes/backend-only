﻿using System;
using System.Collections.Generic;

namespace ExadelPractice.DTOs
{
    public class InterviewSkillDTOArray
    {
        public Guid InterviewId { get; set; }
        public ICollection<CandidateSkillDTO> Skills { get; set; }
    }
}