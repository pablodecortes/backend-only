﻿using System.Collections.Generic;

namespace ExadelPractice.DTOs
{
    public class PositionDTOArray
    {
        public ICollection<ReducedPositionDTO> Positions { get; set; }
        public long Amount { get; set; }
    }
}