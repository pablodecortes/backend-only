﻿using System;

namespace ExadelPractice.DTOs
{
    public class CandidateStatusDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
    }
}