﻿using System;

namespace ExadelPractice.DTOs
{
    public class CandidateCityDTO
    {
        public string Name { get; set; }
        public Guid Id { get; set; }
        public Guid CandidateCityId { get; set; }

        public bool Equals(CandidateCityDTO city)
        {
            return
                city.Id == Id &&
                city.Name == Name;
        }
    }
}