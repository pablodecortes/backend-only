﻿using System;

namespace ExadelPractice.DTOs
{
    public class PositionCityDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}