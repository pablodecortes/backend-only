﻿using System.ComponentModel.DataAnnotations;

namespace ExadelPractice.DTOs
{
    public class UserDTO
    {
        [Required]
        public string Id { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Surname { get; set; }

        [Required]
        public string AccessLevel { get; set; }
    }
}