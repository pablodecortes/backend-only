﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ExadelPractice.Models;

namespace ExadelPractice.DTOs
{
    public class ReducedCandidateDTO
    {
        public Guid Id { get; set; }
        public string NameEng { get; set; }
        public string SurnameEng { get; set; }
        public CandidateStatusDTO Status { get; set; }
        public DateTime LastContact { get; set; }
        public ProfessionDTO Profession { get; set; }
        public double Experience { get; set; }
        public CandidateCityDTO City { get; set; }
        public List<CandidateSkillDTO> Skills { get; set; }
    }
}