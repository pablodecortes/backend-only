﻿using System;

namespace ExadelPractice.DTOs
{
    public class CandidatePositionDTO
    {
        public Guid CandidateId { get; set; }
        public Guid PositionId { get; set; }
    }
}