﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExadelPractice.DTOs
{
    public class CandidateNote
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
    }
}