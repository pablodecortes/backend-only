﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.IO;
using ExadelPractice.Models;
using Newtonsoft.Json;

namespace ExadelPractice.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<ExadelPracticeContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(ExadelPracticeContext context)
        {
            InitSkills(context);
            InitCities(context);
            InitProfessions(context);
            InitPosisionStatuses(context);
            InitCandidateStatuses(context);
            InitProjests(context);

            context.SaveChanges();
        }

        private static void InitSkills(ExadelPracticeContext context)
        {
            List<Skill> items;
            const string path = @"C:\Users\Pavel\Documents\Visual Studio 2017\Projects\backend-only\ExadelPractice\App_Data\skillsArchive.json";

            using (var r = new StreamReader(path))
            {
                var json = r.ReadToEnd();
                items = JsonConvert.DeserializeObject<List<Skill>>(json);
            }

            context.Skills.AddOrUpdate(items.ToArray());
        }

        private static void InitCities(ExadelPracticeContext context)
        {
            var cities = new[]
            {
                new City
                {
                    Id = Guid.NewGuid(),
                    Name = "Walnut Creek"
                },
                new City
                {
                    Id = Guid.NewGuid(),
                    Name = "Boulder"
                },
                new City
                {
                    Id = Guid.NewGuid(),
                    Name = "Gomel"
                },
                new City
                {
                    Id = Guid.NewGuid(),
                    Name = "Grodno"
                },
                new City
                {
                    Id = Guid.NewGuid(),
                    Name = "Minsk"
                },
                new City
                {
                    Id = Guid.NewGuid(),
                    Name = "Vitebsk"
                },
                new City
                {
                    Id = Guid.NewGuid(),
                    Name = "Klaipėda"
                },
                new City
                {
                    Id = Guid.NewGuid(),
                    Name = "Vilnius"
                },
                new City
                {
                    Id = Guid.NewGuid(),
                    Name = "Bialystock"
                },
                new City
                {
                    Id = Guid.NewGuid(),
                    Name = "Szczecin"
                },
                new City
                {
                    Id = Guid.NewGuid(),
                    Name = "Warsaw"
                },
                new City
                {
                    Id = Guid.NewGuid(),
                    Name = "Chelyabinsk"
                },
                new City
                {
                    Id = Guid.NewGuid(),
                    Name = "Yekaterinburg"
                },
                new City
                {
                    Id = Guid.NewGuid(),
                    Name = "Kharkov"
                },
                new City
                {
                    Id = Guid.NewGuid(),
                    Name = "Vinnytsia"
                }
            };

            context.Cities.AddOrUpdate(city => city.Name, cities);
        }

        private static void InitProfessions(ExadelPracticeContext context)

        {
            var professions = new[]
            {
                new Profession
                {
                    Id = Guid.NewGuid(),
                    Name = "HR"
                },
                new Profession
                {
                    Id = Guid.NewGuid(),
                    Name = "PM"
                },
                new Profession
                {
                    Id = Guid.NewGuid(),
                    Name = "Junior QA"
                },
                new Profession
                {
                    Id = Guid.NewGuid(),
                    Name = "Middle QA"
                },
                new Profession
                {
                    Id = Guid.NewGuid(),
                    Name = "Senior QA"
                },
                new Profession
                {
                    Id = Guid.NewGuid(),
                    Name = "Junior SE"
                },
                new Profession
                {
                    Id = Guid.NewGuid(),
                    Name = "Middle SE"
                },
                new Profession
                {
                    Id = Guid.NewGuid(),
                    Name = "Senior SE"
                }
            };

            context.Professions.AddOrUpdate(p => p.Name, professions);
        }

        private static void InitPosisionStatuses(ExadelPracticeContext context)
        {
            var pStatuses = new[]
            {
                new PositionStatus
                {
                    Id = Guid.NewGuid(),
                    Name = "On hold",
                    IsActive = true
                },
                new PositionStatus
                {
                    Id = Guid.NewGuid(),
                    Name = "Active",
                    IsActive = true
                },
                new PositionStatus
                {
                    Id = Guid.NewGuid(),
                    Name = "CV provided",
                    IsActive = true
                },
                new PositionStatus
                {
                    Id = Guid.NewGuid(),
                    Name = "Waiting for interview with customer",
                    IsActive = true
                },
                new PositionStatus
                {
                    Id = Guid.NewGuid(),
                    Name = "Interview with customer",
                    IsActive = true
                },
                new PositionStatus
                {
                    Id = Guid.NewGuid(),
                    Name = "Candidate declined",
                    IsActive = false
                },
                new PositionStatus
                {
                    Id = Guid.NewGuid(),
                    Name = "Candidate approved",
                    IsActive = false
                },
                new PositionStatus
                {
                    Id = Guid.NewGuid(),
                    Name = "Closed",
                    IsActive = false
                },
                new PositionStatus
                {
                    Id = Guid.NewGuid(),
                    Name = "Cancelled",
                    IsActive = false
                }
            };

            context.PositionStatus.AddOrUpdate(p => p.Name, pStatuses);
        }

        private static void InitCandidateStatuses(ExadelPracticeContext context)
        {
            var cStatuses = new[]
            {
                new CandidateStatus
                {
                    Id = Guid.NewGuid(),
                    Name = "Pool",
                    IsActive = true
                },
                new CandidateStatus
                {
                    Id = Guid.NewGuid(),
                    Name = "In progress",
                    IsActive = true
                },
                new CandidateStatus
                {
                    Id = Guid.NewGuid(),
                    Name = "On hold",
                    IsActive = true
                },
                new CandidateStatus
                {
                    Id = Guid.NewGuid(),
                    Name = "Rejected",
                    IsActive = false
                },
                new CandidateStatus
                {
                    Id = Guid.NewGuid(),
                    Name = "Interview",
                    IsActive = true
                },
                new CandidateStatus
                {
                    Id = Guid.NewGuid(),
                    Name = "Interview with customer",
                    IsActive = true
                },
                new CandidateStatus
                {
                    Id = Guid.NewGuid(),
                    Name = "Job offer",
                    IsActive = true
                },
                new CandidateStatus
                {
                    Id = Guid.NewGuid(),
                    Name = "Job offer rejected",
                    IsActive = false
                },
                new CandidateStatus
                {
                    Id = Guid.NewGuid(),
                    Name = "Job offer accepted",
                    IsActive = false
                },
                new CandidateStatus
                {
                    Id = Guid.NewGuid(),
                    Name = "Hired",
                    IsActive = false
                },
                new CandidateStatus
                {
                    Id = Guid.NewGuid(),
                    Name = "Attention",
                    IsActive = true
                },
            };

            context.CandidateStatus.AddOrUpdate(p => p.Name, cStatuses);
        }

        private static void InitProjests(ExadelPracticeContext context)
        {
            var projects = new[]
            {
                new Project
                {
                    Id = Guid.NewGuid(),
                    Name = "Project1",
                },
                new Project
                {
                    Id = Guid.NewGuid(),
                    Name = "Project2",
                },
                new Project
                {
                    Id = Guid.NewGuid(),
                    Name = "Project3",
                },
                new Project
                {
                    Id = Guid.NewGuid(),
                    Name = "Project4",
                },
                new Project
                {
                    Id = Guid.NewGuid(),
                    Name = "Project5",
                },
                new Project
                {
                    Id = Guid.NewGuid(),
                    Name = "Project6",
                },
                new Project
                {
                    Id = Guid.NewGuid(),
                    Name = "Project7",
                },
                new Project
                {
                    Id = Guid.NewGuid(),
                    Name = "Project8",
                }
            };

            context.Projects.AddOrUpdate(p => p.Name, projects);
        }
    }
}
