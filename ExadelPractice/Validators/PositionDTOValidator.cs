﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ExadelPractice.Abstract.Services;
using ExadelPractice.DTOs;
using ExadelPractice.Models;
using FluentValidation;

namespace ExadelPractice.Validators
{
    public class PositionDTOValidator : AbstractValidator<PositionDTO>
    {
        private readonly ICityService _cityService;
        private readonly IProfessionService _professionService;
        private readonly ISkillsService _skillsService;
        private readonly IPositionStatusService _statusService;
        private readonly IProjectService _projectService;

        public PositionDTOValidator(ICityService cityService, IProfessionService professionService,
            ISkillsService skillsService, IPositionStatusService statusService, IProjectService projectService)
        {
            _cityService = cityService;
            _professionService = professionService;
            _skillsService = skillsService;
            _statusService = statusService;
            _projectService = projectService;

            RuleFor(c => c.Header).NotNull().NotEmpty();
            RuleFor(c => c.Experience).InclusiveBetween((short) 0, Int16.MaxValue);

            RuleFor(c => c.City).Must(IsValidCity);
            RuleFor(c => c.Profession).Must(IsValidProffession);
            RuleFor(c => c.Status).Must(IsValidStatus);
            RuleFor(c => c.Skills).Must(SkillsAreValid);
            RuleFor(c => c.Project).Must(IsValidProject);
        }

        private bool IsValidCity(PositionCityDTO city)
        {
            if (city == null)
            {
                return true;
            }
            var id = city.Id;
            return _cityService.IsExists(id);
        }

        private bool IsValidProffession(PositionProfessionDTO prof)
        {
            if (prof == null)
            {
                return false;
            }

            var id = prof.Id;
            return _professionService.IsExists(id);
        }

        private bool IsValidStatus(PositionStatusDTO status)
        {
            if (status == null)
            {
                return false;
            }

            Guid id = status.Id;
            return _statusService.IsExists(id);
        }

        private bool SkillsAreValid(IEnumerable<PositionSkillDTO> skills)
        {
            HashSet<Guid> ids = new HashSet<Guid>();
            if (skills == null)
            {
                return false;
            }

            foreach (var skill in skills)
            {
                Guid id = skill.Id;
                if (!_skillsService.IsExists(id) || ids.Contains(id))
                {
                    return false;
                }
                ids.Add(id);
            }
            return true;
        }

        private bool IsValidProject(PositionProjectDTO project)
        {
            if (project == null)
            {
                return true;
            }

            Guid id = project.Id;
            return _projectService.IsExists(id);
        }
    }
}