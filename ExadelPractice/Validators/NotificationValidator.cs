﻿using ExadelPractice.Filters.ControllersFilters;
using FluentValidation;

namespace ExadelPractice.Validators
{
    public class NotificationValidator : AbstractValidator<NotificationFilter>
    {
        public NotificationValidator()
        {
            RuleFor(n => n.Skip).InclusiveBetween(0, int.MaxValue).WithMessage("Skip is not valid.");
            RuleFor(n => n.Amount).InclusiveBetween(0, int.MaxValue).WithMessage("Amount is not valid.");
        }
    }
}