﻿using System;
using System.Collections.Generic;
using Castle.Core.Internal;
using ExadelPractice.Abstract.Services;
using ExadelPractice.Filters.ControllersFilters;
using FluentValidation;

namespace ExadelPractice.Validators
{
    public class CandidateFilterValidator : AbstractValidator<CandidateFilter>
    {
        private readonly ICityService _cityService;
        private readonly IProfessionService _professionService;
        private readonly ISkillsService _skillsService;
        private readonly ICandidateStatusService _statusService;

        public CandidateFilterValidator(ICityService cityService, IProfessionService professionService, 
            ISkillsService skillsService, ICandidateStatusService statusService)
        {
            _cityService = cityService;
            _professionService = professionService;
            _skillsService = skillsService;
            _statusService = statusService;

            RuleFor(c => c.Skip).InclusiveBetween(0, Int32.MaxValue);
            RuleFor(c => c.Amount).InclusiveBetween(0, Int32.MaxValue);
            RuleFor(c => c.Cities).Must(AreValidCities);
            RuleFor(c => c.Professions).Must(AreValidProfessions);
            RuleFor(c => c.Statuses).Must(AreValidStatuses);
            RuleFor(c => c.Skills).Must(SkillsAreValid);
            RuleFor(c => c.Experience).InclusiveBetween((short) 0, Int16.MaxValue);
        }

        private bool AreValidCities(List<Guid> ids)
        {
            if (ids.IsNullOrEmpty())
            {
                return true;
            }

            foreach (Guid id in ids)
            {
                if (!_cityService.IsExists(id))
                {
                    return false;

                }
            }
            return true;
        }

        private bool AreValidProfessions(List<Guid> ids)
        {
            if (ids.IsNullOrEmpty())
            {
                return true;
            }

            foreach (Guid id in ids)
            {
                if (!_professionService.IsExists(id))
                {
                    return false;

                }
            }
            return true;
        }

        private bool AreValidStatuses(List<Guid> ids)
        {
            if (ids.IsNullOrEmpty())
            {
                return true;
            }

            foreach (Guid id in ids)
            {
                if (!_statusService.IsExists(id))
                {
                    return false;

                }
            }
            return true;
        }

        private bool SkillsAreValid(List<Guid> ids)
        {
            if (ids == null)
            {
                return true;
            }

            foreach (var id in ids)
            {
                if (!_skillsService.IsExists(id))
                {
                    return false;
                }
            }
            return true;
        }
    }
}