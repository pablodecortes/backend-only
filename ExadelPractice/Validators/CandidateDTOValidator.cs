﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Castle.Core.Internal;
using ExadelPractice.Abstract.Services;
using ExadelPractice.DTOs;
using ExadelPractice.Models;
using FluentValidation;

namespace ExadelPractice.Implements.Validators
{
    public class CandidateDTOValidator : AbstractValidator<CandidateDTO>
    {
        private readonly ICityService _cityService;
        private readonly IProfessionService _professionService;
        private readonly ISkillsService _skillsService;
        private readonly ICandidateStatusService _statusService;

        public CandidateDTOValidator(ICityService cityService, IProfessionService professionService,
            ISkillsService skillsService, ICandidateStatusService statusService)
        {
            _cityService = cityService;
            _professionService = professionService;
            _skillsService = skillsService;
            _statusService = statusService;

            RuleFor(c => c.NameRus).NotNull().NotEmpty();
            RuleFor(c => c.SurnameRus).NotNull().NotEmpty();
            RuleFor(c => c.NameEng).NotNull().NotEmpty();
            RuleFor(c => c.SurnameEng).NotNull().NotEmpty();

            RuleFor(c => c.City).Must(IsValidCity);
            RuleFor(c => c.Profession).Must(IsValidProffession);
            RuleFor(c => c.Status).Must(IsValidStatus);
            RuleFor(c => c.Skills).Must(SkillsAreValid);
            RuleFor(c => c.Email).Must(CandidateEmailsAreValid);


            RuleFor(c => c.PhoneNumber).Must(IsPhoneNumber);
            RuleFor(c => c.Salary).InclusiveBetween(0, Int32.MaxValue);
            RuleFor(c => c.Experience).InclusiveBetween((short) 0, Int16.MaxValue);
            RuleFor(c => c.EnglishLevel).InclusiveBetween((short) 0, (short) 7);
        }

        private bool IsValidCity(CandidateCityDTO city)
        {
            if (city == null)
            {
                return true;
            }
            var id = city.Id;
            return _cityService.IsExists(id);
        }

        private bool IsValidProffession(ProfessionDTO prof)
        {
            if (prof == null)
            {
                return false;
            }

            var id = prof.Id;
            return _professionService.IsExists(id);
        }

        private bool IsValidStatus(CandidateStatusDTO status)
        {
            if (status == null)
            {
                return false;
            }

            Guid id = status.Id;
            return _statusService.IsExists(id);
        }

        private bool SkillsAreValid(IEnumerable<CandidateSkillDTO> skills)
        {
            HashSet<Guid> ids = new HashSet<Guid>();
            if (skills == null)
            {
                return false;
            }

            foreach (var skill in skills)
            {
                Guid id = skill.Id;
                if (!_skillsService.IsExists(id) || ids.Contains(id))
                {
                    return false;
                }
                ids.Add(id);
            }
            return true;
        }

        private bool IsPhoneNumber(string number)
        {
            if (number.IsNullOrEmpty()) return false;
            return Regex.Match(number, "^[+][0-9]{9,18}").Success;
        }

        private bool CandidateEmailsAreValid(IEnumerable<EmailDTO> emails)
        {
            if (emails == null)
            {
                return false;
            }
            bool wasPrimaryEmail = false;
            foreach (var email in emails)
            {
                if (!IsValidEmail(email.Email))
                {
                    return false;
                }

                if (!email.IsPrimary) continue;
                if (wasPrimaryEmail)
                    return false;

                wasPrimaryEmail = true;
            }

            return wasPrimaryEmail;
        }

        private bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
    }
}