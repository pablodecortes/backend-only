﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using ExadelPractice.Abstract.Services;
using ExadelPractice.DTOs;
using FluentValidation;

/*
{
  "position": "00000000-0000-0000-0000-000000000000"
}
 */

namespace ExadelPractice.Validators
{
    public class InterviewDTOValidator : AbstractValidator<InterviewDTO>
    {
        private readonly IUserService _userService;
        private readonly ICandidateService _candidateService;
        private readonly ISkillsService _skillsService;
        private readonly IPositionService _positionService;

        public InterviewDTOValidator(IUserService userService, ICandidateService candidateService,
            ISkillsService skillsService, IPositionService positionService)
        {
            _userService = userService;
            _candidateService = candidateService;
            _skillsService = skillsService;
            _positionService = positionService;

            RuleFor(c => c.Type).Must(IsValidInterviewType);
            RuleFor(c => c.Header).NotNull().NotEmpty();
            RuleFor(c => c.EnglishLevel).InclusiveBetween((short)0, (short)7);
            RuleFor(c => c.Salary).InclusiveBetween(0, Int32.MaxValue);

            RuleFor(c => c.Candidate).Must(IsValidCandidate);
            RuleFor(c => c.Skills).Must(SkillsAreValid);
            RuleFor(c => c.HrmEmail).NotNull().NotEmpty().Must(IsValidHRM);
            RuleFor(c => c.InterviewerEmail).Must(IsValidUser);
            RuleFor(c => c.Position).Must(IsValidPosition);

        }

        private bool IsValidInterviewType(string type)
        {
            var types = new List<string>
            {
                "hrm",
                "ts",
                "cu"
            };
            return types.Contains(type.ToLower());
        }

        private bool IsValidCandidate(CandidateInterviewDTO candidate)
        {
            if (candidate == null)
            {
                return false;
            }

            var id = candidate.Id;
            return _candidateService.IsExists(id);
        }

        private bool SkillsAreValid(IEnumerable<InterviewSkillDTO> skills)
        {
            if (skills == null)
            {
                return false;
            }

            return skills.Select(skill => skill.SkillId).All(id => _skillsService.IsExists(id));
        }

        private bool IsValidHRM(string email)
        {
            var user = _userService.GetUser(email);
            if (user == null)
            {
                return false;
            }

            var accessLevel = user.AccessLevel.ToLower();;
            return accessLevel == "hrm" || accessLevel == "admin";
        }

        private bool IsValidUser(string email)
        {
            if (email == null)
            {
                return false;
            }

            return _userService.IsExists(email);
        }

        private bool IsValidPosition(Guid? id)
        {
            if (id == null)
            {
                return true;
            }

            return _positionService.IsExists((Guid) id);
        }
    }
}