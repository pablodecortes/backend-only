﻿using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Configuration;
using ExadelPractice.Abstract.Services;
using Microsoft.Owin.Security.OAuth;

namespace ExadelPractice.Providers
{
    public class SimpleAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        private readonly IAuthService _authService;

        public SimpleAuthorizationServerProvider(IAuthService authService)
        {
            _authService = authService;
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext ctx)
        {
            ctx.Validated();
            return Task.FromResult<object>(null);
        }

        public override Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext ctx)
        {
            AddCorsHeaders(ctx);

            var user = _authService.FindUser(ctx.UserName, ctx.Password);

            if (user == null)
            {
                ctx.SetError("invalid_grant", "The user name or password is incorrect.");

                return Task.FromResult<object>(null);
            }

            var identity = new ClaimsIdentity(ctx.Options.AuthenticationType);
            identity.AddClaim(new Claim(ClaimTypes.Role, user.AccessLevel));
            identity.AddClaim(new Claim(ClaimTypes.Name, user.Email));

            ctx.Validated(identity);
            
            return Task.FromResult<object>(null);
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext ctx)
        {
            foreach (var property in ctx.Properties.Dictionary)
            {
                ctx.AdditionalResponseParameters.Add(property.Key, property.Value);
            }
            return Task.FromResult<object>(null);
        }

        public void AddCorsHeaders(OAuthGrantResourceOwnerCredentialsContext ctx)
        {
            ctx.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] {AppConfig.GetCorsOrigin()});
            ctx.OwinContext.Response.Headers.Add("Access-Control-Allow-Credentials", new[] { "true" });
            ctx.OwinContext.Response.Headers.Add("Access-Control-Allow-Methods", new[] { "*" });
            ctx.OwinContext.Response.Headers.Add("Access-Control-Allow-Headers", new[] { "*" });
        }
    }
}