﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ExadelPractice.Models
{
    public class Interview
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string Type { get; set; }
        public CandidateInterview CandidateInterview { get; set; }
        public string Status { get; set; }
        public string Header { get; set; }
        public string InterviewerEmail { get; set; }
        public string HrmEmail { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public string Profession { get; set; }

        public string FeedbackText { get; set; }
        public string JobChangeReason { get; set; }
        public string ReadyToGetStarted { get; set; }
        public string WillingnessToTravel { get; set; }
        public string Motivation { get; set; }
        public short EnglishLevel { get; set; }
        public decimal Salary { get; set; }
        public ICollection<InterviewSkill> InterviewSkills { get; set; }
        public string FeedbackMessage { get; set; }

        public Guid? Position { get; set; }
    }
}