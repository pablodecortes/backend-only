﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ExadelPractice.Models
{
    public class CandidateProfession
    {
        [Key]
        [ForeignKey("Candidate")]
        public Guid Id { get; set; }
        public Candidate Candidate { get; set; }
        public Guid ProfessionId { get; set; }
        public virtual Profession Profession { get; set; }
    }
}