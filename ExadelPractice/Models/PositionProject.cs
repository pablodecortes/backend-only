﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ExadelPractice.Models
{
    public class PositionProject
    {
        [Key]
        [ForeignKey("Position")]
        public Guid PositionId { get; set; }
        public Position Position { get; set; }
        public Guid ProjectId { get; set; }
        public virtual Project Project { get; set; }
    }
}