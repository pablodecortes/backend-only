﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ExadelPractice.Models
{
    public class Skill
    {
        [Key]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid? ParentId { get; set; }
        public ICollection<CandidateSkill> CandidateSkills { get; set; }
        public ICollection<PositionSkill> PositionSkills { get; set; }
        public ICollection<InterviewSkill> InterviewSkills { get; set; }
    }
}
