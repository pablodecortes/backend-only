﻿using System;
using System.Collections.Generic;
using ExadelPractice.DTOs;

namespace ExadelPractice.Models
{
    public class CandidatesOrCandidateDTO
    {
        public List<CandidateNote> candidates { get; set; }
        public CandidateDTO candidate { get; set; }
    }
}