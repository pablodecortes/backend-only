﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ExadelPractice.Models
{
    public class CandidateStatusLayer
    {
        [Key]
        [ForeignKey("Candidate")]
        public Guid Id { get; set; }
        public Candidate Candidate { get; set; }
        public Guid CandidateStatusId { get; set; }
        public virtual CandidateStatus CandidateStatus { get; set; }
    }
}