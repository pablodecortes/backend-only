﻿using System;
using System.Collections.Generic;

namespace ExadelPractice.Models
{
    public class SkillNode
    {
        public Guid? Id { get; set; }
        public Guid? ParentId { get; set; }
        public string Name { get; set; }
        public ICollection<SkillNode> Children { get; set; }

    }
}