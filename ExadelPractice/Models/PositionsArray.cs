﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ExadelPractice.DTOs;

namespace ExadelPractice.Models
{
    public class PositionsArray
    {
        public IEnumerable<Position> positions { get; set; }
        public int amount { get; set; }
    }
}