﻿using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace ExadelPractice.Models
{
    public class ExadelPracticeContext : DbContext
    {
        public ExadelPracticeContext() : base("name=ExadelPracticeContext")
        {
        }

        public DbSet<Candidate> Candidates { get; set; }
        public DbSet<Position> Positions { get; set; }
        public DbSet<Skill> Skills { get; set; }
        public DbSet<CandidateSkill> CandidateSkills { get; set; }
        public DbSet<CandidateEmail> CandidateEmails { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<PositionSkill> PositionSkills { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<CandidateCity> CandidateCities { get; set; }
        public DbSet<PositionCity> PositionCities { get; set; }
        public DbSet<Profession> Professions { get; set; }
        public DbSet<PositionStatus> PositionStatus { get; set; }
        public DbSet<CandidateStatus> CandidateStatus { get; set; }
        public DbSet<HistoryNode> HistoryNodes { get; set; }
        public DbSet<Interview> Interviews { get; set; }
        public DbSet<InterviewProfession> InterviewProfessions { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Resume> Resumes { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<HistoryCache> HistoryCache { get; set; }
        public DbSet<HistoryMessage> HistoryMessages { get; set; }
        public DbSet<GoogleCredential> GoogleCredentials { get; set; }

        protected override void OnModelCreating(DbModelBuilder builder)
        {
            CreateUserModel(builder);

            CreateCandidateSkillsModel(builder);
            CreateCandidateCityModel(builder);
            CreateCandidateProfessionModel(builder);
            CreateCandidatePositionModel(builder);
            CreateCandidateStatusModel(builder);
            CreateCandidateInterviewModel(builder);

            CreatePositionSkillsModel(builder);
            CreatePositionCityModel(builder);
            CreatePositionStatusModel(builder);
            CreatePositionProfessionModel(builder);
            CreatePositionInterviewModel(builder);
            CreatePositionRrojectModel(builder);

            CreateInterviewSkillsModel(builder);
            CreateInterviewProfessionModel(builder);
        }

        private void CreateUserModel(DbModelBuilder builder)
        {
            builder.Entity<IdentityUserLogin>().HasKey(l => l.UserId);
            builder.Entity<IdentityRole>().HasKey(r => r.Id);
            builder.Entity<IdentityUserRole>().HasKey(r => new { r.RoleId, r.UserId });
        }

        private void CreateCandidateSkillsModel(DbModelBuilder builder)
        {
            // Primary keys
            builder.Entity<Candidate>().HasKey(q => q.Id);
            builder.Entity<Skill>().HasKey(q => q.Id);
            builder.Entity<CandidateSkill>().HasKey(q =>
                new {
                    q.CandidateId,
                    q.SkillId
                });

            // Relationships
            builder.Entity<CandidateSkill>()
                .HasRequired(t => t.Skill)
                .WithMany(t => t.CandidateSkills)
                .HasForeignKey(t => t.SkillId);

            builder.Entity<CandidateSkill>()
                .HasRequired(t => t.Candidate)
                .WithMany(t => t.CandidateSkills)
                .HasForeignKey(t => t.CandidateId);
        }

        private void CreatePositionSkillsModel(DbModelBuilder builder)
        {
            // Primary keys
            builder.Entity<Position>().HasKey(q => q.Id);
            builder.Entity<Skill>().HasKey(q => q.Id);
            builder.Entity<PositionSkill>().HasKey(q =>
                new {
                    q.PositionId,
                    q.SkillId
                });

            // Relationships
            builder.Entity<PositionSkill>()
                .HasRequired(t => t.Skill)
                .WithMany(t => t.PositionSkills)
                .HasForeignKey(t => t.SkillId);

            builder.Entity<PositionSkill>()
                .HasRequired(t => t.Position)
                .WithMany(t => t.Skills)
                .HasForeignKey(t => t.PositionId);
        }

        private void CreateCandidateCityModel(DbModelBuilder builder)
        {
            //Relationships
            builder.Entity<CandidateCity>()
                .HasRequired(t => t.City)
                .WithMany(t => t.CandidateCities)
                .HasForeignKey(t => t.CityId);
        }

        private void CreatePositionCityModel(DbModelBuilder builder)
        {
            //Relationships
            builder.Entity<PositionCity>()
                .HasRequired(t => t.City)
                .WithMany(t => t.PositionCities)
                .HasForeignKey(t => t.CityId);
        }

        private void CreateCandidateProfessionModel(DbModelBuilder builder)
        {
            //Relationships
            builder.Entity<CandidateProfession>()
                .HasRequired(t => t.Profession)
                .WithMany(t => t.CandidateProfessions)
                .HasForeignKey(t => t.ProfessionId);
        }

        private void CreatePositionProfessionModel(DbModelBuilder builder)
        {
            //Relationships
            builder.Entity<PositionProfession>()
                .HasRequired(t => t.Profession)
                .WithMany(t => t.PositionProfessions)
                .HasForeignKey(t => t.ProfessionId);
        }

        private void CreateCandidatePositionModel(DbModelBuilder builder)
        {
            // Primary keys
            builder.Entity<Candidate>().HasKey(q => q.Id);
            builder.Entity<Position>().HasKey(q => q.Id);
            builder.Entity<CandidatePosition>().HasKey(q =>
                new {
                    q.CandidateId,
                    q.PositionId
                });

            // Relationships
            builder.Entity<CandidatePosition>()
                .HasRequired(t => t.Position)
                .WithMany(t => t.CandidatePositions)
                .HasForeignKey(t => t.PositionId);

            builder.Entity<CandidatePosition>()
                .HasRequired(t => t.Candidate)
                .WithMany(t => t.CandidatePositions)
                .HasForeignKey(t => t.CandidateId);
        }

        private void CreatePositionStatusModel(DbModelBuilder builder)
        {
            //Relationships
            builder.Entity<PositionStatusLayer>()
                .HasRequired(t => t.PositionStatus)
                .WithMany(t => t.PositionStatusLayers)
                .HasForeignKey(t => t.PositionStatusId);
        }

        private void CreateCandidateStatusModel(DbModelBuilder builder)
        {
            //Relationships
            builder.Entity<CandidateStatusLayer>()
                .HasRequired(t => t.CandidateStatus)
                .WithMany(t => t.CandidateStatusLayers)
                .HasForeignKey(t => t.CandidateStatusId);
        }

        private void CreateInterviewSkillsModel(DbModelBuilder builder)
        {
            builder.Entity<InterviewSkill>().HasKey(q =>
                new {
                    q.InterviewId,
                    q.SkillId
                });

            // Relationships
            builder.Entity<InterviewSkill>()
                .HasRequired(t => t.Skill)
                .WithMany(t => t.InterviewSkills)
                .HasForeignKey(t => t.SkillId);

            builder.Entity<InterviewSkill>()
                .HasRequired(t => t.Interview)
                .WithMany(t => t.InterviewSkills)
                .HasForeignKey(t => t.InterviewId);
        }

        private void CreateInterviewProfessionModel(DbModelBuilder builder)
        {
            builder.Entity<InterviewProfession>()
                .HasRequired(t => t.Profession)
                .WithMany(t => t.InterviewProfessions)
                .HasForeignKey(t => t.ProfessionId);
        }

        private void CreateCandidateInterviewModel(DbModelBuilder builder)
        {
            builder.Entity<CandidateInterview>()
                .HasRequired(t => t.Candidate)
                .WithMany(t => t.CandidateInterviews)
                .HasForeignKey(t => t.CandidateId);
        }

        private void CreatePositionInterviewModel(DbModelBuilder builder)
        {
            builder.Entity<PositionInterview>()
                .HasRequired(t => t.Position)
                .WithMany(t => t.PositionInterviews)
                .HasForeignKey(t => t.PositionId);
        }

        private void CreatePositionRrojectModel(DbModelBuilder builder)
        {
            //Relationships
            builder.Entity<PositionProfession>()
                .HasRequired(t => t.Profession)
                .WithMany(t => t.PositionProfessions)
                .HasForeignKey(t => t.ProfessionId);
        }

    }
}
