﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ExadelPractice.Models
{
    public class Candidate
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        [Required]
        public string NameRus { get; set; }
        [Required]
        public string SurnameRus { get; set; }
        public string PatronymicRus { get; set; }
        [Required]
        public string NameEng { get; set; }
        [Required]
        public string SurnameEng { get; set; }
        [Required]
        public CandidateStatusLayer CandidateStatusLayer { get; set; }
        public ICollection<CandidateEmail> Email { get; set; }
        public string LinkedIn { get; set; }
        public string SkypeId { get; set; }
        public string PhoneNumber { get; set; }
        public CandidateCity CandidateCity { get; set; }
        public CandidateProfession CandidateProfession { get; set; }
        [Required]
        public ICollection<CandidateSkill> CandidateSkills { get; set; }
        public double Experience { get; set; }
        public decimal Salary { get; set; }
        public DateTime? LastContact { get; set; }
        public DateTime? NextContact { get; set; }
        public string HR { get; set; }
        public short EnglishLevel { get; set; }
        public string Description { get; set; }
        public ICollection<CandidateInterview> CandidateInterviews { get; set; }
        public ICollection<CandidatePosition> CandidatePositions { get; set; }
    }
}