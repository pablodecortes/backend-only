﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ExadelPractice.Models
{
    public class CandidateInterview
    {
        [Key]
        [ForeignKey("Interview")]
        public Guid InterviewId { get; set; }
        public Interview Interview { get; set; }
        public Guid CandidateId { get; set; }
        public virtual Candidate Candidate { get; set; }
    }
}