﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExadelPractice.Models
{
    public class LuceneCandidate
    {
        public Guid Id { get; set; }
        public string NameRus { get; set; }
        public string SurnameRus { get; set; }
        public string NameEng { get; set; }
        public string SurnameEng { get; set; }
    }
}