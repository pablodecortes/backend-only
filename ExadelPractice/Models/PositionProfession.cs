﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ExadelPractice.Models
{
    public class PositionProfession
    {
        [Key]
        [ForeignKey("Position")]
        public Guid PositionId { get; set; }
        public Position Position { get; set; }
        public Guid ProfessionId { get; set; }
        public virtual Profession Profession { get; set; }
    }
}