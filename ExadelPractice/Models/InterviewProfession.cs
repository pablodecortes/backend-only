﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ExadelPractice.Models
{
    public class InterviewProfession
    {
        [Key]
        [ForeignKey("Interview")]
        public Guid InterviewId { get; set; }
        public Interview Interview { get; set; }
        public Guid ProfessionId { get; set; }
        public virtual Profession Profession { get; set; }
    }
}