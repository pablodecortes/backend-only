﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ExadelPractice.Models
{
    public class PositionInterview
    {
        [Key]
        [ForeignKey("Interview")]
        public Guid Id { get; set; }
        public Interview Interview { get; set; }
        public Guid PositionId { get; set; }
        public virtual Position Position { get; set; }
    }
}