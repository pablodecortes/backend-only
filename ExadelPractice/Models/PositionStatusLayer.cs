﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ExadelPractice.Models
{
    public class PositionStatusLayer
    {
        [Key]
        [ForeignKey("Position")]
        public Guid Id { get; set; }
        public Position Position { get; set; }
        public Guid PositionStatusId { get; set; }
        public virtual PositionStatus PositionStatus { get; set; }
    }
}