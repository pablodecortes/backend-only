﻿using System.ComponentModel.DataAnnotations;

namespace ExadelPractice.Models
{
    public class GoogleCredential
    {
        [Key]
        public string Key { get; set; }
        
        public string Value { get; set; }
    }
}