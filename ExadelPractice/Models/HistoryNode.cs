﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ExadelPractice.Models
{
    public class HistoryNode
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public ICollection<HistoryMessage> Messages { get; set; }
        public string Action { get; set; }
        public DateTime Date { get; set; }
        public Guid? CandidateId { get; set; }
        public Guid? PositionId { get; set; }
        public string Employee { get; set; }
    }
}