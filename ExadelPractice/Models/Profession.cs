﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ExadelPractice.Models
{
    public class Profession
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        public ICollection<CandidateProfession> CandidateProfessions { get; set; }
        public ICollection<PositionProfession> PositionProfessions { get; set; }
        public ICollection<InterviewProfession> InterviewProfessions { get; set; }
    }
}