﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ExadelPractice.Models
{
    public class CandidateStatus
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public ICollection<CandidateStatusLayer> CandidateStatusLayers { get; set; }
    }
}