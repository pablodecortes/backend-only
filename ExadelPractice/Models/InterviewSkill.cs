﻿using System;

namespace ExadelPractice.Models
{
    public class InterviewSkill
    {
        public Guid InterviewId { get; set; }
        public Guid SkillId { get; set; }
        public Interview Interview { get; set; }
        public virtual Skill Skill { get; set; }
        public int DefultRating { get; set; }
        public int ConfRating { get; set; }
        public double Experience { get; set; }
        public bool IsPrimary { get; set; }
        public bool WasConfirmed { get; set; }
    }
}