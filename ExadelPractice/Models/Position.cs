﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ExadelPractice.Models
{
    public class Position
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string Header { get; set; }
        public PositionStatusLayer PositionStatusLayer { get; set; }
        public PositionProfession PositionProfession { get; set; }
        public DateTime? ProjectDate { get; set; }
        public DateTime? RequestDate { get; set; }
        public short EnglishLevel { get; set; }
        public string Description { get; set; }
        public short Experience { get; set; }
        public PositionCity PositionCity { get; set; }
        public PositionProject PositionProject { get; set; }
        public ICollection<PositionSkill> Skills { get; set; }
        public ICollection<CandidatePosition> CandidatePositions { get; set; }
        public ICollection<PositionInterview> PositionInterviews { get; set; }

    }
}
