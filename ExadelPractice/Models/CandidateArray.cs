﻿using System.Collections.Generic;

namespace ExadelPractice.Models
{
    public class CandidateArray
    {
        public IEnumerable<Candidate> candidates { get; set; }
        public int amount { get; set; }
    }
}