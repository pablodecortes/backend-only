using System;

namespace ExadelPractice.Models
{
    public class CandidateSkill
    {
        public Guid CandidateId { get; set; }
        public Guid SkillId { get; set; }
        public Candidate Candidate { get; set; }
        public virtual Skill Skill { get; set; }
        public int Rating { get; set; }
        public double Experience { get; set; }
        public int? LastUsage { get; set; }
        public bool IsPrimary { get; set; }
        public bool WasConfirmed { get; set; }
    }
}