﻿using System;

namespace ExadelPractice.Models
{
    public class PositionSkill
    {
        public Guid PositionId { get; set; }

        public Guid SkillId { get; set; }

        public Position Position { get; set; }
        public virtual Skill Skill { get; set; }
        public int Level { get; set; }
        public bool IsPrimary { get; set; }
    }
}