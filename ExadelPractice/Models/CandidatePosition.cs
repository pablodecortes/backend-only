﻿using System;

namespace ExadelPractice.Models
{
    public class CandidatePosition
    {
        public Guid CandidateId { get; set; }
        public Guid PositionId { get; set; }
        public virtual Candidate Candidate { get; set; }
        public virtual Position Position { get; set; }
    }
}