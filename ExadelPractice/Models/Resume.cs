﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ExadelPractice.Models
{
    public class Resume
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        
        public Guid CandidateId { get; set; }
       
        public string Attachment { get; set; }

        public string ContentType { get; set; }

        public string Extension { get; set; }
    }
}