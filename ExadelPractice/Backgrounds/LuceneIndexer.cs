﻿using System.Collections.Generic;
using System.Linq;
using ExadelPractice.Abstract.Repositories;
using ExadelPractice.Libraries;
using ExadelPractice.Models;
using Hangfire;

namespace ExadelPractice.Backgrounds
{
    public class LuceneIndexer
    {
        private readonly LuceneCandidateSearch _luceneCandidate;
        private readonly LucenePositionSearch _lucenePosition;

        private readonly ICandidateRepository _candidateRepository;
        private readonly IPositionRepository _positionRepository;
        private readonly IHistoryCacheRepository _cache;

        public LuceneIndexer(LucenePositionSearch lucenePosition,
            LuceneCandidateSearch luceneCandidate, 
            ICandidateRepository candidateRepository,
            IPositionRepository positionRepository,
            IHistoryCacheRepository cache)
        {
            _lucenePosition = lucenePosition;
            _luceneCandidate = luceneCandidate;
            _candidateRepository = candidateRepository;
            _positionRepository = positionRepository;
            _cache = cache;
        }

        public void InitBackgrounds()
        {
            RecurringJob.AddOrUpdate(() => UpdateIndexes(), Cron.MinuteInterval(1));
            RecurringJob.AddOrUpdate(() => Optimize(), Cron.DayInterval(2));
        }

        public void UpdateIndexes()
        {
            _luceneCandidate.AddUpdateLuceneIndex(GetCandidates() ?? new List<LuceneCandidate>());
            _lucenePosition.AddUpdateLuceneIndex(GetPositions() ?? new List<LucenePosition>());
            _cache.Clear();
        }


        private IEnumerable<LuceneCandidate> GetCandidates()
        {
            var ids = _cache.GetCandidatesCache();
            var models = ids.Select(_candidateRepository.Read);
            return models.Select(AutoMapper.Mapper.Map<Candidate, LuceneCandidate>);
        }

        private IEnumerable<LucenePosition> GetPositions()
        {
            var ids = _cache.GetPositionsCache();
            var models = ids.Select(_positionRepository.Read);
            return models.Select(AutoMapper.Mapper.Map<Position, LucenePosition>);
        }

        public void Optimize()
        {
            _luceneCandidate.Optimize();
            _lucenePosition.Optimize();
        }
    }
}