﻿using System;
using System.Collections.Generic;
using ExadelPractice.Models;

namespace ExadelPractice.Abstract.Factories
{
    public interface INotificationFactory
    {
        ICollection<Notification> CreateInterviewNotifications(string email, Guid interviewId);
        ICollection<Notification> CreateInfoNotifications(string email, string route, Guid routeId);
        ICollection<Notification> CreateApproveNotifications(string email, Guid interviewId);
        ICollection<Notification> CreateFeedBackNotifications(string email, Guid interviewId);
        ICollection<Notification> CreateAttentionNotification(string email, Guid candidateId);
    }
}
