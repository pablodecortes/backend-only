﻿using System.Threading.Tasks;

namespace ExadelPractice.Abstract.Hubs
{
    public interface IPushNotificationHub
    {
        Task OnConnected();
        Task OnReconnected();
        Task OnDisconnected(bool stopCalled);
    }
}