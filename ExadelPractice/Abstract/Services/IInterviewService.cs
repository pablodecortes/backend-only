﻿using System;
using System.Collections.Generic;
using ExadelPractice.DTOs;

namespace ExadelPractice.Abstract.Services
{
    public interface IInterviewService
    {
        InterviewDTO Create(InterviewDTO item);
        InterviewDTO Read(Guid id);
        void Update(InterviewDTO item, string userName);
        ICollection<InterviewDTO> GetInterviews();
        bool IsExists(Guid id);
    }
}
