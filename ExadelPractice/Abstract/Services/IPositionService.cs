﻿using System;
using System.Collections.Generic;
using ExadelPractice.DTOs;
using ExadelPractice.Filters.ControllersFilters;

namespace ExadelPractice.Abstract.Services
{
    public interface IPositionService
    {
        PositionDTO Create(PositionDTO item, string email);
        PositionDTO Read(Guid id);
        void Update(PositionDTO item, string email);

        PositionDTOArray GetActivePositions(PositionsFilter filter);
        PositionDTOArray GetPositionsFromArchive(PositionsFilter filter);
        bool IsExists(Guid id);
    }
}