﻿using System.Collections.Generic;
using ExadelPractice.DTOs;
using ExadelPractice.Filters.ControllersFilters;

namespace ExadelPractice.Abstract.Services
{
    public interface IPositionSearch
    {
        IEnumerable<ReducedPositionDTO> SearchPositions(PositionsFilter positionsFilter);
    }
}
