﻿using System.Threading.Tasks;
using ExadelPractice.DTOs;
using ExadelPractice.Models;
using Microsoft.AspNet.Identity;

namespace ExadelPractice.Abstract.Services
{
    public interface IAuthService
    {
        UserDTO FindUser(string userName, string password);
        IdentityResult RegisterUser(User user);
    }
}