﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExadelPractice.DTOs;
using ExadelPractice.Models;

namespace ExadelPractice.Abstract.Services
{
    public interface IHistoryService
    {
        void CreateForCandidate(CandidateDTO oldCandidate, CandidateDTO newCandidate, string username);
        void CreateForCandidate(CandidateDTO newCandidate, string username);
        void CreateForPosition(PositionDTO oldPosition, PositionDTO newPosition, string username);
        void CreateForPosition(PositionDTO newPosition, string username);
        IEnumerable<HistoryNode> GetHistoryNodes(int skip, int amount);
        IEnumerable<HistoryNode> GetHistoryNodesForCandidate(Guid candidateId, int skip, int amount);
        IEnumerable<HistoryNode> GetHistoryNodesForPosition(Guid positionId, int skip, int amount);
    }
}
