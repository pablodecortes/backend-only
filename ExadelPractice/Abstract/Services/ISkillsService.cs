﻿using System;
using System.Collections.Generic;
using ExadelPractice.Models;

namespace ExadelPractice.Abstract.Services
{
    public interface ISkillsService
    {
        void Create(Skill item);
        Skill Read(Guid id);
        IEnumerable<Skill> GetSkills();
        bool IsExists(Guid id);
    }
}
