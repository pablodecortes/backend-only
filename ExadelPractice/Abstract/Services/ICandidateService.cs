﻿using System;
using ExadelPractice.DTOs;
using ExadelPractice.Filters.ControllersFilters;
using ExadelPractice.Models;

namespace ExadelPractice.Abstract.Services
{
    public interface ICandidateService
    {
        CandidatesOrCandidateDTO Create(CandidateDTO newCandidate, string email);
        CandidateDTO Read(Guid id);
        void Update(CandidateDTO candidate, string email);
        bool IsExists(Guid id);
        void PatchCandidates(Guid id, InterviewSkillDTOArray candidateSkills);
        CandidateDTO CreateForce(CandidateDTO item, string email);
        CandidateDTOArray FilterCandidates(CandidateFilter filter, bool isActive);
    }
}
