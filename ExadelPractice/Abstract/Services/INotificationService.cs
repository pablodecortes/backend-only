﻿using ExadelPractice.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ExadelPractice.Filters.ControllersFilters;

namespace ExadelPractice.Abstract.Services
{
    public interface INotificationService
    {
        void PostInfoNotifications(string email, string route, Guid routeId);
        void PostFeedBackNotifications(string email, Guid interviewId, DateTime nextContact);
        void PostApproveNotifications(string email, Guid intreviewId);
        void PostInterviewNotifications(string email, Guid interviewId);
        void PostAttentionNotifications(string email, Guid candidateId, DateTime nextContact);
        IEnumerable<Notification> GetNotifications(NotificationFilter filter, string email);
        Task MarkNotificationAsRead(Notification notification, string userName);
    }
}