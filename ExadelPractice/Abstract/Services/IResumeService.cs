﻿using System;
using ExadelPractice.Models;

namespace ExadelPractice.Abstract.Services
{
    public interface IResumeService
    {
        Resume Read(Guid candidateId);
        void Post(Resume resume);
    }
}