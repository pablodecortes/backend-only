﻿using ExadelPractice.Models;

namespace ExadelPractice.Abstract.Services
{
    public interface IPushNotificationService
    {
        void SendNotification(Notification notification, string userName);
    }
}
