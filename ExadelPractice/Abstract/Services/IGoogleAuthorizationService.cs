﻿using System.Threading.Tasks;
using Google.Apis.Auth.OAuth2;

namespace ExadelPractice.Abstract.Services
{
    public interface IGoogleAuthorizationService
    {
        Task<UserCredential> ReceiveCredentials();
    }
}
