﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExadelPractice.Models;

namespace ExadelPractice.Abstract.Services
{
    public interface IProfessionService
    {
        void Create(Profession item);
        Profession Read(Guid id);
        IEnumerable<Profession> GetProfessions();
        bool IsExists(Guid id);
    }
}
