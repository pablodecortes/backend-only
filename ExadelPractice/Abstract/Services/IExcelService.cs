﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;
using ClosedXML.Excel;
using ExadelPractice.DTOs;

namespace ExadelPractice.Abstract.Services
{
    public interface IExcelService
    {
        XLWorkbook CreateXLBook(IEnumerable<ReportDTO> reports);
        void FillWorksheet(IXLWorksheet ws, ReportDTO[] reports);
        void InsertCollection(IXLWorksheet ws, ReportDTO[] reports, int iteration);
        void InsertToCell(IXLWorksheet ws, ICollection<string> list, int row, int col);
        void CreateTitles(IXLWorksheet ws, string[] titles);
        MemoryStream SaveAsStream(XLWorkbook wb);
    }
}
