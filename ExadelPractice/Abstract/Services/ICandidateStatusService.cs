﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExadelPractice.Models;

namespace ExadelPractice.Abstract.Services
{
    public interface ICandidateStatusService
    {
        void Create(CandidateStatus item);
        CandidateStatus Read(Guid id);
        IEnumerable<CandidateStatus> GetCandidateStatuses();
        bool IsExists(Guid id);
    }
}
