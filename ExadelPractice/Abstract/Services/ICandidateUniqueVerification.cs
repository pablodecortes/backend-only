﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExadelPractice.DTOs;
using ExadelPractice.Libraries;

namespace ExadelPractice.Abstract.Services
{
    public interface ICandidateUniqueVerification
    {
        List<CandidateNote> Check(CandidateDTO item);
    }
}
