﻿using System.Collections.Generic;
using ExadelPractice.DTOs;
using ExadelPractice.Filters.ControllersFilters;

namespace ExadelPractice.Abstract.Services
{
    public interface ICandidateFilterService
    {
        IEnumerable<CandidateDTO> FilterCandidates(CandidateFilter filter, bool isActive);
    }
}
