﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using ExadelPractice.DTOs;
using ExadelPractice.Models;

namespace ExadelPractice.Abstract.Services
{
    public interface IPositionStatusService
    {
        PositionStatusDTO Create(PositionStatusDTO item);
        PositionStatusDTO Read(Guid id);
        void Delete(Guid id);
        bool IsExists(Guid id);
        IEnumerable<PositionStatusDTO> GetStatuses();
    }
}
