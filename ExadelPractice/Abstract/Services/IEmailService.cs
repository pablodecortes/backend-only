﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExadelPractice.Abstract.Services
{
    public interface IEmailService
    {
        void SendEmail(string email, string name, DateTime date);
    }
}
