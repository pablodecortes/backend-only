﻿using System;

namespace ExadelPractice.Abstract.Services
{
    public interface ICalendarEventService
    {
        void AddCalendarEvent(string email, string name, DateTime date);
    }
}
