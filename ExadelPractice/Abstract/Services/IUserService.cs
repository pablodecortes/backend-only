﻿using System.Collections.Generic;
using ExadelPractice.DTOs;

namespace ExadelPractice.Abstract.Services
{
    public interface IUserService
    {
        UserDTO GetUser(string userName);
        IEnumerable<UserDTO> GetHrms();
        IEnumerable<UserDTO> GetTechs();
        IEnumerable<UserDTO> GetAdmins();

        bool IsExists(string email);
    }
}