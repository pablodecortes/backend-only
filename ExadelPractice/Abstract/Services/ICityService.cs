﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExadelPractice.Models;

namespace ExadelPractice.Abstract.Services
{
    public interface ICityService
    {
        void Create(City item);
        City Read(Guid id);
        IEnumerable<City> GetCities();
        bool IsExists(Guid id);
    }
}
