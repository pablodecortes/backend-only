﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExadelPractice.Implements.Services;
using ExadelPractice.Models;

namespace ExadelPractice.Abstract.Services
{
    public interface ISkillsTreeable
    {
        SkillNode GetSkillTree();
    }
}
