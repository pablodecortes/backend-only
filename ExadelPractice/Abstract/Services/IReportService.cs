﻿using System.Collections.Generic;
using System.IO;
using ExadelPractice.DTOs;
using ExadelPractice.Models;

namespace ExadelPractice.Abstract.Services
{
    public interface IReportService
    {
        MemoryStream CreateReport(IEnumerable<CandidateDTO> candidates);
    }
}
