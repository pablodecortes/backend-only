﻿using System;
using System.Collections.Generic;
using ExadelPractice.Models;

namespace ExadelPractice.Abstract.Services
{
    public interface IProjectService
    {
        void Create(Project item);
        Project Read(Guid id);
        IEnumerable<Project> GetProjects();
        bool IsExists(Guid id);
    }
}
