﻿using ExadelPractice.DTOs;
using ExadelPractice.Filters.ControllersFilters;

namespace ExadelPractice.Abstract.Services
{
    public interface IPositionFilterService
    {
        PositionDTOArray FilterPositions(PositionsFilter filter, bool isActive);
    }
}
