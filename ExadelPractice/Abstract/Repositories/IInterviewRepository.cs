﻿using System;
using System.Collections.Generic;
using ExadelPractice.Models;

namespace ExadelPractice.Abstract.Repositories
{
    public interface IInterviewRepository
    {
        Interview Create(Interview item);
        Interview Read(Guid id);
        void Update(Interview item);

        ICollection<Interview> GetInterviews();
    }
}
