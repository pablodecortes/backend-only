﻿using System;
using System.Collections.Generic;
using ExadelPractice.Filters.ControllersFilters;
using ExadelPractice.Models;

namespace ExadelPractice.Abstract.Repositories
{
    public interface IPositionRepository
    {
        Position Create(Position item);
        Position Read(Guid id);
        void Update(Position item);

        IEnumerable<Position> GetAll();
     PositionsArray GetActivePositions(PositionsFilter filter, bool isActive);
        IEnumerable<LucenePosition> GetLucenePositions();
    }
}
