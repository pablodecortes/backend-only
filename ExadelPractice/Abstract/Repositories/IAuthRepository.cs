﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExadelPractice.Models;
using Microsoft.AspNet.Identity;

namespace ExadelPractice.Abstract.Repositories
{
    public interface IAuthRepository
    {
        IdentityResult RegisterUser(User user);
        User FindUser(string userName, string password);
    }
}
