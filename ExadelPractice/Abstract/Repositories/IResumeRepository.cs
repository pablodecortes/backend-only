﻿using System;
using ExadelPractice.Models;

namespace ExadelPractice.Abstract.Repositories
{
    public interface IResumeRepository
    {
        Resume Read(Guid candidateId);
        void Post(Resume resume);
    }
}