﻿using System;
using System.Collections.Generic;
using ExadelPractice.DTOs;
using ExadelPractice.Filters.ControllersFilters;
using ExadelPractice.Models;

namespace ExadelPractice.Abstract.Repositories
{
    public interface ICandidateRepository
    {
        Candidate Create(Candidate item);
        Candidate Read(Guid id);
        void Update(Candidate item);
        void Patch(Guid id, ICollection<CandidateSkillDTO> skills);
        ICollection<Candidate> GetAllCandidates();
        ICollection<Candidate> FilterCandidates(CandidateFilter filter, bool isActive);
        IEnumerable<LuceneCandidate> GetLuceneCandidates();
    }
}