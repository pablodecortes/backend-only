﻿using System.Threading.Tasks;
using ExadelPractice.Models;

namespace ExadelPractice.Abstract.Repositories
{
    public interface IDbStoreRepository
    {
        Task DeleteCredential(string key);
        GoogleCredential GetCredential(string key);
        Task ClearCredentials();
        Task StoreCredential(string key, string value);
    }
}
