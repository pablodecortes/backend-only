﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExadelPractice.Models;

namespace ExadelPractice.Abstract.Repositories
{
    public interface ICandidateStatusRepository
    {
        void Create(CandidateStatus item);
        CandidateStatus Read(Guid id);
        IEnumerable<CandidateStatus> GetCandidateStatuses();
    }
}
