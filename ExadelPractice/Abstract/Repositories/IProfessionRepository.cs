﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExadelPractice.Models;

namespace ExadelPractice.Abstract.Repositories
{
    public interface IProfessionRepository
    {
        void Create(Profession item);
        Profession Read(Guid id);
        IEnumerable<Profession> GetProfessions();
    }
}
