﻿using ExadelPractice.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ExadelPractice.Abstract.Repositories
{
    public interface ISkillsRepository
    {
        void Create(Skill item);
        Skill Read(Guid id);
        IEnumerable<Skill> GetAllSkills();
    }
}
