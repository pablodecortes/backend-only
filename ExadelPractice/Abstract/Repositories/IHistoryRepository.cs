﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExadelPractice.Models;

namespace ExadelPractice.Abstract.Repositories
{
    public interface IHistoryRepository
    {
        void Create(HistoryNode node);
        IEnumerable<HistoryNode> GetHistoryNodes(int skip, int amount);
        IEnumerable<HistoryNode> GetHistoryNodesForCandidate(Guid candidateId, int skip, int amount);
        IEnumerable<HistoryNode> GetHistoryNodesForPosition(Guid positionId, int skip, int amount);
    }
}
