﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ExadelPractice.Models;
using ExadelPractice.Filters.ControllersFilters;

namespace ExadelPractice.Abstract.Repositories
{
    public interface INotificationRepository
    {
        ICollection<Notification> GetNotifications(NotificationFilter filter, string email);
        void PostNotification(Notification notification);
        Task UpdateNotification(Notification notification);
    }
}