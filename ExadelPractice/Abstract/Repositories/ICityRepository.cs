﻿using System;
using System.Collections.Generic;
using ExadelPractice.Models;

namespace ExadelPractice.Abstract.Repositories
{
    public interface ICityRepository
    {
        void Create(City item);
        City Read(Guid id);
        IEnumerable<City> GetCities();
    }
}
