﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ExadelPractice.Models;

namespace ExadelPractice.Abstract.Repositories
{
    public interface IUserRepository
    {
        User GetUser(string userName);
        ICollection<User> GetHrms();
        ICollection<User> GetTechs();
        ICollection<User> GetAdmins();
    }
}