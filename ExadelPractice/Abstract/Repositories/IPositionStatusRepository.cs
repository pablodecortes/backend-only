﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExadelPractice.Models;

namespace ExadelPractice.Abstract.Repositories
{
    public interface IPositionStatusRepository
    {

        PositionStatus Create(PositionStatus item);
        PositionStatus Read(Guid id);
        void Delete(Guid id);

        IEnumerable<PositionStatus> GetStatuses();
    }
}
