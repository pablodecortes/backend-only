﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExadelPractice.Abstract.Repositories
{
    public interface IHistoryCacheRepository
    {
        void Clear();
        List<Guid> GetCandidatesCache();
        List<Guid> GetPositionsCache();
        void AddCandidateId(Guid id);
        void AddPositionId(Guid id);
    }
}
