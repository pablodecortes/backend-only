﻿using System;
using System.Collections.Generic;
using ExadelPractice.Models;

namespace ExadelPractice.Abstract.Repositories
{
    public interface IProjectRepositoty
    {
        void Create(Project item);
        Project Read(Guid id);
        IEnumerable<Project> GetProjects();
    }
}
