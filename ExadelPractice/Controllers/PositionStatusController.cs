﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;
using ExadelPractice.Abstract.Services;
using ExadelPractice.DTOs;
using ExadelPractice.Filters.AuthorizationFilters;

namespace ExadelPractice.Controllers
{
    public class PositionStatusController : ApiController
    {
        private readonly IPositionStatusService _service;
        public PositionStatusController(IPositionStatusService service)
        {
            _service = service;
        }
        // GET: api/Status
        [AuthorizationAccess("hrm", "admin")]
        public IEnumerable<PositionStatusDTO> GetPositionStatus()
        {
            return _service.GetStatuses();
        }

        // GET: api/Status/5
        [AuthorizationAccess("hrm", "admin")]
        [ResponseType(typeof(PositionStatusDTO))]
        public IHttpActionResult GetPositionStatus(Guid id)
        {
            var status = _service.Read(id);
            if (status == null)
            {
                return NotFound();
            }
            return Ok(status);
        }

        // POST: api/Status
        [AuthorizationAccess("hrm", "admin")]
        [ResponseType(typeof(PositionStatusDTO))]
        public IHttpActionResult PostPositionStatus(PositionStatusDTO positionStatus)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var item = _service.Create(positionStatus);
            return CreatedAtRoute("DefaultApi", new {id = item.Id}, item);
        }

        // DELETE: api/Status/5
        [AuthorizationAccess("hrm", "admin")]
        [ResponseType(typeof(PositionStatusDTO))]
        public IHttpActionResult DeletePositionStatus(Guid id)
        {
            var status = _service.Read(id);
            if (status == null)
            {
                return NotFound();
            }
            _service.Delete(id);
            return Ok(status);
        }
    }
}