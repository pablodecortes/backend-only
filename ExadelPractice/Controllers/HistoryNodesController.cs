﻿using System;
using System.Web.Http;
using ExadelPractice.Abstract.Services;
using ExadelPractice.Filters.AuthorizationFilters;

namespace ExadelPractice.Controllers
{
    public class HistoryNodesController : ApiController
    {
        private readonly IHistoryService _service;

        public HistoryNodesController(IHistoryService service)
        {
            _service = service;
        }

        [HttpGet]
        [AuthorizationAccess("hrm", "admin")]
        [Route("api/history")]
        public IHttpActionResult GetHistoryNodes(int skip = 0, int amount = 15)
        {
            if (skip < 0 || amount <= 0)
            {
                return BadRequest("Skip and amount must be Natural numbers");
            }

            var res = _service.GetHistoryNodes(skip, amount);
            return Ok(res);
        }

        [HttpGet]
        [AuthorizationAccess("hrm", "admin")]
        [Route("api/history/candidate/{id}")]
        public IHttpActionResult GetHistoryNodesForCandidate(Guid id, int skip = 0, int amount = 15)
        {
            if (skip < 0 || amount <= 0)
            {
                return BadRequest("Skip and amount must be Natural numbers");
            }

            var res = _service.GetHistoryNodesForCandidate(id, skip, amount);
            return Ok(res);
        }

        [HttpGet]
        [AuthorizationAccess("hrm", "admin")]
        [Route("api/history/position/{id}")]
        public IHttpActionResult GetHistoryNodesForPosition(Guid id, int skip = 0, int amount = 15)
        {
            if (skip < 0 || amount <= 0)
            {
                return BadRequest("Skip and amount must be Natural numbers");
            }

            var res = _service.GetHistoryNodesForPosition(id, skip, amount);
            return Ok(res);
        }

    }
}