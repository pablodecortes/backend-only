﻿using System;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using ExadelPractice.Abstract.Services;
using ExadelPractice.DTOs;
using ExadelPractice.Filters.AuthorizationFilters;
using ExadelPractice.Filters.ControllersFilters;
using FluentValidation;

namespace ExadelPractice.Controllers
{
    public class PositionsController : ApiController
    {
        private readonly IPositionService _service;
        private readonly IValidator<PositionsFilter> _filterValidator;
        private readonly IValidator<PositionDTO> _modelValidator;

        public PositionsController(IPositionService service, IValidator<PositionsFilter> validator, IValidator<PositionDTO> modelValidator)
        {
            _service = service;
            _filterValidator = validator;
            _modelValidator = modelValidator;
        }

        [HttpGet]
        [AuthorizationAccess("hrm", "admin")]
        [Route("api/positions/active")]
        [ResponseType(typeof(ReducedPositionDTO))]
        public IHttpActionResult GetActivePositions([FromUri] PositionsFilter filter)
        {
            if (filter == null)
            {
                filter = new PositionsFilter();
            }
            var result = _filterValidator.Validate(filter);
            if (!result.IsValid)
            {
                var errors = result.Errors;
                return BadRequest(string.Join(". ", errors));
            }
            var response = _service.GetActivePositions(filter);
            return Ok(response);
        }

        [HttpGet]
        [AuthorizationAccess("hrm", "admin")]
        [Route("api/positions/archive")]
        public IHttpActionResult GetPositionsFromArchive([FromUri] PositionsFilter positionsFilter)
        {
            if (positionsFilter == null)
            {
                positionsFilter = new PositionsFilter();
            }

            var result = _filterValidator.Validate(positionsFilter);
            if (!result.IsValid)
            {
                var errors = result.Errors;
                return BadRequest(string.Join(". ", errors));
            }

            var response = _service.GetPositionsFromArchive(positionsFilter);
            return Ok(response);
        }

        // GET: api/Positions/5
        [AuthorizationAccess("hrm", "admin")]
        [ResponseType(typeof(PositionDTO))]
        public IHttpActionResult GetPosition(Guid id)
        {
            var position = _service.Read(id);

            if (position == null)
            {
                return NotFound();
            }

            var skills = position.Skills.OrderByDescending(x => x.IsPrimary); //if u read it, that means that u know, what to ask
            position.Skills = skills.ToList();

            return Ok(position);
        }

        // PUT: api/Positions/5
        [AuthorizationAccess("hrm", "admin")]
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPosition(Guid id, PositionDTO position)
        {
            if (position == null || id != position.Id)
            {
                return BadRequest();
            }

            var result = _modelValidator.Validate(position);
            if (!result.IsValid)
            {
                var errors = result.Errors;
                return BadRequest(string.Join(". ", errors));
            }

            var userName = ActionContext.RequestContext.Principal.Identity.Name;
            _service.Update(position, userName);
            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Positions
        [AuthorizationAccess("hrm", "admin")]
        [ResponseType(typeof(PositionDTO))]
        public IHttpActionResult PostPosition(PositionDTO position)
        {
            if (position == null)
            {
                return BadRequest();
            }

            var result = _modelValidator.Validate(position);
            if (!result.IsValid)
            {
                var errors = result.Errors;
                return BadRequest(string.Join(". ", errors));
            }

            var userName = ActionContext.RequestContext.Principal.Identity.Name;
            var item = _service.Create(position, userName);
            return CreatedAtRoute("DefaultApi", new { item.Id}, item);
        }
    }
}