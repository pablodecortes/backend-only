﻿using System.Data.Entity.Infrastructure;
using System.Threading.Tasks;
using System.Web.Http;
using ExadelPractice.Abstract.Services;
using ExadelPractice.Filters.AuthorizationFilters;
using ExadelPractice.Filters.ControllersFilters;
using ExadelPractice.Models;
using FluentValidation;

namespace ExadelPractice.Controllers
{
    [RoutePrefix("api/notifications")]
    public class NotificationsController : ApiController
    {
        private readonly INotificationService _notificationService;
        private readonly IValidator<NotificationFilter> _validator;

        public NotificationsController(INotificationService notificationService,
            IValidator<NotificationFilter> validator)
        {
            _notificationService = notificationService;
            _validator = validator;
        }

        [HttpGet]
        [Authorize]
        public IHttpActionResult GetNotifications([FromUri] NotificationFilter filter)
        {
            if (filter == null)
            {
                filter = new NotificationFilter();
            }

            var validation = _validator.Validate(filter);
            if (!validation.IsValid)
            {
                var errors = validation.Errors;
                return BadRequest(string.Join(". ", errors));
            }

            var email = ActionContext.RequestContext.Principal.Identity.Name;
            var notifications = _notificationService.GetNotifications(filter, email);

            return Ok(notifications);
        }

        [HttpPut]
        [Route("read")]
        [Authorize]
        public async Task<IHttpActionResult> PutNotification([FromBody] Notification notification)
        {
            try
            {
                var email = ActionContext.RequestContext.Principal.Identity.Name;
                await _notificationService.MarkNotificationAsRead(notification, email);

                return Ok();
            }
            catch (DbUpdateException)
            {
                return NotFound();
            }
        }
    }
}
