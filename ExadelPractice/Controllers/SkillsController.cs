﻿using System;
using System.Web.Http;
using System.Web.Http.Description;
using ExadelPractice.Abstract.Services;
using ExadelPractice.Filters.AuthorizationFilters;
using ExadelPractice.Models;

namespace ExadelPractice.Controllers
{
    public class SkillsController : ApiController
    {
        private readonly ISkillsService _service;
        private readonly ISkillsTreeable _skillsTreeable;

        public SkillsController(ISkillsTreeable skillsTreeable, ISkillsService service)
        {
            _skillsTreeable = skillsTreeable;
            _service = service;
        }

        // GET: api/Skills
        [AuthorizationAccess("hrm", "admin")]
        [Route("api/skills")]
        public IHttpActionResult GetSkills()
        {
            var res = _skillsTreeable.GetSkillTree();
            return Ok(res);
        }

        [AuthorizationAccess("hrm", "admin")]
        [Route("api/skills/list")]
        public IHttpActionResult GetListOfSkills()
        {
            var res = _service.GetSkills();
            return Ok(res);
        }

        // GET: api/Skills/5
        [AuthorizationAccess("hrm", "admin")]
        [ResponseType(typeof(Skill))]
        public IHttpActionResult GetSkill(Guid id)
        {
            var skill = _service.Read(id);
            if (skill == null)
            {
                return NotFound();
            }

            return Ok(skill);
        }

        // POST: api/Skills
        [AuthorizationAccess("hrm", "admin")]
        [ResponseType(typeof(Skill))]
        public IHttpActionResult PostSkill(Skill skill)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _service.Create(skill);
            return Ok();
        }
    }
}