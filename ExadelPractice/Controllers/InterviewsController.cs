﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using ExadelPractice.Abstract.Services;
using ExadelPractice.DTOs;
using ExadelPractice.Filters.AuthorizationFilters;
using ExadelPractice.Models;
using FluentValidation;

namespace ExadelPractice.Controllers
{
    public class InterviewsController : ApiController
    {
        private readonly IInterviewService _service;
        private readonly IValidator<InterviewDTO> _validator;

        public InterviewsController(IInterviewService service, IValidator<InterviewDTO> validator)
        {
            _service = service;
            _validator = validator;
        }

        // GET: api/Interviews
        [AuthorizationAccess("hrm", "admin")]
        public IEnumerable<InterviewDTO> GetInterviews()
        {
            return _service.GetInterviews();
        }

        // GET: api/Interviews/5
        [Authorize]
        [ResponseType(typeof(Interview))]
        public IHttpActionResult GetInterview(Guid id)
        {
            var interview = _service.Read(id);
            if (interview == null)
            {
                return NotFound();
            }

            return Ok(interview);
        }

        // PUT: api/Interviews/5
        [Authorize]
        [ResponseType(typeof(void))]
        public IHttpActionResult PutInterview(Guid id, InterviewDTO interview)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != interview.Id)
            {
                return BadRequest("Id validation error");
            }

            var userName = ActionContext.RequestContext.Principal.Identity.Name;
            _service.Update(interview, userName);
            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Interviews
        [AuthorizationAccess("hrm", "admin")]
        [ResponseType(typeof(Interview))]
        public IHttpActionResult PostInterview(InterviewDTO interview)
        {
            if (interview == null)
            {
                return BadRequest();
            }

            var validation = _validator.Validate(interview);
            if (!validation.IsValid)
            {
                var errors = validation.Errors;
                return BadRequest(string.Join(". ", errors));
            }
            
            var result = _service.Create(interview);
            return CreatedAtRoute("DefaultApi", new { id = result.Id }, result);
        }
    }
}