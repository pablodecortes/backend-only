﻿using System.Web.Http;
using ExadelPractice.Abstract.Services;
using ExadelPractice.Filters.AuthorizationFilters;

namespace ExadelPractice.Controllers
{
    [RoutePrefix("api/users")]
    public class UsersController : ApiController
    {
        private readonly IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        // GET api/Account/UserInfo
        [HttpGet]
        [Route("UserInfo")]
        [Authorize]
        public IHttpActionResult GetUser()
        {
            var userName = ActionContext.RequestContext.Principal.Identity.Name;
            var user = _userService.GetUser(userName);

            return Ok(user);
        }

        [HttpGet]
        [AuthorizationAccess("admin", "hrm")]
        [Route("HRMs")]
        public IHttpActionResult GetHrms()
        {
            var hrms = _userService.GetHrms();

            return Ok(hrms);
        }

        [HttpGet]
        [AuthorizationAccess("admin", "hrm")]
        [Route("Techs")]
        public IHttpActionResult GetTechs()
        {
            var techs = _userService.GetTechs();

            return Ok(techs);
        }
    }
}
