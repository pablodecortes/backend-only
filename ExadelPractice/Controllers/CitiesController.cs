﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;
using ExadelPractice.Abstract.Services;
using ExadelPractice.Filters.AuthorizationFilters;
using ExadelPractice.Models;

namespace ExadelPractice.Controllers
{
    public class CitiesController : ApiController
    {
        private readonly ICityService _service;

        public CitiesController(ICityService service)
        {
            _service = service;
        }


        // admin & HR GET: api/Cities
        [AuthorizationAccess("hrm", "admin")]
        public IEnumerable<City> GetCities()
        {
            return _service.GetCities();
        }

        // admin GET: api/Cities/5
        [AuthorizationAccess("hrm", "admin")]
        [ResponseType(typeof(City))]
        public IHttpActionResult GetCity(Guid id)
        {
            City city = _service.Read(id);
            if (city == null)
            {
                return NotFound();
            }

            return Ok(city);
        }

        // admin POST: api/Cities
        [AuthorizationAccess("hrm", "admin")]
        [ResponseType(typeof(City))]
        public IHttpActionResult PostCity(City city)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _service.Create(city);
            return CreatedAtRoute("DefaultApi", new { id = city.Id }, city);
        }
    }
}