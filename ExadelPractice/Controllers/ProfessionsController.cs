﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;
using ExadelPractice.Abstract.Services;
using ExadelPractice.Filters.AuthorizationFilters;
using ExadelPractice.Models;

namespace ExadelPractice.Controllers
{
    public class ProfessionsController : ApiController
    {
        private readonly IProfessionService _service;

        public ProfessionsController(IProfessionService service)
        {
            _service = service;
        }

        // GET: api/Professions
        [AuthorizationAccess("hrm", "admin")]
        public IEnumerable<Profession> GetProfessions()
        {
            return _service.GetProfessions();
        }

        // GET: api/Professions/5
        [AuthorizationAccess("hrm", "admin")]
        [ResponseType(typeof(Profession))]
        public IHttpActionResult GetProfession(Guid id)
        {
            var profession = _service.Read(id);
            if (profession == null)
            {
                return NotFound();
            }

            return Ok(profession);
        }

        // POST: api/Professions
        [AuthorizationAccess("hrm", "admin")]
        [ResponseType(typeof(Profession))]
        public IHttpActionResult PostProfession(Profession profession)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _service.Create(profession);

            return CreatedAtRoute("DefaultApi", new { id = profession.Id }, profession);
        }
    }
}