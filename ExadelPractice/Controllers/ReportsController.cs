﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using ExadelPractice.Abstract.Services;
using ExadelPractice.Filters.ControllersFilters;
using FluentValidation;

namespace ExadelPractice.Controllers
{
    public class ReportsController : ApiController
    {
        private readonly ICandidateFilterService _candidateFilterService;
        private readonly IReportService _reportService;
        private readonly IValidator<CandidateFilter> _validator;

        public ReportsController(
            ICandidateFilterService candidateFilterService,
            IValidator<CandidateFilter> validator,
            IReportService reportService
        )
        {
            _candidateFilterService = candidateFilterService;
            _validator = validator;
            _reportService = reportService;
        }
        
        [Route("api/report/{mode}")]
        public IHttpActionResult GetReports(string mode, [FromUri] CandidateFilter filter)
        {
            var response = new HttpResponseMessage();

            if (filter == null)
            {
                filter = new CandidateFilter();
            }

            var validation = _validator.Validate(filter);
            if (!validation.IsValid)
            {
                var errors = validation.Errors;
                return BadRequest(string.Join(". ", errors));
            }

            try
            {
                var filterMode = mode.ToLower() == "active";
                var candidates = _candidateFilterService.FilterCandidates(filter, filterMode);
                var stream = _reportService.CreateReport(candidates);

                response.StatusCode = HttpStatusCode.OK;
                response.Content = new StreamContent(stream);
            }
            catch (NullReferenceException)
            {
                return BadRequest("Nothing was found with this filter.");
            }

            var currentTime = DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss");
            response.Content.Headers.ContentType =
                new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            response.Content.Headers.Add("Content-Disposition", $"attachment;filename=report{currentTime}.xlsx");

            return ResponseMessage(response);
        }
    }
}
