﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Castle.Core.Internal;
using ExadelPractice.Abstract.Services;
using ExadelPractice.DTOs;
using ExadelPractice.Filters.AuthorizationFilters;
using ExadelPractice.Filters.ControllersFilters;
using ExadelPractice.Models;
using FluentValidation;

namespace ExadelPractice.Controllers
{
    public class CandidatesController : ApiController
    {
        private readonly ICandidateService _candidateService;
        private readonly IValidator<CandidateFilter> _filterValidator;
        private readonly IValidator<CandidateDTO> _candidateValidator;

        public CandidatesController(ICandidateService candidateService,
            IValidator<CandidateFilter> filterValidator,
            IValidator<CandidateDTO> candidateValidator)
        {
            _candidateService = candidateService;
            _filterValidator = filterValidator;
            _candidateValidator = candidateValidator;
        }

        // admin & HR GET: api/Candidates/{mode}
        [AuthorizationAccess("hrm", "admin")]
        [Route("api/candidates/active")]
        public IHttpActionResult GetActiveCandidates([FromUri] CandidateFilter candidateFilter)
        {
            if (candidateFilter == null)
            {
                candidateFilter = new CandidateFilter();
            }

            var result = _filterValidator.Validate(candidateFilter);
            if (!result.IsValid)
            {
                var errors = result.Errors;
                return BadRequest(string.Join(". ", errors));
            }
            var candidates = _candidateService.FilterCandidates(candidateFilter, true);
            return Ok(candidates);
        }

        [AuthorizationAccess("hrm", "admin")]
        [Route("api/candidates/archive")]
        public IHttpActionResult GetArchiveCandidates([FromUri] CandidateFilter candidateFilter)
        {
            if (candidateFilter == null)
            {
                candidateFilter = new CandidateFilter();
            }

            var result = _filterValidator.Validate(candidateFilter);
            if (!result.IsValid)
            {
                var errors = result.Errors;
                return BadRequest(string.Join(". ", errors));
            }
            var candidates = _candidateService.FilterCandidates(candidateFilter, false);
            return Ok(candidates);
        }

        // admin & HR GET: api/Candidates/5
        [AuthorizationAccess("hrm", "admin")]
        [ResponseType(typeof(CandidateDTO))]
        public IHttpActionResult GetCandidate(Guid id)
        {
            var candidate = _candidateService.Read(id);

            if (candidate == null)
            {
                return NotFound();
            }

            return Ok(candidate);
        }

        // admin & HR PUT: api/Candidates/5
        [AuthorizationAccess("hrm", "admin")]
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCandidate(Guid id, CandidateDTO candidate)
        {
            if (candidate == null || id != candidate.Id)
            {
                return BadRequest();
            }

            var result = _candidateValidator.Validate(candidate);
            if (!result.IsValid)
            {
                var errors = result.Errors;
                return BadRequest(string.Join(". ", errors));
            }

            var userName = ActionContext.RequestContext.Principal.Identity.Name;
            _candidateService.Update(candidate, userName);
            return StatusCode(HttpStatusCode.NoContent);
        }

        [Authorize]
        [Route("api/candidates/skills/{id}")]
        [HttpPatch]
        public IHttpActionResult PatchCandidate(Guid id, InterviewSkillDTOArray interviewSkills)
        {
            if (!_candidateService.IsExists(id))
            {
                return NotFound();
            }

            if (interviewSkills.Skills == null)
            {
                return BadRequest("Skills are null.");
            }

            _candidateService.PatchCandidates(id, interviewSkills);

            return Ok();
        }

        // admin & HR POST: api/Candidates
        [AuthorizationAccess("hrm", "admin")]
        [ResponseType(typeof(CandidatesOrCandidateDTO))]
        public HttpResponseMessage PostCandidate(CandidateDTO candidate)
        {
            if (candidate == null)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            var result = _candidateValidator.Validate(candidate);
            if (!result.IsValid)
            {
                var errors = result.Errors;
                return Request.CreateResponse(HttpStatusCode.BadRequest, string.Join(". ", errors));
            }


            var userName = ActionContext.RequestContext.Principal.Identity.Name;
            var item = _candidateService.Create(candidate, userName);
            if (!item.candidates.IsNullOrEmpty())
            {
              return Request.CreateResponse(HttpStatusCode.Accepted, new
                {
                    Message = "Candidates found with similar name",
                    item.candidates
                });
            }
            return Request.CreateResponse(HttpStatusCode.Created, item.candidate);
        }

        [AuthorizationAccess("hrm", "admin")]
        [ResponseType(typeof(CandidateDTO))]
        [Route("api/candidates/force")]
        public HttpResponseMessage PostForceCandidate(CandidateDTO candidate)
        {
            if (candidate == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "");
            }

            var result = _candidateValidator.Validate(candidate);
            if (!result.IsValid)
            {
                var errors = result.Errors;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, string.Join(". ",errors));
            }

            var userName = ActionContext.RequestContext.Principal.Identity.Name;
            var item = _candidateService.CreateForce(candidate, userName);
            return Request.CreateResponse(HttpStatusCode.Created, item);
        }
    }
}