﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using ExadelPractice.Abstract.Services;
using ExadelPractice.Filters.AuthorizationFilters;
using ExadelPractice.Models;

namespace ExadelPractice.Controllers
{
    public class ResumeController : ApiController
    {
        private readonly IResumeService _resumeService;

        public ResumeController(IResumeService resumeService)
        {
            _resumeService = resumeService;
        }

        [HttpGet]
        [Route("api/resume/{id}")]
        public IHttpActionResult GetResume(Guid id)
        {
            try
            {
                var response = new HttpResponseMessage();
                var resume = _resumeService.Read(id);

                response.Content = new ByteArrayContent(Convert.FromBase64String(resume.Attachment));
                response.Content.Headers.Add("Content-Disposition", $"attachment;filename=resume.{resume.Extension}");
                response.Content.Headers.ContentType = new MediaTypeHeaderValue($"{resume.ContentType}");

                return ResponseMessage(response);
            }
            catch (NullReferenceException)
            {
                return NotFound();
            }
        }

        [HttpPost]
        [AuthorizationAccess("hrm", "admin")]
        [Route("api/resume")]
        public IHttpActionResult PostResume(Resume resume)
        {
            _resumeService.Post(resume);

            return Ok();
        }
    }
}
