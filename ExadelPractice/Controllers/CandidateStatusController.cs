﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ExadelPractice.Filters.AuthorizationFilters;
using ExadelPractice.Models;

namespace ExadelPractice.Controllers
{
    public class CandidateStatusController : ApiController
    {
        private readonly ExadelPracticeContext _db;

        public CandidateStatusController(ExadelPracticeContext db)
        {
            _db = db;
        }

        // GET: api/CandidateStatus
        [AuthorizationAccess("hrm", "admin")]
        public IQueryable<CandidateStatus> GetCandidateStatus()
        {
            return _db.CandidateStatus;
        }

        // GET: api/CandidateStatus/5
        [AuthorizationAccess("hrm", "admin")]
        [ResponseType(typeof(CandidateStatus))]
        public IHttpActionResult GetCandidateStatus(Guid id)
        {
            var candidateStatus = _db.CandidateStatus.Find(id);
            if (candidateStatus == null)
            {
                return NotFound();
            }

            return Ok(candidateStatus);
        }

        // PUT: api/CandidateStatus/5
        [AuthorizationAccess("hrm", "admin")]
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCandidateStatus(Guid id, CandidateStatus candidateStatus)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != candidateStatus.Id)
            {
                return BadRequest();
            }

            _db.Entry(candidateStatus).State = EntityState.Modified;

            try
            {
                _db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CandidateStatusExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/CandidateStatus
        [AuthorizationAccess("hrm", "admin")]
        [ResponseType(typeof(CandidateStatus))]
        public IHttpActionResult PostCandidateStatus(CandidateStatus candidateStatus)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _db.CandidateStatus.Add(candidateStatus);
            _db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = candidateStatus.Id }, candidateStatus);
        }

        // DELETE: api/CandidateStatus/5
        [AuthorizationAccess("hrm", "admin")]
        [ResponseType(typeof(CandidateStatus))]
        public IHttpActionResult DeleteCandidateStatus(Guid id)
        {
            var candidateStatus = _db.CandidateStatus.Find(id);
            if (candidateStatus == null)
            {
                return NotFound();
            }

            _db.CandidateStatus.Remove(candidateStatus);
            _db.SaveChanges();

            return Ok(candidateStatus);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CandidateStatusExists(Guid id)
        {
            return _db.CandidateStatus.Count(e => e.Id == id) > 0;
        }
    }
}