﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;
using ExadelPractice.Abstract.Services;
using ExadelPractice.Filters.AuthorizationFilters;
using ExadelPractice.Models;

namespace ExadelPractice.Controllers
{
    public class ProjectsController : ApiController
    {
        private readonly IProjectService _service;

        public ProjectsController(IProjectService service)
        {
            _service = service;
        }
        // GET: api/Projects
        [AuthorizationAccess("hrm", "admin")]
        public IEnumerable<Project> GetProjects()
        {
            return _service.GetProjects();
        }

        // GET: api/Projects/5
        [AuthorizationAccess("hrm", "admin")]
        [ResponseType(typeof(Project))]
        public IHttpActionResult GetProject(Guid id)
        {
            var project = _service.Read(id);
            if (project == null)
            {
                return NotFound();
            }

            return Ok(project);
        }

        // POST: api/Projects
        [AuthorizationAccess("hrm", "admin")]
        [ResponseType(typeof(Project))]
        public IHttpActionResult PostProject(Project project)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _service.Create(project);
            return CreatedAtRoute("DefaultApi", new { id = project.Id }, project);
        }
    }
}