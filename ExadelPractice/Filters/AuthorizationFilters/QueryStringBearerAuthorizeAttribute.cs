﻿using System.Security.Claims;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Microsoft.AspNet.SignalR.Owin;

namespace ExadelPractice.Filters.AuthorizationFilters
{
    public class QueryStringBearerAuthorizeAttribute : AuthorizeAttribute
    {
        public override bool AuthorizeHubConnection(HubDescriptor hubDescriptor, IRequest request)
        {
            var token = request.QueryString.Get("Bearer");
            var ticket = OAuthConfig.AuthServerOptions.AccessTokenFormat.Unprotect(token);

            if (ticket?.Identity == null || !ticket.Identity.IsAuthenticated)
            {
                return false;
            }

            request.Environment["User"] = new ClaimsPrincipal(ticket.Identity);

            return true;
        }

        public override bool AuthorizeHubMethodInvocation(IHubIncomingInvokerContext ctx, bool appliesToMethod)
        {
            var connectionId = ctx.Hub.Context.ConnectionId;
            var environment = ctx.Hub.Context.Request.Environment;
            var principal = environment["User"] as ClaimsPrincipal;

            if (principal?.Identity == null || !principal.Identity.IsAuthenticated)
            {
                return false;
            }

            var serverRequest = new ServerRequest(environment);
            ctx.Hub.Context = new HubCallerContext(serverRequest, connectionId);

            return true;
        }
    }
}