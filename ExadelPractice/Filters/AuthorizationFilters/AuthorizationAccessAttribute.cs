﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace ExadelPractice.Filters.AuthorizationFilters
{
    public class AuthorizationAccessAttribute : Attribute, IAuthorizationFilter
    {
        private readonly string[] _levels;
        public AuthorizationAccessAttribute(params string[] levels)
        {
            _levels = levels;
        }

        public Task<HttpResponseMessage> ExecuteAuthorizationFilterAsync(HttpActionContext actionContext,
            CancellationToken cancellationToken, Func<Task<HttpResponseMessage>> continuation)
        {
            var principal = actionContext.RequestContext.Principal as ClaimsPrincipal;

            if (principal == null || !principal.Identity.IsAuthenticated)
            {
                return Task.FromResult(actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized));
            }

            var accessLevel = GetAccessLevel(principal);

            if (accessLevel != null && _levels.Any(x => x == accessLevel))
            {
                return continuation();
            }
            return Task.FromResult(actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized));
        }

        private string GetAccessLevel(ClaimsPrincipal principal)
        {
            var claims = principal.Claims.ToDictionary(x => x.Type, x => x.Value);
            var accessLevel = claims[ClaimTypes.Role];

            return accessLevel;
        }
        public bool AllowMultiple => true;
    }
}