﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Hangfire.Dashboard;
using Microsoft.Owin;

namespace ExadelPractice.Filters.AuthorizationFilters
{
    public class HangfireAuthorizationFilter : IDashboardAuthorizationFilter
    {
        public bool Authorize(DashboardContext context)
        {
            var owinContext = new OwinContext(context.GetOwinEnvironment());

            var claims = owinContext.Authentication.User.Claims;

            return IsAdmin(claims);
        }

        private string GetAccessLevel(IEnumerable<Claim> claims)
        {
            var claimsDictionary = claims.ToDictionary(x => x.Type, x => x.Value);
            var accessLevel = claimsDictionary[ClaimTypes.Role];

            return accessLevel.ToLower();
        }

        private bool IsAdmin(IEnumerable<Claim> claims)
        {
            var accessLevel = GetAccessLevel(claims);

            return accessLevel == "admin";
        }
    }
}