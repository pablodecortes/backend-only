﻿using System;
using System.Collections.Generic;

namespace ExadelPractice.Filters.ControllersFilters
{
    public class CandidateFilter
    {
        public int Skip { get; set; } = 0;
        public int? Amount { get; set; }
        public string QueryString { get; set; }
        public List<Guid> Cities { get; set; }
        public List<Guid> Professions { get; set; }
        public List<Guid> Statuses { get; set; }
        public List<Guid> Skills { get; set; }
        public short Experience { get; set; } = 0;
        public short EnglishLevel { get; set; } = 0;
    }
}