﻿namespace ExadelPractice.Filters.ControllersFilters
{
    public class NotificationFilter
    {
        public int Skip { get; set; } = 0;
        public int Amount { get; set; } = 10;
    }
}