﻿using System;
using System.Collections.Generic;

namespace ExadelPractice.Filters.ControllersFilters
{
    public class PositionsFilter
    {
        public int Skip { get; set; } = 0;
        public int Amount { get; set; } = 10;
        public List<Guid> Cities { get; set; }
        public List<Guid> Professions { get; set; }
        public List<Guid> Statuses { get; set; }
        public short Experience { get; set; } = 0;
        public List<Guid> Skills { get; set; }
        public string QueryString { get; set; }
    }
}