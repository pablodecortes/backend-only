﻿using System.Web.Http;
using ExadelPractice.Backgrounds;
using Microsoft.Owin;
using Ninject;
using Owin;

[assembly: OwinStartup(typeof(ExadelPractice.API.Startup))]
namespace ExadelPractice.API
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var config = new HttpConfiguration();
            
            var kernel = NinjectConfig.CreateKernel();

            JsonSerializerConfig.Register(config);

            SignalRConfig.Register(app);
            
            OAuthConfig.Register(app, kernel);

            HangfireConfig.Register(app, kernel);

            AutomapperConfig.Register();

            WebApiConfig.Register(config);

            SwaggerConfig.Register(config);

            NinjectConfig.Register(app, config, kernel);

            var luceneIndexer = kernel.Get<LuceneIndexer>();
            luceneIndexer.InitBackgrounds();

            log4net.Config.XmlConfigurator.Configure();
        }
    }
}
