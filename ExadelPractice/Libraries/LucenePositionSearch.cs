﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ExadelPractice.Abstract.Repositories;
using ExadelPractice.Abstract.Services;
using ExadelPractice.DTOs;
using ExadelPractice.Filters.ControllersFilters;
using ExadelPractice.Models;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.QueryParsers;
using Lucene.Net.Search;
using Lucene.Net.Store;
using Version = Lucene.Net.Util.Version;

namespace ExadelPractice.Libraries
{
    public class LucenePositionSearch : IPositionSearch

    {
        private readonly IPositionRepository _repository;

        private const string DirectoryPath = @"C:\Lu\Pos";
        private static FSDirectory _directory;

        private static FSDirectory Directory
        {
            get
            {
                if (_directory == null) _directory = FSDirectory.Open(new DirectoryInfo(DirectoryPath));
                lock (_directory)
                {
                    if (IndexWriter.IsLocked(_directory)) IndexWriter.Unlock(_directory);

                    return _directory;
                }
            }
        }

        public LucenePositionSearch(IPositionRepository repository)
        {
            _repository = repository;
            Init();
        }

        public IEnumerable<ReducedPositionDTO> SearchPositions(PositionsFilter positionsFilter)
        {
            if (positionsFilter?.QueryString == null)
            {
                return _repository.GetAll().Select(AutoMapper.Mapper.Map<Position, ReducedPositionDTO>);
            }
            positionsFilter.QueryString = positionsFilter.QueryString.Replace(" ", "~ ");
            positionsFilter.QueryString = positionsFilter.QueryString + "~";
            return Search(positionsFilter.QueryString).Select(x => _repository.Read(x.Id))
                .Select(AutoMapper.Mapper.Map<Position, ReducedPositionDTO>);
        }

        public void Init()
        {

            AddUpdateLuceneIndex(_repository.GetLucenePositions());
        }

        private void AddToLuceneIndex(LucenePosition position, IndexWriter writer)
        {
            var searchQuery = new TermQuery(new Term("Id", position.Id.ToString()));
            writer.DeleteDocuments(searchQuery);

            var doc = new Document();

            doc.Add(new Field("Id", position.Id.ToString(), Field.Store.YES, Field.Index.NOT_ANALYZED));
            doc.Add(new Field("Header", position.Header, Field.Store.YES, Field.Index.ANALYZED));

            writer.AddDocument(doc);
        }

        public void AddUpdateLuceneIndex(IEnumerable<LucenePosition> positions)
        {
            Task.Run(() =>
            {
                var analyzer = new StandardAnalyzer(Version.LUCENE_30);
                using (var writer = new IndexWriter(Directory, analyzer, IndexWriter.MaxFieldLength.UNLIMITED))
                {
                    foreach (var item in positions)
                    {
                        AddToLuceneIndex(item, writer);
                    }
                    analyzer.Close();
                    writer.Dispose();
                }
            });
        }

        public void AddUpdateLuceneIndex(LucenePosition position)
        {
            AddUpdateLuceneIndex(new List<LucenePosition> {position});
        }

        public void RemoveRecord(Guid id)
        {
            var analyzer = new StandardAnalyzer(Version.LUCENE_30);
            using (var writer = new IndexWriter(Directory, analyzer, IndexWriter.MaxFieldLength.UNLIMITED))
            {
                var searchQuery = new TermQuery(new Term("Id", id.ToString()));
                writer.DeleteDocuments(searchQuery);

                analyzer.Close();
                writer.Dispose();
            }
        }

        public bool ClearIndex()
        {
            try
            {
                var analyzer = new StandardAnalyzer(Version.LUCENE_30);
                using (var writer = new IndexWriter(Directory, analyzer, true, IndexWriter.MaxFieldLength.UNLIMITED))
                {
                    writer.DeleteAll();

                    analyzer.Close();
                    writer.Dispose();
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        //Call when the DB is big and slow
        public void Optimize()
        {
            var analyzer = new StandardAnalyzer(Version.LUCENE_30);
            using (var writer = new IndexWriter(Directory, analyzer, IndexWriter.MaxFieldLength.UNLIMITED))
            {
                analyzer.Close();
                writer.Optimize();
                writer.Dispose();
            }
        }

        private LucenePosition MapToLucenePosition(Document doc)
        {
            return new LucenePosition()
            {
                Id = new Guid(Convert.ToString(doc.Get("Id"))),
                Header = Convert.ToString(doc.Get("Header"))
            };
        }

        private IEnumerable<LucenePosition> MapToDataList(IEnumerable<Document> hits)
        {
            return hits.Select(MapToLucenePosition).ToList();
        }

        private IEnumerable<LucenePosition> MapToDataList(IEnumerable<ScoreDoc> hits, IndexSearcher searcher)
        {
            return hits.Select(hit => MapToLucenePosition(searcher.Doc(hit.Doc))).ToList();
        }

        private Query ParseQuery(string searchQuery, QueryParser parser)
        {
            Query query;
            try
            {
                query = parser.Parse(searchQuery.Trim());
            }
            catch (ParseException)
            {
                query = parser.Parse(QueryParser.Escape(searchQuery.Trim()));
            }
            return query;
        }

        //Call when updating the db
        public void RecreateIndex()
        {
            ClearIndex();
            Init();
            Optimize();
        }

        public IEnumerable<LucenePosition> Search(string searchQuery, string searchField = "")
        {
            if (string.IsNullOrEmpty(searchQuery))
            {
                return _repository.GetLucenePositions();
            }

            using (var searcher = new IndexSearcher(Directory, false))
            {
                const int hitsLimit = 100;
                var analyzer = new StandardAnalyzer(Version.LUCENE_30);

                if (!string.IsNullOrEmpty(searchField))
                {
                    var parser = new QueryParser(Version.LUCENE_30, searchField, analyzer);
                    var query = ParseQuery(searchQuery, parser);
                    var hits = searcher.Search(query, hitsLimit).ScoreDocs;
                    var results = MapToDataList(hits, searcher);
                    analyzer.Close();
                    searcher.Dispose();
                    return results;
                }
                else
                {
                    var parser = new MultiFieldQueryParser
                        (Version.LUCENE_30, new[] {"id", "Header"}, analyzer);
                    var query = ParseQuery(searchQuery, parser);
                    var hits = searcher.Search
                        (query, null, hitsLimit, Sort.RELEVANCE)
                        .ScoreDocs;
                    var results = MapToDataList(hits, searcher);
                    analyzer.Close();
                    searcher.Dispose();
                    return results;
                }
            }
        }
    }
}