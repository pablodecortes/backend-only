﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ClosedXML.Excel;
using ExadelPractice.Abstract.Repositories;
using ExadelPractice.Models;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.QueryParsers;
using Lucene.Net.Search;
using Lucene.Net.Store;
using Version = Lucene.Net.Util.Version;

namespace ExadelPractice.Libraries
{
    public class LuceneCandidateSearch
    {
        private readonly ICandidateRepository _repository;

        private const string DirectoryPath = @"C:\Lu\Can";
        private FSDirectory _directory;
        private FSDirectory Directory
        {
            get
            {
                if (_directory == null) _directory = FSDirectory.Open(new DirectoryInfo(DirectoryPath));
                lock (_directory)
                {
                    if (IndexWriter.IsLocked(_directory)) IndexWriter.Unlock(_directory);
                  
                    return _directory;
                }
            }
        }

        public LuceneCandidateSearch(ICandidateRepository repository)
        {
            _repository = repository;
            Init();
        }

        public void Init()
        {
            AddUpdateLuceneIndex(_repository.GetLuceneCandidates());
        }

        private void AddToLuceneIndex(LuceneCandidate candidate, IndexWriter writer)
        {
            var searchQuery = new TermQuery(new Term("Id", candidate.Id.ToString()));
            writer.DeleteDocuments(searchQuery);

            var doc = new Document();

            doc.Add(new Field("Id", candidate.Id.ToString(), Field.Store.YES, Field.Index.NOT_ANALYZED));
            doc.Add(new Field("NameEng", candidate.NameEng, Field.Store.YES, Field.Index.ANALYZED));
            doc.Add(new Field("SurnameEng", candidate.SurnameEng, Field.Store.YES, Field.Index.ANALYZED));
            doc.Add(new Field("NameRus", candidate.NameRus, Field.Store.YES, Field.Index.ANALYZED));
            doc.Add(new Field("SurnameRus", candidate.SurnameRus, Field.Store.YES, Field.Index.ANALYZED));

            writer.AddDocument(doc);
        }

        public void AddUpdateLuceneIndex(IEnumerable<LuceneCandidate> candidates)
        {
            Task.Run(() =>
            {
                var analyzer = new StandardAnalyzer(Version.LUCENE_30);
                using (var writer = new IndexWriter(Directory, analyzer, IndexWriter.MaxFieldLength.UNLIMITED))
                {
                    candidates.ForEach(item => AddToLuceneIndex(item, writer));
                    analyzer.Close();
                    writer.Dispose();
                }
            });
        }

      public bool ClearIndex()
        {
            try
            {
                var analyzer = new StandardAnalyzer(Version.LUCENE_30);
                using (var writer = new IndexWriter(Directory, analyzer, true, IndexWriter.MaxFieldLength.UNLIMITED))
                {
                    writer.DeleteAll();

                    analyzer.Close();
                    writer.Dispose();
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
        //Call when the DB is big and slow
        public void Optimize()
        {
            var analyzer = new StandardAnalyzer(Version.LUCENE_30);
            using (var writer = new IndexWriter(Directory, analyzer, IndexWriter.MaxFieldLength.UNLIMITED))
            {
                analyzer.Close();
                writer.Optimize();
                writer.Dispose();
            }
        }

        private LuceneCandidate MapToLuceneCandidate(Document doc)
        {
            return new LuceneCandidate()
            {
                Id = new Guid(Convert.ToString(doc.Get("Id"))),
                NameEng = doc.Get("NameEng"),
                NameRus = doc.Get("NameRus"),
                SurnameEng = doc.Get("SurnameEng"),
                SurnameRus = doc.Get("SurnameRus")
            };
        }

        private IEnumerable<LuceneCandidate> MapToDataList(IEnumerable<ScoreDoc> hits,
            IndexSearcher searcher)
        {
            return hits.Select(hit => MapToLuceneCandidate(searcher.Doc(hit.Doc))).ToList();
        }

        private Query ParseQuery(string searchQuery, QueryParser parser)
        {
            Query query;
            try
            {
                query = parser.Parse(searchQuery.Trim());
            }
            catch (ParseException)
            {
                query = parser.Parse(QueryParser.Escape(searchQuery.Trim()));
            }
            return query;
        }

        //Call when updating the db
        public void RecreateIndex()
        {
            ClearIndex();
            Init();
            Optimize();
        }

        public IEnumerable<LuceneCandidate> Search(string searchQuery, string searchField = "")
        {
            if (string.IsNullOrEmpty(searchQuery))
            {
                var models = _repository.GetAllCandidates();
                return models.Select(AutoMapper.Mapper.Map<Candidate, LuceneCandidate>);
            }

            using (var searcher = new IndexSearcher(Directory, false))
            {
                var hits_limit = 100;
                var analyzer = new StandardAnalyzer(Version.LUCENE_30);

                if (!string.IsNullOrEmpty(searchField))
                {
                    var parser = new QueryParser(Version.LUCENE_30, searchField, analyzer);
                    var query = ParseQuery(searchQuery, parser);
                    var hits = searcher.Search(query, hits_limit).ScoreDocs;
                    var results = MapToDataList(hits, searcher);
                    analyzer.Close();
                    searcher.Dispose();
                    return results;
                }
                else
                {
                    var parser = new MultiFieldQueryParser
                        (Version.LUCENE_30, new []{"id", "NameEng", "NameRus", "SurnameEng", "SurnameRus"}, analyzer);
                    var query = ParseQuery(searchQuery, parser);
                    var hits = searcher.Search
                        (query, null, hits_limit, Sort.RELEVANCE)   
                        .ScoreDocs;
                    var results = MapToDataList(hits, searcher);
                    analyzer.Close();
                    searcher.Dispose();
                    return results;
                }
            }
        }
    }
}