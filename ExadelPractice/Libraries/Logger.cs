﻿using System;
using log4net;

namespace ExadelPractice.Libraries
{
    public class Logger
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(Object));

        public void Log(string str)
        {
            _log.Debug(str);
        }

        public void Log(Exception e)
        {
            _log.Debug(e);
        }
    }
}