﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using ExadelPractice.Abstract.Hubs;
using Microsoft.AspNet.SignalR;

namespace ExadelPractice.Implements.Hubs
{
    public class PushNotificationHub : Hub, IPushNotificationHub
    {
        public override Task OnConnected()
        {
            AssignToSecurityGroup();

            return base.OnConnected();
        }

        public override Task OnReconnected()
        {
            AssignToSecurityGroup();

            return base.OnReconnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            RemoveFromSecurityGroups();

            return base.OnDisconnected(stopCalled);
        }

        private void AssignToSecurityGroup()
        {
            var principal = Context.Request.Environment["User"] as ClaimsPrincipal;
            var userName = principal.Identity.Name.ToLower();

            Groups.Add(Context.ConnectionId, userName);
        }

        private void RemoveFromSecurityGroups()
        {
            var principal = Context.Request.Environment["User"] as ClaimsPrincipal;
            var userName = principal.Identity.Name.ToLower();

            Groups.Remove(Context.ConnectionId, userName);
        }
    }
}