﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using ExadelPractice.Abstract.Repositories;
using ExadelPractice.Models;

namespace ExadelPractice.Implements.Repositories
{
    public class CityRepository: ICityRepository
    {
        private readonly ExadelPracticeContext _db;
        private readonly DbSet<City> _cities;

        public CityRepository(ExadelPracticeContext db)
        {
            _db = db;
            _cities = _db.Cities;
        }
        public void Create(City item)
        {
            _cities.Add(item);
            _db.SaveChanges();
        }

        public City Read(Guid id)
        {
            return _cities.Find(id);
        }

        public IEnumerable<City> GetCities()
        {
            return _cities.ToList();
        }
    }
}