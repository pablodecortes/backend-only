﻿using ExadelPractice.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using ExadelPractice.Abstract.Repositories;

namespace ExadelPractice.Implements.Repositories
{
    public class AuthRepository : IAuthRepository
    {
        private readonly UserManager<User> _userManager;

        public AuthRepository(ExadelPracticeContext ctx)
        {
            _userManager = new UserManager<User>(new UserStore<User>(ctx));
        }

        public IdentityResult RegisterUser(User user)
        {
            var result = _userManager.Create(user, user.Password);

            return result;
        }

        public User FindUser(string userName, string password)
        {
            var user = _userManager.Find(userName, password);

            return user;
        }

        // todo : dispose
    }
}