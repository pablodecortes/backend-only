﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using ExadelPractice.Abstract.Repositories;
using ExadelPractice.Models;

namespace ExadelPractice.Implements.Repositories
{
    public class ProfessionRepository: IProfessionRepository
    {
        private readonly ExadelPracticeContext _db;
        private readonly DbSet<Profession> _professions;

        public ProfessionRepository(ExadelPracticeContext db)
        {
            _db = db;
            _professions = _db.Professions;
        }

        public void Create(Profession item)
        {
            _professions.Add(item);
            _db.SaveChanges();
        }

        public Profession Read(Guid id)
        {
            return _professions.Find(id);
        }


        public IEnumerable<Profession> GetProfessions()
        {
            return _professions.ToList();
        }
    }
}