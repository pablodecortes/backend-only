﻿using System;
using System.Data.Entity.Migrations;
using System.Linq;
using ExadelPractice.Abstract.Repositories;
using ExadelPractice.Models;

namespace ExadelPractice.Implements.Repositories
{
    public class ResumeRepository : IResumeRepository
    {
        private readonly ExadelPracticeContext _db;

        public ResumeRepository(ExadelPracticeContext db)
        {
            _db = db;
        }

        public void Post(Resume resume)
        {
            _db.Resumes.AddOrUpdate(r => r.CandidateId, resume);

            _db.SaveChanges();
        }

        public Resume Read(Guid candidateId)
        {
            var resume = _db.Resumes.FirstOrDefault(x => x.CandidateId == candidateId);

            return resume;
        }
    }
}