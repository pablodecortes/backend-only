﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using ExadelPractice.Abstract.Repositories;
using ExadelPractice.Models;

namespace ExadelPractice.Implements.Repositories
{
    public class InterviewRepository : IInterviewRepository
    {
        private readonly ExadelPracticeContext _db;

        public InterviewRepository(ExadelPracticeContext db)
        {
            _db = db;
        }

        public Interview Create(Interview item)
        {
            _db.Interviews.Add(item);
            _db.SaveChanges();
            return Read(item.Id);
        }

        public Interview Read(Guid id)
        {
            return LoadInterview().FirstOrDefault(x => x.Id == id);
        }

        public void Update(Interview item)
        {
            _db.Entry(Read(item.Id)).CurrentValues.SetValues(item);
            var c = Read(item.Id);
            c.CandidateInterview = item.CandidateInterview;
            c.InterviewSkills = UpdateCollection(c.InterviewSkills, item.InterviewSkills);
            _db.SaveChanges();
        }

        public ICollection<Interview> GetInterviews()
        {
            return LoadInterview().OrderByDescending(i => i.Date).ToList();
        }

        private IQueryable<Interview> LoadInterview()
        {
            return _db.Interviews
                .Include(x => x.CandidateInterview)
                .Include(x => x.CandidateInterview.Interview)
                .Include(x => x.InterviewSkills)
                .Include(x => x.InterviewSkills.Select(s => s.Skill));
        }

        private ICollection<T> UpdateCollection<T>(ICollection<T> oldColl, ICollection<T> newColl) // todo : user it in method above
        {
            if (oldColl == null)
            {
                return new List<T>(newColl);
            }
            oldColl.Clear();
            foreach (var item in newColl)
            {
                oldColl.Add(item);
            }
            return oldColl;
        }
    }
}