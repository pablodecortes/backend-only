﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using ExadelPractice.Abstract.Repositories;
using ExadelPractice.Models;
using System.Linq;
using Castle.Core.Internal;
using ExadelPractice.Abstract.Services;
using ExadelPractice.Filters.ControllersFilters;
using ExadelPractice.Libraries;

namespace ExadelPractice.Implements.Repositories
{
    public class PositionRepository : IPositionRepository
    {
        private readonly ExadelPracticeContext _db;

        public PositionRepository(ExadelPracticeContext db)
        {
            _db = db;
        }

        public Position Create(Position item)
        {
            _db.Positions.Add(item);
            _db.SaveChanges();
            return Read(item.Id);
        }

        public Position Read(Guid id)
        {
            var candidate = LoadPosition()
                .FirstOrDefault(c => c.Id == id);

            return candidate;
        }

        public void Update(Position item)
        {
            _db.Entry(Read(item.Id)).CurrentValues.SetValues(item);
            var c = Read(item.Id);
            c.CandidatePositions = item.CandidatePositions;
            c.PositionStatusLayer = item.PositionStatusLayer;
            c.PositionCity = item.PositionCity;
            c.PositionProfession = item.PositionProfession;
            c.Skills.Clear();
            foreach (var skill in item.Skills)
            {
                c.Skills.Add(skill);
            }
            if (c.CandidatePositions == null)
            {
                c.CandidatePositions = new List<CandidatePosition>();
            }
            c.CandidatePositions.Clear();
            foreach (var canPos in item.CandidatePositions)
            {
                c.CandidatePositions.Add(canPos);
            }
            if (c.PositionInterviews == null)
            {
                c.PositionInterviews = new List<PositionInterview>();
            }
            c.PositionInterviews.Clear();
            foreach (var interview in item.PositionInterviews)
            {
                c.PositionInterviews.Add(interview);
            }
            _db.SaveChanges();
        }

        public IEnumerable<Position> GetAll()
        {
            return LoadPosition();
        }
        public PositionsArray GetActivePositions(PositionsFilter filter, bool isActive)
        {
            var resultPositions = new PositionsArray();
            var positions = _db.Positions.Where(c => c != null)
                .Where(p => p.PositionStatusLayer.PositionStatus.IsActive == isActive)
                .Where(p => p.Experience >= filter.Experience).ToList();

            positions = FilterPositionBy(positions, filter).ToList();

            resultPositions.amount = positions.Count();
            resultPositions.positions = positions.OrderBy(p => p.RequestDate).Skip(filter.Skip).Take(filter.Amount);

            return resultPositions;
        }
        private IQueryable<Position> LoadPosition()
        {
            var positions = _db.Positions
                .Include(c => c.PositionCity)
                .Include(c => c.Skills)
                .Include(c => c.Skills.Select(e => e.Skill))
                .Include(c => c.PositionStatusLayer)
                .Include(c => c.PositionStatusLayer.PositionStatus)
                .Include(c => c.PositionProfession)
                .Include(c => c.PositionProfession.Position)
                .Include(c => c.CandidatePositions)
                .Include(c => c.PositionProject);

            return positions;
        }

        private IEnumerable<Position> FilterPositionBy(IEnumerable<Position> q, PositionsFilter f)
        {
            if (!f.Cities.IsNullOrEmpty())
            {
                q = q.Where(c => f.Cities.Contains(c.PositionCity.CityId));
            }

            if (!f.Professions.IsNullOrEmpty())
            {
                q = q.Where(c => f.Professions.Contains(c.PositionProfession.ProfessionId));
            }

            if (!f.Statuses.IsNullOrEmpty())
            {
                q = q.Where(c => f.Statuses.Contains(c.PositionStatusLayer.PositionStatus.Id));
            }

            if (f.Skills != null)
            {
                q = q.Where(c => f.Skills.All(s => c.Skills.Any(cs => s.Equals(cs.SkillId))));
            }

            return q;
        }

        public IEnumerable<LucenePosition> GetLucenePositions()
        {
            return _db.Positions.Select(x => new LucenePosition
            {
                Header = x.Header,
                Id = x.Id
            }).ToList();
        }
    }
}
