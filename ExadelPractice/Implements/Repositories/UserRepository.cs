﻿using System.Collections.Generic;
using System.Linq;
using ExadelPractice.Abstract.Repositories;
using ExadelPractice.Models;

namespace ExadelPractice.Implements.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly ExadelPracticeContext _db;

        public UserRepository(ExadelPracticeContext db)
        {
            _db = db;
        }

        public User GetUser(string userName)
        {
            var user = _db.Users.FirstOrDefault(x => x.UserName == userName);

            return user;
        }

        public ICollection<User> GetHrms()
        {
            var hrms = _db.Users.Where(x => x.AccessLevel == "hrm");

            return hrms.ToList();
        }

        public ICollection<User> GetTechs()
        {
            var techs = _db.Users.Where(x => x.AccessLevel == "tech");

            return techs.ToList();
        }

        public ICollection<User> GetAdmins()
        {
            var admins = _db.Users.Where(x => x.AccessLevel == "admin");

            return admins.ToList();
        }
    }
}