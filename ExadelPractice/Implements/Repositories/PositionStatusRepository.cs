﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Http;
using ExadelPractice.Abstract.Repositories;
using ExadelPractice.Models;

namespace ExadelPractice.Implements.Repositories
{
    public class PositionStatusRepository : ApiController, IPositionStatusRepository
    {

        private readonly ExadelPracticeContext _db;
        private readonly DbSet<PositionStatus> statuses;
        public PositionStatusRepository(ExadelPracticeContext db)
        {
            _db = db;
            statuses = _db.PositionStatus;
        }
        public PositionStatus Create(PositionStatus item)
        {
            statuses.Add(item);
            _db.SaveChanges();
            return Read(item.Id);
        }

        public PositionStatus Read(Guid id)
        {
            return _db.PositionStatus.Find(id);
        }

        public void Delete(Guid id)
        {
            PositionStatus status = statuses.Find(id);

            if (status == null)
            {
                throw new DbUpdateException();
            }

            statuses.Remove(status);
            _db.SaveChanges();
        }

        public IEnumerable<PositionStatus> GetStatuses()
        {
            return statuses.ToList();
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}