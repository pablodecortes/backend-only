﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web.Http;
using ExadelPractice.Abstract.Repositories;
using ExadelPractice.Models;

namespace ExadelPractice.Implements.Repositories
{
    public class SkillsRepository : ApiController, ISkillsRepository
    {
        private ExadelPracticeContext _db;
        

        public SkillsRepository(ExadelPracticeContext db)
        {
            _db = db;
        }
        public void Create(Skill item)
        {
            _db.Skills.Add(item);
            _db.SaveChanges();
        }

        public Skill Read(Guid id)
        {
            return _db.Skills.Find(id);
        }

        public IEnumerable<Skill> GetAllSkills()
        {
            return _db.Skills.ToList();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}