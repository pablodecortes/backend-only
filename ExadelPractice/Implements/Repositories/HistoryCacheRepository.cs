﻿using System;
using System.Collections.Generic;
using System.Linq;
using ExadelPractice.Abstract.Repositories;
using ExadelPractice.Models;

namespace ExadelPractice.Implements.Repositories
{
    public class HistoryCacheRepository: IHistoryCacheRepository
    {
        private readonly ExadelPracticeContext _db;

        public HistoryCacheRepository(ExadelPracticeContext db)
        {
            _db = db;
        }

        public void Clear()
        {
            var range = _db.HistoryCache;
            _db.HistoryCache.RemoveRange(range);
            _db.SaveChangesAsync();
        }

        public List<Guid> GetCandidatesCache()
        {
            return _db.HistoryCache
                .Where(c => c.CandidateId != null && c.CandidateId != Guid.Empty)
                .Select(c => c.CandidateId ?? Guid.Empty)
                .ToList();
        }

        public List<Guid> GetPositionsCache()
        {
            return _db.HistoryCache
                .Where(c => c.PositionId != null && c.PositionId != Guid.Empty)
                .Select(c => c.PositionId ?? Guid.Empty)
                .ToList();
        }

        public void AddCandidateId(Guid id)
        {
            var node = new HistoryCache {CandidateId = id};
            _db.HistoryCache.Add(node);
            _db.SaveChangesAsync();
        }

        public void AddPositionId(Guid id)
        {
            var node = new HistoryCache { PositionId = id };
            _db.HistoryCache.Add(node);
            _db.SaveChangesAsync();
        }
    }
}