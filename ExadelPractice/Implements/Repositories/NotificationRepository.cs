﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ExadelPractice.Abstract.Repositories;
using ExadelPractice.Filters.ControllersFilters;
using ExadelPractice.Models;

namespace ExadelPractice.Implements.Repositories
{
    public class NotificationRepository : INotificationRepository
    {
        private readonly ExadelPracticeContext _db;

        public NotificationRepository(ExadelPracticeContext db)
        {
            _db = db;
        }

        public ICollection<Notification> GetNotifications(NotificationFilter filter, string email)
        {
            var notifications = _db.Notifications
                .Where(x => x.UserEmail == email)
                .OrderByDescending(x => x.CreatedAt)
                .Skip(filter.Skip)
                .Take(filter.Amount);

            return notifications.ToList();
        }

        public async Task UpdateNotification(Notification notification)
        {
            _db.Entry(notification).State = EntityState.Modified;

            await _db.SaveChangesAsync();
        }

        public void PostNotification(Notification notification)
        {
            _db.Notifications.Add(notification);

            _db.SaveChanges();
        }
    }
}