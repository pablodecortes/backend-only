﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using ExadelPractice.Abstract.Repositories;
using ExadelPractice.Models;

namespace ExadelPractice.Implements.Repositories
{
    public class HistoryRepository : IHistoryRepository
    {
        private ExadelPracticeContext _db;

        public HistoryRepository(ExadelPracticeContext db)
        {
            _db = db;
        }

        public void Create(HistoryNode node)
        {
            _db.HistoryNodes.Add(node);
            _db.SaveChanges();
        }

        public IEnumerable<HistoryNode> GetHistoryNodes(int skip, int amount)
        {
            var nodes = _db.HistoryNodes
                .Include(c => c.Messages)
                .OrderByDescending(c => c.Date)
                .Skip(skip).Take(amount).ToList();

            return nodes;
        }

        public IEnumerable<HistoryNode> GetHistoryNodesForCandidate(Guid candidateId, int skip, int amount)
        {
            var nodes = _db.HistoryNodes
                .Include(c => c.Messages)
                .Where(c => c.CandidateId == candidateId)
                .OrderByDescending(c => c.Date)
                .Skip(skip).Take(amount).ToList();

            return nodes;
        }

        public IEnumerable<HistoryNode> GetHistoryNodesForPosition(Guid positionId, int skip, int amount)
        {
            var nodes = _db.HistoryNodes
                .Include(c => c.Messages)
                .Where(c => c.PositionId == positionId)
                .OrderByDescending(c => c.Date)
                .Skip(skip).Take(amount).ToList();

            return nodes;
        }
    }
}