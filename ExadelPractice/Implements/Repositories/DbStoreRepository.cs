﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading.Tasks;
using ExadelPractice.Abstract.Repositories;
using ExadelPractice.Models;

namespace ExadelPractice.Implements.Repositories
{
    public class DbStoreRepository : IDbStoreRepository
    {
        private readonly ExadelPracticeContext _db;

        public DbStoreRepository(ExadelPracticeContext db)
        {
            _db = db;
        }

        public async Task DeleteCredential(string key)
        {
            var item = _db.GoogleCredentials.FirstOrDefault(x => x.Key == key);
            if (item != null)
            {
                _db.GoogleCredentials.Remove(item);
                await _db.SaveChangesAsync();
            }
        }

        public GoogleCredential GetCredential(string key)
        {
            var credential = _db.GoogleCredentials.FirstOrDefault(x => x.Key == key);

            return credential;
        }

        public async Task ClearCredentials()
        {
            var objectContext = ((IObjectContextAdapter)_db).ObjectContext;
            await objectContext.ExecuteStoreCommandAsync("TRUNCATE TABLE [GoogleCredentials]");
        }

        public async Task StoreCredential(string key, string value)
        {
            var item = await _db.GoogleCredentials.SingleOrDefaultAsync(x => x.Key == key);

            if (item == null)
            {
                _db.GoogleCredentials.Add(new GoogleCredential { Key = key, Value = value });
            }
            else
            {
                item.Value = value;
            }

            await _db.SaveChangesAsync();
        }
    }
}