﻿using System;
using System.Collections.Generic;
using ExadelPractice.Models;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using Castle.Core.Internal;
using DocumentFormat.OpenXml.Drawing.Diagrams;
using ExadelPractice.Abstract.Repositories;
using ExadelPractice.DTOs;
using ExadelPractice.Filters.ControllersFilters;

namespace ExadelPractice.Implements.Repositories
{
    public class CandidateRepository : ICandidateRepository
    {
        private readonly ExadelPracticeContext _db;
        private readonly DbSet<Candidate> _candidates;

        public CandidateRepository(ExadelPracticeContext db)
        {
            _db = db;
            _candidates = _db.Candidates;
        }

        public IEnumerable<LuceneCandidate> GetLuceneCandidates()
        {
            var candidates = _candidates.ToList();
            return candidates.Select(AutoMapper.Mapper.Map<Candidate, LuceneCandidate>);
        }
        public Candidate Create(Candidate item)
        {
            _candidates.Add(item);
            _db.SaveChanges();
            return Read(item.Id);
        }

        public void Delete(Guid id)
        {
            Candidate candidate = _candidates.Find(id);

            if (candidate == null)
            {
                throw new DbUpdateException();
            }

            _candidates.Remove(candidate);
            _db.SaveChanges();
        }

        public ICollection<Candidate> GetAllCandidates()
        {
            return _db.Candidates.ToList();
        }

        public ICollection<Candidate> FilterCandidates(CandidateFilter filter, bool isActive)
        {
            IQueryable<Candidate> candidates = LoadCandidate()
                .Where(c => c.CandidateStatusLayer.CandidateStatus.IsActive == isActive)
                .Where(c => c.Experience >= filter.Experience)
                .Where(c => c.EnglishLevel >= filter.EnglishLevel);

            if (!filter.Cities.IsNullOrEmpty())
            {
                candidates = candidates.Where(c => filter.Cities.Contains(c.CandidateCity.CityId));
            }

            if (!filter.Professions.IsNullOrEmpty())
            {
                candidates = candidates.Where(c => filter.Professions.Contains(c.CandidateProfession.ProfessionId));
            }

            if (!filter.Statuses.IsNullOrEmpty())
            {
                candidates = candidates.Where(c => filter.Statuses.Contains(c.CandidateStatusLayer.CandidateStatus.Id));
            }

            if (filter.Skills.IsNullOrEmpty())
            {
                candidates = candidates.OrderBy(c => c.NextContact);
            }
            

            return candidates.ToList();
        }

        public Candidate Read(Guid id)
        {
            var candidate = LoadCandidate()
                .FirstOrDefault(c => c.Id == id);

            return candidate;
        }

        public void Patch(Guid id, ICollection<CandidateSkillDTO> skills)
        {
            var candidate = Read(id);
            foreach (var skill in skills)
            {
                var s = FindSKill(skill.Id, candidate);
                UpdateSkill(s, skill);
            }
            _db.SaveChanges();
        }

        private void UpdateSkill(CandidateSkill oldSkill, CandidateSkillDTO newSkill)
        {
            if (oldSkill == null || newSkill == null) return;
            oldSkill.Rating = newSkill.Rating;
            oldSkill.WasConfirmed = newSkill.WasConfirmed;
        }

        private static CandidateSkill FindSKill(Guid id, Candidate candidate)
        {
            return candidate.CandidateSkills.FirstOrDefault(s => s.SkillId == id);
        }

        public void Update(Candidate item)
        {
            _db.Entry(Read(item.Id)).CurrentValues.SetValues(item);
            var c = Read(item.Id);
            c.CandidateStatusLayer = item.CandidateStatusLayer;
            c.CandidateCity = item.CandidateCity;
            c.CandidateProfession = item.CandidateProfession;
            c.CandidateSkills.Clear();
            foreach (var skill in item.CandidateSkills)
            {
                c.CandidateSkills.Add(skill);
            }
            List<CandidateEmail> tmp = new List<CandidateEmail>(c.Email);
            foreach (var email in tmp)
            {
                _db.CandidateEmails.Remove(email);
            }
            foreach (var email in item.Email)
            {
                _db.CandidateEmails.Add(email);
            }
            if (c.CandidatePositions == null)
            {
                c.CandidatePositions = new List<CandidatePosition>();
            }
            c.CandidatePositions.Clear();
            foreach (var canPos in item.CandidatePositions)
            {
                c.CandidatePositions.Add(canPos);
            }
            //if (c.CandidateInterviews == null)
            //{
            //    c.CandidateInterviews = new List<CandidateInterview>();
            //}
            //c.CandidateInterviews.Clear();
            //foreach (var interview in item.CandidateInterviews)
            //{
            //    c.CandidateInterviews.Add(interview);
            //}
            _db.SaveChanges();
        }

        private IQueryable<Candidate> LoadCandidate()
        {
            return _db.Candidates
                .Include(c => c.Email)
                .Include(c => c.CandidateSkills)
                .Include(c => c.CandidateSkills.Select(e => e.Skill))
                .Include(c => c.CandidateCity)
                .Include(c => c.CandidateCity.City)
                .Include(c => c.CandidateProfession)
                .Include(c => c.CandidateProfession.Profession)
                .Include(c => c.CandidatePositions)
                .Include(c => c.CandidatePositions.Select(e => e.Position))
                .Include(c => c.CandidateStatusLayer)
                .Include(c => c.CandidateStatusLayer.CandidateStatus)
                .Include(c => c.CandidateInterviews)
                .Include(c => c.CandidateInterviews.Select(e => e.Interview));
        }

        private ICollection<T> UpdateCollection<T>(ICollection<T> oldColl, ICollection<T> newColl) // todo : user it in method above
        {
            if (oldColl == null)
            {
                return new List<T>(newColl);
            }
            oldColl.Clear();
            foreach (var item in newColl)
            {
                oldColl.Add(item);
            }
            return oldColl;
        }
    }
}