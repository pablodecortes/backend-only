﻿using System;
using System.Collections.Generic;
using System.Linq;
using ExadelPractice.Abstract.Repositories;
using ExadelPractice.Models;

namespace ExadelPractice.Implements.Repositories
{
    public class CandidateStatusRepository : ICandidateStatusRepository
    {
        private readonly ExadelPracticeContext _db;

        public CandidateStatusRepository(ExadelPracticeContext db)
        {
            _db = db;
        }

        public void Create(CandidateStatus item)
        {
            _db.CandidateStatus.Add(item);
            _db.SaveChanges();
        }

        public CandidateStatus Read(Guid id)
        {
            return _db.CandidateStatus.Find(id);
        }

        public IEnumerable<CandidateStatus> GetCandidateStatuses()
        {
            return _db.CandidateStatus.ToList();
        }
    }
}