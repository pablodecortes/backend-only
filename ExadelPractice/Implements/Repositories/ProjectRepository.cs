﻿using System;
using System.Collections.Generic;
using System.Linq;
using Castle.Components.DictionaryAdapter;
using ExadelPractice.Abstract.Repositories;
using ExadelPractice.Models;

namespace ExadelPractice.Implements.Repositories
{
    public class ProjectRepository: IProjectRepositoty
    {
        private readonly ExadelPracticeContext _db;

        public ProjectRepository(ExadelPracticeContext db)
        {
            _db = db;
        }
        public void Create(Project item)
        {
            _db.Projects.Add(item);
            _db.SaveChanges();
        }

        public Project Read(Guid id)
        {
            return _db.Projects.Find(id);
        }

        public IEnumerable<Project> GetProjects()
        {
            return _db.Projects.ToList();
        }
    }
}