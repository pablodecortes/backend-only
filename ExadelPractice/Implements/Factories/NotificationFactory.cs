﻿using System;
using System.Collections.Generic;
using System.Linq;
using ExadelPractice.Abstract.Factories;
using ExadelPractice.Abstract.Services;
using ExadelPractice.DTOs;
using ExadelPractice.Models;

namespace ExadelPractice.Implements.Factories
{
    public class NotificationFactory : INotificationFactory
    {
        private readonly IUserService _userService;

        public NotificationFactory(IUserService userService)
        {
            _userService = userService;
        }

        public ICollection<Notification> CreateInfoNotifications(string email, string route, Guid routeId)
        {
            var user = _userService.GetUser(email);

            var hrms = _userService.GetHrms();
            var admins = _userService.GetAdmins();
            var users = admins.Concat(hrms);

            var time = DateTime.Now;
            var collection = users.Select(u => new Notification
            {
                Type = "info",
                Title = $"{user.Name} {user.Surname} added a new {route}.",
                CreatedAt = time,
                RouteId = routeId,
                RouteType = route,
                UserEmail = u.Email
            });

            return collection.ToList();
        }

        public ICollection<Notification> CreateInterviewNotifications(string email, Guid interviewId)
        {
            var user = _userService.GetUser(email);

            var time = DateTime.Now;
            var userNotification = new Notification
            {
                Type = "interview",
                Title = "You were scheduled interview.",
                CreatedAt = time,
                UserEmail = user.Email,
                RouteId = interviewId,
                RouteType = "interview"
            };
            
            var admins = _userService.GetAdmins();
            var title = $"{user.Name} {user.Surname} was scheduled interview.";
            var notifications = CreateNotifications(admins, userNotification, title);

            return notifications;
        }

        public ICollection<Notification> CreateApproveNotifications(string email, Guid interviewId)
        {
            var user = _userService.GetUser(email);

            var time = DateTime.Now;
            var userNotification = new Notification
            {
                Type = "approve",
                Title = "Approve feedback from TS.",
                CreatedAt = time,
                UserEmail = user.Email,
                RouteId = interviewId,
                RouteType = "interview"
            };

            var admins = _userService.GetAdmins();
            var title = $"{user.Name} {user.Surname} needs to approve feedback from TS.";
            var notifications = CreateNotifications(admins, userNotification, title);

            return notifications;
        }

        public ICollection<Notification> CreateFeedBackNotifications(string email, Guid interviewId)
        {
            var user = _userService.GetUser(email);

            var time = DateTime.Now;
            var userNotification = new Notification
            {
                Type = "feedback",
                Title = "You need to fill the form.",
                CreatedAt = time,
                UserEmail = user.Email,
                RouteId = interviewId,
                RouteType = "interview"
            };

            var admins = _userService.GetAdmins();
            var title = $"{user.Name} {user.Surname} needs to fill the form.";
            var notifications = CreateNotifications(admins, userNotification, title);

            return notifications;
        }

        public ICollection<Notification> CreateAttentionNotification(string email, Guid candidateId)
        {
            var user = _userService.GetUser(email);

            var time = DateTime.Now;
            var userNotification = new Notification
            {
                Type = "interview",
                Title = "You need to contact with candidate.",
                CreatedAt = time,
                UserEmail = user.Email,
                RouteId = candidateId,
                RouteType = "candidate"
            };

            var admins = _userService.GetAdmins();
            var title = $"{user.Name} {user.Surname} needs to contact with candidate.";
            var notifications = CreateNotifications(admins, userNotification, title);

            return notifications;
        }

        private static ICollection<Notification> CreateNotifications(IEnumerable<UserDTO> users, Notification notification, string title)
        {
            var notifications = users.Select(u => ConvertNotification(notification, title, u.Email)).ToList();

            notifications.Add(notification);

            return notifications;
        }

        private static Notification ConvertNotification(Notification notification, string title, string email)
        {
            var convertedNotification = new Notification
            {
                Type = notification.Type,
                Title = title,
                CreatedAt = notification.CreatedAt,
                UserEmail = email,
                RouteId = notification.RouteId,
                RouteType = notification.RouteType
            };

            return convertedNotification;
        }
    }
}