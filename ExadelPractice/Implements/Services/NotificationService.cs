﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ExadelPractice.Abstract.Factories;
using ExadelPractice.Abstract.Repositories;
using ExadelPractice.Abstract.Services;
using ExadelPractice.Filters.ControllersFilters;
using ExadelPractice.Models;
using Hangfire;

namespace ExadelPractice.Implements.Services
{
    public class NotificationService : INotificationService
    {
        private readonly INotificationRepository _repository;
        private readonly INotificationFactory _notificationFactory;
        private readonly IPushNotificationService _pushNotification;

        public NotificationService(IPushNotificationService pushNotification, 
            INotificationRepository repository, INotificationFactory notificationFactory)
        {
            _pushNotification = pushNotification;
            _repository = repository;
            _notificationFactory = notificationFactory;
        }

        public NotificationService(INotificationRepository repository, INotificationFactory notificationFactory)
        {
            _repository = repository;
            _notificationFactory = notificationFactory;
        }

        public IEnumerable<Notification> GetNotifications(NotificationFilter filter, string email)
        {
            return _repository.GetNotifications(filter, email);
        }

        public async Task MarkNotificationAsRead(Notification notification, string userName)
        {
            await _repository.UpdateNotification(notification);
        }

        public void PostInfoNotifications(string email, string route, Guid routeId)
        {
            var notifications = _notificationFactory.CreateInfoNotifications(email, route, routeId);

            foreach (var notification in notifications)
            {
                _repository.PostNotification(notification);
                _pushNotification.SendNotification(notification, notification.UserEmail);
            }
        }

        public void PostInterviewNotifications(string email, Guid interviewId)
        {
            var notifications = _notificationFactory.CreateInterviewNotifications(email, interviewId);

            foreach (var notification in notifications)
            {
                _repository.PostNotification(notification);
                _pushNotification.SendNotification(notification, notification.UserEmail);
            }
        }

        public void PostApproveNotifications(string email, Guid intreviewId)
        {
            var notifications = _notificationFactory.CreateApproveNotifications(email, intreviewId);

            foreach (var notification in notifications)
            {
                _repository.PostNotification(notification);
                _pushNotification.SendNotification(notification, notification.UserEmail);
            }
        }

        public void PostFeedBackNotifications(string email, Guid interviewId, DateTime nextContact)
        {
            var notification = _notificationFactory.CreateFeedBackNotifications(email, interviewId);

            BackgroundJob.Schedule(
                () => SendBackgroundNotifications(notification),
                new DateTimeOffset(nextContact));
        }

        public void PostAttentionNotifications(string email, Guid candidateId, DateTime nextContact)
        {
            var notification = _notificationFactory.CreateAttentionNotification(email, candidateId);

            BackgroundJob.Schedule(
                () => SendBackgroundNotifications(notification),
                new DateTimeOffset(nextContact));
        }

        public void SendBackgroundNotifications(ICollection<Notification> notifications)
        {
            foreach (var notification in notifications)
            {
                _repository.PostNotification(notification);
                _pushNotification.SendNotification(notification, notification.UserEmail);
            }
        }
    }
}