﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Castle.Core.Internal;
using ExadelPractice.Abstract.Repositories;
using ExadelPractice.Abstract.Services;
using ExadelPractice.DTOs;
using ExadelPractice.Filters.ControllersFilters;
using ExadelPractice.Libraries;
using ExadelPractice.Models;

namespace ExadelPractice.Implements.Services
{
    public class CandidateFilterService : ICandidateFilterService
    {
        private readonly LuceneCandidateSearch _lSearch;
        private readonly ICandidateRepository _repository;

        public CandidateFilterService(LuceneCandidateSearch search, 
            ICandidateRepository repository)
        {
            _lSearch = search;
            _repository = repository;
        }

        public IEnumerable<CandidateDTO> FilterCandidates(CandidateFilter filter, bool isActive)
        {
            if (string.IsNullOrEmpty(filter?.QueryString))
            {
                var candidatesFromRepository = LoadCandidatesFromRepository(filter, isActive);
                return Sort(candidatesFromRepository, filter?.Skills);
            }

            var candidatesFromLucene = FindCandidates(filter?.QueryString);
            return FilterAfterLuceneSearch(candidatesFromLucene, filter, isActive);
        }

        private List<CandidateDTO> FilterAfterLuceneSearch(IEnumerable<CandidateDTO> candidates, CandidateFilter filter, bool isActive)
        {
            var candidateDtos = candidates as IList<CandidateDTO> ?? candidates.ToList();
            if (candidateDtos.IsNullOrEmpty()) return null;

            candidates = candidateDtos.Where(x => x != null).Where(c => c.Status.IsActive == isActive);
            if (!filter.Statuses.IsNullOrEmpty())
            {
                candidates = candidates.Where(c => filter.Statuses.Contains(c.Status.Id));
            }

            if (!filter.Cities.IsNullOrEmpty())
            {
                candidates = candidates.Where(c => filter.Cities.Contains(c.City.Id));
            }

            if (!filter.Professions.IsNullOrEmpty())
            {
                candidates = candidates.Where(c => filter.Professions.Contains(c.Profession.Id));
            }
            return candidates.ToList();
        }

        private IEnumerable<CandidateDTO> FindCandidates(string query)
        {
            var luceneQuery = query?.Replace(" ", "~0.6 ") + "~0.6";
            luceneQuery = luceneQuery + " OR " + luceneQuery?.Replace("~0.6", "*");
            var result = _lSearch.Search(luceneQuery);
            return result.Select(x => _repository.Read(x.Id))
                .Select(Mapper.Map<Candidate, CandidateDTO>);
        }

        private IEnumerable<CandidateDTO> LoadCandidatesFromRepository(CandidateFilter filter, bool isActive)
        {
            var models = _repository.FilterCandidates(filter, isActive);
            return models.Select(Mapper.Map<Candidate, CandidateDTO>);
        }

        private static IEnumerable<CandidateDTO> Sort(IEnumerable<CandidateDTO> candidates, IReadOnlyList<Guid> skills)
        {
            if (skills == null || skills.Count == 0) return candidates;

            var sortcandidates = candidates.OrderBy(c =>
            {
                var cs = c.Skills.FirstOrDefault(s => s.Id == skills.FirstOrDefault());
                return cs?.Rating ?? 0;
            });

            for (var i = 1; i < skills.Count - 1; i++)
            { 
                sortcandidates = sortcandidates.ThenBy(c =>
                {
                    var cs = c.Skills.FirstOrDefault(s => s.Id == skills[i]);
                    return cs?.Rating ?? 0;
                });
            }

            return sortcandidates.Reverse().AsEnumerable();
        }
    }
}
