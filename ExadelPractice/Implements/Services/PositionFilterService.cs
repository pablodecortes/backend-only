﻿using System;
using System.Collections.Generic;
using System.Linq;
using Castle.Core.Internal;
using ExadelPractice.Abstract.Repositories;
using ExadelPractice.Abstract.Services;
using ExadelPractice.DTOs;
using ExadelPractice.Filters.ControllersFilters;
using ExadelPractice.Libraries;
using ExadelPractice.Models;

namespace ExadelPractice.Implements.Services
{
    public class PositionFilterService : IPositionFilterService
    {
        private readonly LucenePositionSearch _lSearch;
        private readonly IPositionRepository _repository;

        public PositionFilterService(LucenePositionSearch lSearch,
            IPositionRepository repository)
        {
            _lSearch = lSearch;
            _repository = repository;
        }

        public PositionDTOArray FilterPositions(PositionsFilter filter, bool isActive)
        {
            var positions = LoadPositions(filter, isActive);
            if (positions.IsNullOrEmpty())
            {
                return new PositionDTOArray
                {
                    Amount = 0,
                    Positions = new List<ReducedPositionDTO>()
                };
            }
            var reducedPositionDtos = positions as IList<ReducedPositionDTO> ?? positions.ToList();
            return new PositionDTOArray
            {
                Amount = reducedPositionDtos.Count,
                Positions = reducedPositionDtos.Skip(filter.Skip).Take(filter.Amount).ToList()

            };
        }

        private IEnumerable<ReducedPositionDTO> LoadPositions(PositionsFilter filter, bool isActive)
        {
            return string.IsNullOrEmpty(filter?.QueryString)
                ? LoadPositionsFromRepository(filter, isActive)
                : FilterAfterLuceneSearch(FindPositions(filter.QueryString), filter, isActive);
        }

        private IEnumerable<ReducedPositionDTO> FilterAfterLuceneSearch(IEnumerable<ReducedPositionDTO> positions,
            PositionsFilter filter, bool isActive)
        {
            if (positions.IsNullOrEmpty()) return null;

            positions = positions.Where(x => x != null).Where(c => c.Status.IsActive == isActive);
            if (!filter.Statuses.IsNullOrEmpty())
            {
                positions = positions.Where(c => filter.Statuses.Contains(c.Status.Id));
            }

            if (!filter.Cities.IsNullOrEmpty())
            {
                positions = positions.Where(c => filter.Cities.Contains(c.City.Id));
            }

            if (!filter.Professions.IsNullOrEmpty())
            {
                positions = positions.Where(c => filter.Professions.Contains(c.Profession.Id));
            }
            return positions.Reverse().ToList();
        }

        private IEnumerable<ReducedPositionDTO> FindPositions(string query)
        {
            var luceneQuery = query?.Replace(" ", "~0.4") + "~0.4";
            var result = _lSearch.Search(luceneQuery);
            return result.Select(x => _repository.Read(x.Id))
                .Select(AutoMapper.Mapper.Map<Position, ReducedPositionDTO>).Reverse();
        }

        private IEnumerable<ReducedPositionDTO> LoadPositionsFromRepository(PositionsFilter filter, bool isActive)
        {
            var models = _repository.GetAll();
            models = models.Where(c => c.PositionStatusLayer.PositionStatus.IsActive == isActive);
            models = FilterPositionBy(models, filter);
            return models.Select(AutoMapper.Mapper.Map<Position, ReducedPositionDTO>);
        }

        private IEnumerable<Position> FilterPositionBy(IEnumerable<Position> q, PositionsFilter f)
        {
            if (f.Experience != 0)
            {
                q = q.Where(c => c.Experience >= f.Experience);
            }
            if (!f.Cities.IsNullOrEmpty())
            {
                q = q.Where(c => f.Cities.Contains(c.PositionCity.CityId));
            }

            if (!f.Professions.IsNullOrEmpty())
            {
                q = q.Where(c => f.Professions.Contains(c.PositionProfession.ProfessionId));
            }

            if (!f.Statuses.IsNullOrEmpty())
            {
                q = q.Where(c => f.Statuses.Contains(c.PositionStatusLayer.PositionStatusId));
            }

            if (f.Skills != null)
            {
                q = q.Where(c => f.Skills.All(s => c.Skills.Any(cs => s.Equals(cs.SkillId))));
            }

            return q;
        }
    }
}