﻿using System;
using System.Net;
using System.Net.Mail;
using ExadelPractice.Abstract.Services;
using Hangfire;

namespace ExadelPractice.Implements.Services
{
    public class EmailService : IEmailService
    {
        private readonly string _email = AppConfig.GetGmailName();
        private readonly string _pass = AppConfig.GetGmailPass();
        private const double BeforeDays = -1;
        private const double BeforeHours = -1;

        public void SendEmail(string email, string name, DateTime date)
        {
            var message = CreateMessage(email, name);

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(_email, _pass)
            };

            smtp.Send(message);
            BackgroundNotifications(email, name, date);
        }

        private MailMessage CreateMessage(string email, string name)
        {
            var message = new MailMessage(_email, email)
            {
                Subject = $"Hello, {email}! Postman Pechkin delivers.",
                Body = $"Hello! You have an interview with {name}. Event was added to your calendar."
            };

            return message;
        }
        
        public void BackgroundMessage(string email, string name)
        {
            var message = CreateMessage(email, name);

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(_email, _pass)
            };

            smtp.Send(message);
        }

        private void BackgroundNotifications(string email, string name, DateTime date)
        {
            BackgroundJob.Schedule(
                () => BackgroundMessage(email, name),
                new DateTimeOffset(date.AddDays(BeforeDays)));
            BackgroundJob.Schedule(
                () => BackgroundMessage(email, name),
                new DateTimeOffset(date.AddHours(BeforeHours)));
        }
    }
}