﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using ExadelPractice.Abstract.Repositories;
using ExadelPractice.Abstract.Services;
using ExadelPractice.DTOs;
using ExadelPractice.Models;

namespace ExadelPractice.Implements.Services
{
    public class PositionStatusService : IPositionStatusService
    {
        private readonly IPositionStatusRepository repository;

        public PositionStatusService(IPositionStatusRepository _repository)
        {
            repository = _repository;
        }
        public PositionStatusDTO Create(PositionStatusDTO item)
        {
            var _item = AutoMapper.Mapper.Map<PositionStatusDTO, PositionStatus>(item);
            var tmp = repository.Create(_item);
            return AutoMapper.Mapper.Map<PositionStatus, PositionStatusDTO>(tmp);
        }

        public PositionStatusDTO Read(Guid id)
        {
           return AutoMapper.Mapper.Map<PositionStatus, PositionStatusDTO>(repository.Read(id));
        }

        public void Delete(Guid id)
        {
            repository.Delete(id);
        }

        public bool IsExists(Guid id)
        {
            return (repository.Read(id) != null);
        }

        public IEnumerable<PositionStatusDTO> GetStatuses()
        {
            var arr = repository.GetStatuses();
            foreach (var item in arr)
            {
                yield return AutoMapper.Mapper.Map< PositionStatus, PositionStatusDTO>(item);
            }
        }
    }
}