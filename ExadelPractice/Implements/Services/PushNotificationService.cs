﻿using ExadelPractice.Abstract.Services;
using ExadelPractice.Implements.Hubs;
using ExadelPractice.Models;
using Microsoft.AspNet.SignalR;

namespace ExadelPractice.Implements.Services
{
    public class PushNotificationService : IPushNotificationService
    {
        private readonly IHubContext _hubContext = GlobalHost.ConnectionManager.GetHubContext<PushNotificationHub>();

        public void SendNotification(Notification notification, string userName)
        {
            _hubContext.Clients.Group(userName).displayMessage(notification);
        }
        
    }
}