﻿using System;
using System.Collections.Generic;
using System.Linq;
using ExadelPractice.Abstract.Repositories;
using ExadelPractice.Abstract.Services;
using ExadelPractice.DTOs;
using ExadelPractice.Filters.ControllersFilters;
using ExadelPractice.Models;

namespace ExadelPractice.Implements.Services
{
    public class PositionService: IPositionService
    {
        private readonly IPositionRepository _repository;
        private readonly INotificationService _notificationService;
        private readonly IHistoryService _history;
        private readonly IHistoryCacheRepository _cache;
        private readonly IPositionFilterService _filterService;

        public PositionService(IPositionRepository repository, 
            INotificationService notificationService,
            IHistoryService history,
            IHistoryCacheRepository cache,
            IPositionFilterService filterService)
        {
            _repository = repository;
            _notificationService = notificationService;
            _history = history;
            _cache = cache;
            _filterService = filterService;
        }
        public PositionDTO Create(PositionDTO item, string email)
        {
            var tmp = AutoMapper.Mapper.Map<PositionDTO, Position>(item);
            var createdPosition = _repository.Create(tmp);
            var resultPosition = AutoMapper.Mapper.Map< Position, PositionDTO>(createdPosition);

            _history.CreateForPosition(resultPosition, email);
            _cache.AddPositionId(resultPosition.Id);
            _notificationService.PostInfoNotifications(email, "position", createdPosition.Id);

            return resultPosition;
        }

        public PositionDTO Read(Guid id)
        {
            var model = _repository.Read(id);
            return AutoMapper.Mapper.Map<Position, PositionDTO>(model);
        }

        public void Update(PositionDTO item, string email)
        {
            var model = AutoMapper.Mapper.Map<PositionDTO, Position>(item);

            _history.CreateForPosition(Read(item.Id), item, email);
            _cache.AddCandidateId(model.Id);

            _repository.Update(model);
        }

        public PositionDTOArray GetActivePositions(PositionsFilter filter)
        {
            return _filterService.FilterPositions(filter, true);
        }

        public PositionDTOArray GetPositionsFromArchive(PositionsFilter filter)
        {
            return _filterService.FilterPositions(filter, false);
        }

        public bool IsExists(Guid id)
        {
            var pos = Read(id);
            return pos != null;
        }
    }
}