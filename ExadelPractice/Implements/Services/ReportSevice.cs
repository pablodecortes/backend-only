﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using static AutoMapper.Mapper;
using ExadelPractice.Abstract.Services;
using ExadelPractice.DTOs;

namespace ExadelPractice.Implements.Services
{
    public class ReportSevice : IReportService
    {
        private readonly IExcelService _excelService;

        public ReportSevice(IExcelService excelService)
        {
            _excelService = excelService;
        }

        public MemoryStream CreateReport(IEnumerable<CandidateDTO> candidates)
        {
            var reports = MapCollection(candidates);

            var workbook = _excelService.CreateXLBook(reports);

            var stream = _excelService.SaveAsStream(workbook);

            return stream;
        }

        private static IEnumerable<ReportDTO> MapCollection(IEnumerable<CandidateDTO> candidates)
        {
            return candidates.Select(Map<CandidateDTO, ReportDTO>);
        }
    }
}