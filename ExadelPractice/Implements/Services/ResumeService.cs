﻿using System;
using ExadelPractice.Abstract.Repositories;
using ExadelPractice.Abstract.Services;
using ExadelPractice.Models;

namespace ExadelPractice.Implements.Services
{
    public class ResumeService : IResumeService
    {
        private readonly IResumeRepository _repository;

        public ResumeService(IResumeRepository repository)
        {
            _repository = repository;
        }

        public void Post(Resume resume)
        {
            resume.ContentType = GetContentType(resume.Attachment);

            resume.Attachment = ConvertToBase64(resume.Attachment);

            _repository.Post(resume);
        }

        public Resume Read(Guid candidateId)
        {
            var resume = _repository.Read(candidateId);

            return resume;
        }

        private static string ConvertToBase64(string str)
        {
            var comma = str.IndexOf(",", StringComparison.Ordinal) + 1;
            var base64Length = str.Length - comma;
            var base64String = str.Substring(comma, base64Length);
            return base64String;
        }

        private static string GetContentType(string str)
        {
            var colon = str.IndexOf(":", StringComparison.Ordinal) + 1;
            var semicolon = str.IndexOf(";", StringComparison.Ordinal);
            var extensionLength = semicolon - colon;
            var extension = str.Substring(colon, extensionLength);
            return extension;
        }
    }
}