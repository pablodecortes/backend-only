﻿using System;
using System.Collections.Generic;
using ExadelPractice.Abstract.Repositories;
using ExadelPractice.Abstract.Services;
using ExadelPractice.Models;

namespace ExadelPractice.Implements.Services
{
    public class CandidateStatusService: ICandidateStatusService
    {
        private readonly ICandidateStatusRepository _repository;

        public CandidateStatusService(ICandidateStatusRepository repository)
        {
            _repository = repository;
        }
        public void Create(CandidateStatus item)
        {
            _repository.Create(item);
        }

        public CandidateStatus Read(Guid id)
        {
            return _repository.Read(id);
        }

        public IEnumerable<CandidateStatus> GetCandidateStatuses()
        {
            return _repository.GetCandidateStatuses();
        }

        public bool IsExists(Guid id)
        {
            var s = Read(id);
            return s != null;
        }
    }
}