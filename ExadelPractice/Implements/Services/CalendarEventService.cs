﻿using System;
using System.Collections.Generic;
using ExadelPractice.Abstract.Services;
using Google.Apis.Calendar.v3;
using Google.Apis.Calendar.v3.Data;
using Google.Apis.Services;

namespace ExadelPractice.Implements.Services
{
    public class CalendarEventService : ICalendarEventService
    {
        private readonly IGoogleAuthorizationService _googleAuthorizationService;

        public CalendarEventService(IGoogleAuthorizationService googleAuthorizationService)
        {
            _googleAuthorizationService = googleAuthorizationService;
        }

        public void AddCalendarEvent(string email, string name, DateTime date)
        {
            var credential = _googleAuthorizationService.ReceiveCredentials().Result;

            var service = new CalendarService(new BaseClientService.Initializer
            {
                HttpClientInitializer = credential,
                ApplicationName = "Interviewer Calendar",
            });

            var interview = CreateEvent(email, name, date);

            var eventService = service.Events.Insert(interview, "primary");
            eventService.SendNotifications = true;
            eventService.Execute();
        }

        private static Event CreateEvent(string email, string name, DateTime date)
        {
            var interview = new Event
            {
                Summary = $"You have an interview with {name}.",
                Location = "Exadel",
                Start = new EventDateTime
                {
                    DateTime = date,
                    TimeZone = "Europe/Minsk"
                },
                End = new EventDateTime
                {
                    DateTime = date,
                    TimeZone = "Europe/Minsk"
                },
                Attendees = new List<EventAttendee>
                {
                    new EventAttendee {Email = email}
                }
            };

            return interview;
        }
    }
}