﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Castle.Core.Internal;
using ExadelPractice.Abstract.Repositories;
using ExadelPractice.Abstract.Services;
using ExadelPractice.DTOs;
using ExadelPractice.Libraries;
using ExadelPractice.Models;

namespace ExadelPractice.Implements.Services
{
    public class CandidateUniqueVerification : ICandidateUniqueVerification
    {
        private readonly LuceneCandidateSearch _lSearch;
        private readonly ICandidateRepository _repository;

        public CandidateUniqueVerification(LuceneCandidateSearch lSearch,
            ICandidateRepository repository)
        {
            _lSearch = lSearch;
            _repository = repository;
        }
        public List<CandidateNote> Check(CandidateDTO item)
        {
            string query = item.NameEng + " " + item.SurnameEng
                           + " " + item.NameRus + " " + item.SurnameRus;
            query = query.Replace(" ", "~0.8 ") + "~0.8";

            var list = _lSearch.Search(query).ToList();

            return list.IsNullOrEmpty() ? new List<CandidateNote>() : list.Select(candidate => new CandidateNote
            {
                Id = candidate.Id,
                Name = candidate.NameEng,
                Surname = candidate.SurnameEng
            }).ToList();
        }
    }
}