﻿using System;
using System.Collections.Generic;
using ExadelPractice.Abstract.Repositories;
using ExadelPractice.Abstract.Services;
using ExadelPractice.Models;

namespace ExadelPractice.Implements.Services
{
    public class ProjectService: IProjectService
    {
        private readonly IProjectRepositoty _repositoty;

        public ProjectService(IProjectRepositoty repositoty)
        {
            _repositoty = repositoty;
        }

        public void Create(Project item)
        {
            _repositoty.Create(item);
        }

        public Project Read(Guid id)
        {
            return _repositoty.Read(id);
        }

        public IEnumerable<Project> GetProjects()
        {
            return _repositoty.GetProjects();
        }

        public bool IsExists(Guid id)
        {
            var p = _repositoty.Read(id);
            return p != null;
        }
    }
}