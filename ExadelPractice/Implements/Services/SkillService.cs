﻿using System;
using System.Collections.Generic;
using ExadelPractice.Abstract.Repositories;
using ExadelPractice.Abstract.Services;
using ExadelPractice.Implements.Repositories;
using ExadelPractice.Models;

namespace ExadelPractice.Implements.Services
{
    public class SkillService: ISkillsService
    {
        private readonly ISkillsRepository _repository;

        public SkillService(ISkillsRepository repository)
        {
            _repository = repository;
        }

        public void Create(Skill item)
        {
            _repository.Create(item);
        }

        public Skill Read(Guid id)
        {
            return _repository.Read(id);
        }

        public IEnumerable<Skill> GetSkills()
        {
            return _repository.GetAllSkills();
        }

        public bool IsExists(Guid id)
        {
            var s = Read(id);
            return s != null;
        }
    }
}