﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using Castle.Core.Internal;
using ExadelPractice.Abstract.Repositories;
using ExadelPractice.Abstract.Services;
using ExadelPractice.DTOs;
using ExadelPractice.Models;

namespace ExadelPractice.Implements.Services
{
    public class HistoryService : IHistoryService
    {
        private readonly IHistoryRepository _repository;
        private ICandidateStatusRepository _candidateStatus;
        private readonly IPositionStatusRepository _positionStatus;

        public HistoryService(IHistoryRepository repository, ICandidateStatusRepository candidateStatus,
            IPositionStatusRepository positionStatus)
        {
            _repository = repository;
            _candidateStatus = candidateStatus;
            _positionStatus = positionStatus;
        }

        public void CreateForCandidate(CandidateDTO oldCandidate, CandidateDTO newCandidate, string username = "")
        {
            Task.Run(() =>
            {
                HistoryNode history = new HistoryNode
                {
                    CandidateId = oldCandidate.Id,
                    Date = DateTime.Now,
                    Action = "Update",
                    Messages = new List<HistoryMessage>()
                };

                foreach (var property in typeof(CandidateDTO).GetProperties())
                {
                    if (property.GetValue(oldCandidate) != null)
                    {
                        if (!(property.GetValue(oldCandidate) is string) &&
                            !property.GetValue(oldCandidate).GetType().IsValueType)
                        {
                            continue;
                        }
                        if (!property.GetValue(oldCandidate).Equals(property.GetValue(newCandidate)))
                        {
                            history.Messages.Add(new HistoryMessage
                            {
                                Message = property.Name + " changed to: " +
                                          property.GetValue(newCandidate).ToString() + ". "
                            });
                        }
                    }
                    else if (property.GetValue(newCandidate) != null)
                    {
                        if (!(property.GetValue(newCandidate) is string) &&
                            !property.GetValue(newCandidate).GetType().IsValueType)
                        {
                            continue;
                        }
                        history.Messages.Add(new HistoryMessage
                        {
                            Message = property.Name + " added: " + property.GetValue(newCandidate).ToString() + ". "
                        });
                    }
                }
                history.Employee = username;
                if (!oldCandidate.Status.Id.Equals(newCandidate.Status.Id))
                {
                    history.Messages.Add(new HistoryMessage
                    {
                        Message = "Status changed to: " + _candidateStatus.Read(newCandidate.Status.Id).Name
                    });
                }
                history.Employee = username;
                if (!history.Messages.IsNullOrEmpty())
                {
                    _repository.Create(history);
                }
            });
        }

        public void CreateForCandidate(CandidateDTO newCandidate, string username)
        {
            Task.Run(() =>
            {
                var node = new HistoryNode
                {
                    Action = "Created",
                    CandidateId = newCandidate.Id,
                    Date = DateTime.Now,
                    Employee = username,
                    Messages = new List<HistoryMessage>(),
                };
                node.Messages.Add(new HistoryMessage
                {
                    Message = "Candidate created"
                });
                _repository.Create(node);
            });
        }

        public void CreateForPosition(PositionDTO oldPosition, PositionDTO newPosition, string username = "")
        {
            Task.Run(() =>
            {
                HistoryNode history = new HistoryNode
                {
                    PositionId = oldPosition.Id,
                    Date = DateTime.Now
                };
                if (!oldPosition.Status.Id.Equals(
                    newPosition.Status.Id))
                {
                    history.Messages.Add(new HistoryMessage
                    {
                        Message = "Status changed to: " + _positionStatus.Read(newPosition.Status.Id).Name
                    });
                }

                history.Action = "Update";
                history.Messages = new List<HistoryMessage>();
                foreach (var property in typeof(PositionDTO).GetProperties())
                {
                    if (property.GetValue(oldPosition) != null)
                    {
                        if (!(property.GetValue(oldPosition) is string) &&
                            !property.GetValue(oldPosition).GetType().IsValueType)
                        {
                            continue;
                        }
                        if (!property.GetValue(oldPosition).Equals(property.GetValue(newPosition)))
                        {
                            history.Messages.Add(new HistoryMessage
                            {
                                Message = property.Name + " changed to: " + property.GetValue(newPosition).ToString() +
                                          ". "
                            });
                        }
                    }
                    else if (property.GetValue(newPosition) != null)
                    {
                        if (!(property.GetValue(newPosition) is string) &&
                            !property.GetValue(newPosition).GetType().IsValueType)
                        {
                            continue;
                        }
                        history.Messages.Add(new HistoryMessage
                        {
                            Message = property.Name + " added: " + property.GetValue(newPosition).ToString() + ". "
                        });
                    }
                }
                history.Employee = username;

                history.Action = "Status Changed";
                history.Employee = username;

                if (!history.Messages.IsNullOrEmpty())
                {
                    _repository.Create(history);
                }
            });
        }

        public void CreateForPosition(PositionDTO newPosition, string username)
        {
            Task.Run(() =>
            {
                var node = new HistoryNode
                {
                    Action = "Created",
                    PositionId = newPosition.Id,
                    Date = DateTime.Now,
                    Employee = username,
                    Messages = new List<HistoryMessage>()
                };
                node.Messages.Add(new HistoryMessage
                {
                    Message = "Position Created"
                });
                _repository.Create(node);
            });
        }

        public IEnumerable<HistoryNode> GetHistoryNodes(int skip, int amount)
        {
            return _repository.GetHistoryNodes(skip, amount);
        }

        public IEnumerable<HistoryNode> GetHistoryNodesForCandidate(Guid candidateId, int skip, int amount)
        {
            return _repository.GetHistoryNodesForCandidate(candidateId, skip, amount);
        }

        public IEnumerable<HistoryNode> GetHistoryNodesForPosition(Guid positionId, int skip, int amount)
        {
            return _repository.GetHistoryNodesForPosition(positionId, skip, amount);
        }
    }
}
