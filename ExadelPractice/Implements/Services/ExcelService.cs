﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ClosedXML.Excel;
using ExadelPractice.Abstract.Services;
using ExadelPractice.DTOs;

namespace ExadelPractice.Implements.Services
{
    public class ExcelService : IExcelService
    {
        private const int TitlesIndex = 1;
        private const int ContentIndex = 2;

        public XLWorkbook CreateXLBook(IEnumerable<ReportDTO> reports)
        {
            var wb = new XLWorkbook();
            var ws = wb.Worksheets.Add("Report");

            ValidateCollection(reports);

            var reportDtos = reports as ReportDTO[] ?? reports.ToArray();
            var properties = GetProperties(reportDtos);
            CreateTitles(ws.Worksheet, properties);

            FillWorksheet(ws, reportDtos);

            ws.Columns().AdjustToContents();

            return wb;
        }

        public void FillWorksheet(IXLWorksheet ws, ReportDTO[] reports)
        {
            for (var i = 0; i < reports.Length; i++)
            {
                InsertCollection(ws, reports, i);
            }
        }

        public void InsertCollection(IXLWorksheet ws, ReportDTO[] reports, int iteration)
        {
            var properties = reports[iteration].GetType().GetProperties();
            for (var j = 0; j < properties.Length; j++)
            {
                var value = properties[j].GetValue(reports[iteration]);
                if (value is ICollection<string>)
                {
                    InsertToCell(ws, value as ICollection<string>, ContentIndex + iteration, TitlesIndex + j);
                }
                else
                {
                    ws.Cell(ContentIndex + iteration, TitlesIndex + j).Value = "'" + value;
                }
            }
        }

        public void InsertToCell(IXLWorksheet ws, ICollection<string> list, int row, int col)
        {
            var index = col;
            foreach (var item in list)
            {
                ws.Cell(row, index).Value = item;
                index++;
            }
        }

        public void CreateTitles(IXLWorksheet ws, string[] titles)
        {
            for (var i = 0; i < titles.Length; i++)
            {
                ws.Cell(1, TitlesIndex + i).Style.Font.Bold = true;
                ws.Cell(1, TitlesIndex + i).Value = titles[i];
            }
        }

        public MemoryStream SaveAsStream(XLWorkbook wb)
        {
            var stream = new MemoryStream();

            wb.SaveAs(stream);
            wb.Dispose();

            stream.Position = 0;

            return stream;
        }

        private void ValidateCollection(IEnumerable<ReportDTO> collection)
        {
            var obj = collection.FirstOrDefault();
            if (obj == null)
            {
                throw new NullReferenceException();
            }
        }

        private string[] GetProperties(ReportDTO[] reports)
        {
            var obj = reports.FirstOrDefault();
            return obj.GetType().GetProperties().Select(x => x.Name).ToArray();
        }
    }
}