﻿using ExadelPractice.Abstract.Repositories;
using ExadelPractice.Models;
using ExadelPractice.Abstract.Services;
using ExadelPractice.DTOs;
using Microsoft.AspNet.Identity;

namespace ExadelPractice.Implements.Services
{
    public class AuthService : IAuthService
    {
        private readonly IAuthRepository _repository;

        public AuthService(IAuthRepository repository)
        {
            _repository = repository;
        }

        public IdentityResult RegisterUser(User user)
        {
            var result = _repository.RegisterUser(user);

            return result;
        }

        public UserDTO FindUser(string userName, string password)
        {
            var user = _repository.FindUser(userName, password);

            return AutoMapper.Mapper.Map<User, UserDTO>(user);
        }
    }
}