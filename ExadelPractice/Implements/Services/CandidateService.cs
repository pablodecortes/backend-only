﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Castle.Core.Internal;
using ExadelPractice.Abstract.Repositories;
using ExadelPractice.Abstract.Services;
using ExadelPractice.DTOs;
using ExadelPractice.Filters.ControllersFilters;
using ExadelPractice.Models;

namespace ExadelPractice.Implements.Services
{
    /// <summary>
    /// CandidateService service 
    /// </summary>
    public class CandidateService : ICandidateService
    {
        private readonly ICandidateRepository _repository;
        private readonly ICandidateUniqueVerification _verification;
        private readonly INotificationService _notificationService;
        private readonly IHistoryService _history;
        private readonly IHistoryCacheRepository _cache;
        private readonly ICandidateFilterService _filterService;

        public CandidateService(ICandidateRepository repository,
            ICandidateUniqueVerification verification,
            INotificationService notificationService,
            IHistoryService history,
            IHistoryCacheRepository cache,
            ICandidateFilterService filterService)
        {
            _repository = repository;
            _verification = verification;
            _notificationService = notificationService;
            _history = history;
            _cache = cache;
            _filterService = filterService;
        }

        public CandidatesOrCandidateDTO Create(CandidateDTO newCandidate, string email)
        {
            var checkedIds = _verification.Check(newCandidate);
            if (!checkedIds.IsNullOrEmpty())
            {
                return new CandidatesOrCandidateDTO {candidates = checkedIds};
            }
            var item = Mapper.Map<CandidateDTO, Candidate>(newCandidate);
            item.HR = email;
            var createdCandidate = _repository.Create(item);

            _history.CreateForCandidate(Mapper.Map<Candidate, CandidateDTO>(createdCandidate), email);
            _cache.AddCandidateId(createdCandidate.Id);
            _notificationService.PostInfoNotifications(email, "candidate", createdCandidate.Id);

            return new CandidatesOrCandidateDTO
            {
                candidate = Mapper.Map<Candidate, CandidateDTO>(createdCandidate),
                candidates = new List<CandidateNote>()
            };
        }

        public CandidateDTO CreateForce(CandidateDTO item, string email)
        {
            var model = Mapper.Map<CandidateDTO, Candidate>(item);
            model.HR = email;
            var response = _repository.Create(model);

            _history.CreateForCandidate(Mapper.Map<Candidate, CandidateDTO>(model), email);
            _cache.AddCandidateId(response.Id);
            _notificationService.PostInfoNotifications(email, "candidate", response.Id);

            return Mapper.Map<Candidate, CandidateDTO>(response);
        }

        public CandidateDTO Read(Guid id)
        {
            return Mapper.Map<Candidate, CandidateDTO>(_repository.Read(id));
        }

        public void Update(CandidateDTO candidate, string email)
        {
            var model = Mapper.Map<CandidateDTO, Candidate>(candidate);
            model.HR = email;
            var oldCandidate = Read(model.Id);
            _history.CreateForCandidate(oldCandidate, candidate, email);
            _repository.Update(model);
            _cache.AddCandidateId(model.Id);
        }

        public bool IsExists(Guid id)
        {
            var c = Read(id);
            return c != null;
        }

        public void PatchCandidates(Guid id, InterviewSkillDTOArray interviewSkills)
        {
            _repository.Patch(id, interviewSkills.Skills);
        }

        public CandidateDTOArray FilterCandidates(CandidateFilter filter, bool isActive)
        {
            var candidates = _filterService.FilterCandidates(filter, isActive);
            return ConvertToCandidateDtoArray(candidates, filter?.Skip, filter?.Amount);
        }

        private static CandidateDTOArray ConvertToCandidateDtoArray(
            IEnumerable<CandidateDTO> candidates,
            int? skip,
            int? amount)
        {
            if (candidates == null)
            {
                return new CandidateDTOArray();
            }

            var candidateDtos = candidates as IList<CandidateDTO> ?? candidates.ToList();
            var reducedCandidates = candidateDtos.Skip(skip ?? 0);

            if (amount != null)
            {
                reducedCandidates = reducedCandidates.Take((int) amount);
            }

            var reducedCandidateDtos = reducedCandidates
                .Select(Mapper.Map<CandidateDTO, ReducedCandidateDTO>)
                .ToList();

            return new CandidateDTOArray
            {
                Candidates = reducedCandidateDtos,
                Amount = candidateDtos.Count
            };
        }
    }
}