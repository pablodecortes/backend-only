﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ExadelPractice.Abstract.Repositories;
using ExadelPractice.Abstract.Services;
using ExadelPractice.Implements.Repositories;
using ExadelPractice.Models;

namespace ExadelPractice.Implements.Services
{
    public class SkillTreeBuilderService : ISkillsTreeable
    {
        private readonly Dictionary<Guid, SkillNode> _dictionary;
        private readonly ISkillsRepository _repository;

        public SkillTreeBuilderService(ISkillsRepository repository)
        {
            _repository = repository;
            _dictionary = new Dictionary<Guid, SkillNode>();
        }

        public SkillNode GetSkillTree()
        {
            initDictionary(_repository.GetAllSkills());
            var head = new SkillNode
            {
                Name = "All skills",
                Children = new LinkedList<SkillNode>()
            };

            foreach (var item in _dictionary)
            {
                if (item.Value.ParentId == null)
                {
                    head.Children.Add(item.Value);
                }
                else
                {
                    var id = ConvertGuid(item.Value.ParentId);
                    if (_dictionary[id].Children == null)
                    {
                        _dictionary[id].Children = new List<SkillNode>();
                    }
                    _dictionary[id].Children.Add(item.Value);
                }
            }

            return head;
        }

        private void initDictionary(IEnumerable<Skill> skills)
        {
            _dictionary.Clear();
            foreach (var item in skills)
            {
                var skillNode = new SkillNode
                {
                    Id = item.Id,
                    Name = item.Name,
                    ParentId = item.ParentId
                };

                _dictionary.Add(item.Id, skillNode);
            }
        }

        private Guid ConvertGuid(Guid? source)
        {
            return source ?? Guid.Empty;
        }

    }
}