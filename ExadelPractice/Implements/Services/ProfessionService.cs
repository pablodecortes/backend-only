﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ExadelPractice.Abstract.Repositories;
using ExadelPractice.Abstract.Services;
using ExadelPractice.Implements.Repositories;
using ExadelPractice.Models;

namespace ExadelPractice.Implements.Services
{
    public class ProfessionService: IProfessionService
    {
        private readonly IProfessionRepository _repository;

        public ProfessionService(IProfessionRepository repository)
        {
            _repository = repository;
        }
        public void Create(Profession item)
        {
            _repository.Create(item);
        }

        public Profession Read(Guid id)
        {
            return _repository.Read(id);
        }

        public IEnumerable<Profession> GetProfessions()
        {
            return _repository.GetProfessions();
        }

        public bool IsExists(Guid id)
        {
            var res = Read(id);
            return res != null;
        }
    }
}