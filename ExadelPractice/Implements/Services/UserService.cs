﻿using System.Collections.Generic;
using System.Linq;
using ExadelPractice.Abstract.Repositories;
using ExadelPractice.Abstract.Services;
using ExadelPractice.DTOs;
using ExadelPractice.Models;

namespace ExadelPractice.Implements.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _repository;

        public UserService(IUserRepository repository)
        {
            _repository = repository;
        }

        public UserDTO GetUser(string userName)
        {
            var user = _repository.GetUser(userName);

            return ConvertUser(user);
        }

        public IEnumerable<UserDTO> GetHrms()
        {
            var hrms = _repository.GetHrms();

            return ConvertUsers(hrms);
        }

        public IEnumerable<UserDTO> GetTechs()
        {
            var techs = _repository.GetTechs();

            return ConvertUsers(techs);
        }

        public IEnumerable<UserDTO> GetAdmins()
        {
            var admins = _repository.GetAdmins();

            return ConvertUsers(admins);
        }

        public bool IsExists(string email)
        {
            var user = GetUser(email);
            return user != null;
        }

        private UserDTO ConvertUser(User user)
        {
            return AutoMapper.Mapper.Map<User, UserDTO>(user);
        }

        private IEnumerable<UserDTO> ConvertUsers(ICollection<User> collection)
        {
            return collection.Select(AutoMapper.Mapper.Map<User, UserDTO>);
        }
    }
}