﻿using System.IO;
using System.Threading;
using System.Threading.Tasks;
using ExadelPractice.Abstract.Services;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Calendar.v3;
using Google.Apis.Util.Store;

namespace ExadelPractice.Implements.Services
{
    public class GoogleAuthorizationService : IGoogleAuthorizationService
    {
        private readonly string _id;
        private readonly string _secret;
        private readonly IDataStore _dbStore;

        public GoogleAuthorizationService(IDataStore dbStore)
        {
            _id = AppConfig.GetGoogleId();
            _secret = AppConfig.GetGoogleSecret();
            _dbStore = dbStore;
        }

        public async Task<UserCredential> ReceiveCredentials()
        {
            var credential = await GoogleWebAuthorizationBroker.AuthorizeAsync(
                new ClientSecrets
                {
                    ClientId = _id,
                    ClientSecret = _secret,
                },
                new[] {CalendarService.Scope.Calendar},
                "user",
                CancellationToken.None,
                _dbStore);

            return credential;
        }
    }
}