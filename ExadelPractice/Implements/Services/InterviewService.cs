﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ExadelPractice.Abstract.Repositories;
using ExadelPractice.Abstract.Services;
using ExadelPractice.DTOs;
using ExadelPractice.Models;

namespace ExadelPractice.Implements.Services
{
    public class InterviewService: IInterviewService
    {
        private readonly IInterviewRepository _repository;
        private readonly INotificationService _notificationService;
        private readonly IEmailService _emailService;
        private readonly ICandidateService _candidateService;
        private readonly ICalendarEventService _calendarEventService;
        private readonly IUserService _userService;

        public InterviewService(IInterviewRepository repository, INotificationService notificationService,
            IEmailService emailService, ICandidateService candidateService, IUserService userService,
            ICalendarEventService calendarEventService)
        {
            _repository = repository;
            _notificationService = notificationService;
            _emailService = emailService;
            _candidateService = candidateService;
            _userService = userService;
            _calendarEventService = calendarEventService;
        }


        public InterviewDTO Create(InterviewDTO item)
        {
            var model = AutoMapper.Mapper.Map<InterviewDTO, Interview>(item);
            var result = _repository.Create(model);

            SendInterviewNotification(model);

            return AutoMapper.Mapper.Map<Interview, InterviewDTO>(result);
        }

        public InterviewDTO Read(Guid id)
        {
            var model = _repository.Read(id);
            return AutoMapper.Mapper.Map<Interview, InterviewDTO>(model);
        }

        public void Update(InterviewDTO item, string userName)
        {
            var model = AutoMapper.Mapper.Map<InterviewDTO, Interview>(item);

            SendApproveNotification(model, userName);

            _repository.Update(model);
        }

        public ICollection<InterviewDTO> GetInterviews()
        {
            var models = _repository.GetInterviews();
            return models.Select(AutoMapper.Mapper.Map<Interview, InterviewDTO>).ToList();
        }

        public bool IsExists(Guid id)
        {
            var inter = _repository.Read(id);
            return inter != null;
        }

        private void SendApproveNotification(Interview interview, string userName)
        {
            var user = _userService.GetUser(userName);

            if (user.AccessLevel == "tech" && userName == interview.InterviewerEmail)
            {
                _notificationService.PostApproveNotifications(interview.HrmEmail, interview.Id);
            }
        }

        private void SendInterviewNotification(Interview interview)
        {
            var candidate = _candidateService.Read(interview.CandidateInterview.CandidateId);
            var candidateFullName = $"{candidate.NameEng} {candidate.SurnameEng}";

            _emailService.SendEmail(interview.InterviewerEmail, candidateFullName, interview.Date);
            _notificationService.PostInterviewNotifications(interview.InterviewerEmail, interview.Id);
            _notificationService.PostFeedBackNotifications(interview.InterviewerEmail, interview.Id, interview.Date);
            _calendarEventService.AddCalendarEvent(interview.InterviewerEmail, candidateFullName, interview.Date);
        }
    }
}