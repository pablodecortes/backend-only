﻿using System;
using System.Collections.Generic;
using ExadelPractice.Abstract.Repositories;
using ExadelPractice.Abstract.Services;
using ExadelPractice.Models;

namespace ExadelPractice.Implements.Services
{
    public class CityService: ICityService
    {
        private ICityRepository _repository;

        public CityService(ICityRepository repository)
        {
            _repository = repository;
        }
        public void Create(City item)
        {
            _repository.Create(item);
        }

        public City Read(Guid id)
        {
            return _repository.Read(id);
        }

        public IEnumerable<City> GetCities()
        {
            return _repository.GetCities();
        }

        public bool IsExists(Guid id)
        {
            var city = Read(id);
            return city != null;
        }
    }
}