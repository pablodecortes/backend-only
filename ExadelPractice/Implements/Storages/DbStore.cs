﻿using System;
using System.Threading.Tasks;
using ExadelPractice.Abstract.Repositories;
using Google.Apis.Util.Store;
using Newtonsoft.Json;

namespace ExadelPractice.Implements.Storages
{
    public class DbStore : IDataStore
    {
        private readonly IDbStoreRepository _repository;

        public DbStore(IDbStoreRepository repository)
        {
            _repository = repository;
        }

        public async Task ClearAsync()
        {
            await _repository.ClearCredentials();
        }

        public async Task DeleteAsync<T>(string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                throw new ArgumentException("Key must have a value");
            }

            var generatedKey = GenerateStoredKey(key, typeof(T));
            await _repository.DeleteCredential(generatedKey);
        }

        public Task<T> GetAsync<T>(string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                throw new ArgumentException("Key must have a value");
            }

            var generatedKey = GenerateStoredKey(key, typeof(T));
            var item = _repository.GetCredential(generatedKey);
            var value = item == null ? default(T) : JsonConvert.DeserializeObject<T>(item.Value);
            return Task.FromResult(value);
        }

        public async Task StoreAsync<T>(string key, T value)
        {
            if (string.IsNullOrEmpty(key))
            {
                throw new ArgumentException("Key must have a value");
            }

            var generatedKey = GenerateStoredKey(key, typeof(T));
            var serializedValue = JsonConvert.SerializeObject(value);

            await _repository.StoreCredential(generatedKey, serializedValue);
        }

        private static string GenerateStoredKey(string key, Type t)
        {
            return $"{t.FullName}-{key}";
        }
    }
}