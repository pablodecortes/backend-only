﻿using System.Linq;
using AutoMapper;
using ExadelPractice.DTOs;
using ExadelPractice.Models;

namespace ExadelPractice.Automapper
{
    public static class PositionMapper
    {
        public static void Configure(IMapperConfigurationExpression config)
        {
            PositionToReducedPositionDto(config);
            PositionToPositionDto(config);
            PositionDtoToPosition(config);
            PositionStatusMapping(config);
            PositionToLucenePosition(config);
        }

        private static void PositionToLucenePosition(IMapperConfigurationExpression config)
        {
            config.CreateMap<Position, LucenePosition>();
        }

        private static void PositionStatusMapping(IMapperConfigurationExpression config)
        {
            config.CreateMap<PositionStatus, PositionStatusDTO>();
            config.CreateMap<PositionStatusDTO, PositionStatus>();
        }

        private static void PositionToReducedPositionDto(IMapperConfigurationExpression config)
        {
            config.CreateMap<Position, ReducedPositionDTO>()
                .ForMember(dest => dest.Skills, opt => opt.MapFrom(src => src.Skills.Select(
                    e => new PositionSkillDTO
                    {
                        Id = e.SkillId,
                        IsPrimary = e.IsPrimary,
                        Level = e.Level,
                        Name = e.Skill.Name,
                        ParentId = e.Skill.ParentId,
                    })))
                .ForMember(dest => dest.City, opt => opt.MapFrom(src =>
                    new PositionCityDTO
                    {
                        Id = src.PositionCity.CityId,
                        Name = src.PositionCity.City.Name,
                    }))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src =>
                    new PositionStatusDTO
                    {
                        IsActive = src.PositionStatusLayer.PositionStatus.IsActive,
                        Name = src.PositionStatusLayer.PositionStatus.Name,
                        Id = src.PositionStatusLayer.PositionStatusId
                    }))
                .ForMember(dest => dest.Profession, opt => opt.MapFrom(src =>
                    new PositionProfessionDTO
                    {
                        Name = src.PositionProfession.Profession.Name,
                        Id = src.PositionProfession.ProfessionId
                    }))
                .ForMember(dest => dest.ProjectName,
                    opt => opt.MapFrom(
                        src => src.PositionProject.Project.Name))
                .ForMember(dest => dest.EnglishLevel,
                    opt => opt.MapFrom(
                        src => src.EnglishLevel));
        }

        private static void PositionToPositionDto(IMapperConfigurationExpression config)
        {
            config.CreateMap<Position, PositionDTO>()
                .ForMember(dest => dest.Skills, opt => opt.MapFrom(src => src.Skills.Select(
                    e => new PositionSkillDTO
                    {
                        Id = e.SkillId,
                        IsPrimary = e.IsPrimary,
                        Level = e.Level,
                        Name = e.Skill.Name,
                        ParentId = e.Skill.ParentId,
                    })))
                .ForMember(dest => dest.Project, opt => opt.MapFrom(src =>
                    new PositionProjectDTO
                    {
                        Id = src.PositionProject.ProjectId,
                        Name = src.PositionProject.Project.Name
                    }))
                .ForMember(dest => dest.EnglishLevel,
                    opt => opt.MapFrom(
                        src => src.EnglishLevel))
                .ForMember(dest => dest.City, opt => opt.MapFrom(src =>
                    new PositionCityDTO
                    {
                        Id = src.PositionCity.CityId,
                        Name = src.PositionCity.City.Name,
                    }))
                .ForMember(dest => dest.CandidatePositions, opt => opt.MapFrom(src => src.CandidatePositions.Select(
                    e => new CandidatePositionDTO
                    {
                        CandidateId = e.CandidateId,
                        PositionId = e.PositionId
                    })))
                .ForMember(dest => dest.PositionInterviews, opt => opt.MapFrom(src => src.PositionInterviews.Select(
                    e => new PositionInterviewDTO
                    {
                        Id = e.Id,
                        PositionId = e.PositionId
                    })))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src =>
                    new PositionStatusDTO
                    {
                        IsActive = src.PositionStatusLayer.PositionStatus.IsActive,
                        Name = src.PositionStatusLayer.PositionStatus.Name,
                        Id = src.PositionStatusLayer.PositionStatusId
                    }))
                .ForMember(dest => dest.Profession, opt => opt.MapFrom(src =>
                    new PositionProfessionDTO
                    {
                        Name = src.PositionProfession.Profession.Name,
                        Id = src.PositionProfession.ProfessionId
                    }));
        }

        private static void PositionDtoToPosition(IMapperConfigurationExpression config)
        {
            config.CreateMap<PositionDTO, Position>()
                .ForMember(dest => dest.EnglishLevel,
                    opt => opt.MapFrom(
                        src => src.EnglishLevel))
                .ForMember(dest => dest.PositionProject, opt => opt.MapFrom(src =>
                    new PositionProject
                    {
                        ProjectId = src.Project.Id,
                        PositionId = src.Id
                    }))
                .ForMember(dest => dest.PositionCity, opt => opt.MapFrom(src =>
                    new PositionCity
                    {
                        CityId = src.City.Id,
                        Id = src.Id
                    }))
                .ForMember(dest => dest.Skills, opt => opt.MapFrom(src => src.Skills.Select(e =>
                    new PositionSkill
                    {
                        IsPrimary = e.IsPrimary,
                        Level = e.Level,
                        SkillId = e.Id,
                        PositionId = src.Id
                    })))
                .ForMember(dest => dest.CandidatePositions, opt => opt.MapFrom(src => src.CandidatePositions.Select(
                    e => new CandidatePosition
                    {
                        CandidateId = e.CandidateId,
                        PositionId = e.PositionId
                    })))
                .ForMember(dest => dest.PositionInterviews, opt => opt.MapFrom(src => src.PositionInterviews.Select(
                    e => new PositionInterview
                    {
                        Id = e.Id,
                        PositionId = e.PositionId
                    })))
                .ForMember(dest => dest.PositionProfession, opt => opt.MapFrom(src =>
                    new PositionProfession
                    {
                        ProfessionId = src.Profession.Id,

                    }))
                .ForMember(dest => dest.PositionStatusLayer, opt => opt.MapFrom(src =>
                    new PositionStatusLayer
                    {
                        PositionStatusId = src.Status.Id,
                        Id = src.Id
                    }));
        }
    }
}