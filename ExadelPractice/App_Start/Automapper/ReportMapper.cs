﻿using System.Linq;
using AutoMapper;
using ExadelPractice.DTOs;

namespace ExadelPractice.Automapper
{
    public static class ReportMapper
    {
        public static void Configure(IMapperConfigurationExpression config)
        {
            CandidateDtoToReport(config);
        }

        private static void CandidateDtoToReport(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<CandidateDTO, ReportDTO>()
                .ForMember(dest => dest.Status,
                    opt => opt.MapFrom(
                        src => src.Status.Name))
                .ForMember(dest => dest.Profession,
                    opt => opt.MapFrom(
                        src => src.Profession.Name))
                .ForMember(dest => dest.City,
                    opt => opt.MapFrom(
                        src => src.City.Name))
                .ForMember(dest => dest.Name,
                    opt => opt.MapFrom(
                        src => src.NameRus))
                .ForMember(dest => dest.Surname,
                    opt => opt.MapFrom(
                        src => src.SurnameRus))
                .ForMember(dest => dest.Patronymic,
                    opt => opt.MapFrom(
                        src => src.PatronymicRus))
                .ForMember(dest => dest.PrimaryEmail,
                    opt => opt.MapFrom(
                        src => src.Email.FirstOrDefault().Email))
                .ForMember(dest => dest.Skills,
                    opt => opt.MapFrom(
                        src => src.Skills.Select(e => e.Name)));
        }
    }
}