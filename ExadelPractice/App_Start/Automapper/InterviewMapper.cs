﻿using System.Linq;
using AutoMapper;
using ExadelPractice.DTOs;
using ExadelPractice.Models;

namespace ExadelPractice.Automapper
{
    public static class InterviewMapper
    {
        public static void Configure(IMapperConfigurationExpression config)
        {
            InterviewDtoToInterview(config);
            InterviewToInterviewDto(config);
        }

        private static void InterviewToInterviewDto(IMapperConfigurationExpression config)
        {
            config.CreateMap<Interview, InterviewDTO>()
                .ForMember(dest => dest.Candidate, opt => opt.MapFrom(src =>
                    new CandidateInterviewDTO
                    {
                        Id = src.CandidateInterview.CandidateId,
                        NameRus = src.CandidateInterview.Candidate.NameRus,
                        SurnameRus = src.CandidateInterview.Candidate.SurnameRus,
                        PatronymicRus = src.CandidateInterview.Candidate.PatronymicRus,
                        NameEng = src.CandidateInterview.Candidate.NameEng,
                        SurnameEng = src.CandidateInterview.Candidate.SurnameEng
                    }))
                .ForMember(dest => dest.Skills, opt => opt.MapFrom(src => src.InterviewSkills.Select(
                    e => new InterviewSkillDTO
                    {
                        SkillId = e.SkillId,
                        SkillName = e.Skill.Name,
                        DefultRating = e.DefultRating,
                        ConfRating = e.ConfRating,
                        Experience = e.Experience,
                        IsPrimary = e.IsPrimary,
                        WasConfirmed = e.WasConfirmed
                    })));
        }

        private static void InterviewDtoToInterview(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<InterviewDTO, Interview>()
                .ForMember(dest => dest.CandidateInterview, opt => opt.MapFrom(src =>
                    new CandidateInterview
                    {
                        InterviewId = src.Id,
                        CandidateId = src.Candidate.Id

                    }))
                .ForMember(dest => dest.InterviewSkills, opt => opt.MapFrom(src => src.Skills.Select(
                    e => new InterviewSkill
                    {
                        SkillId = e.SkillId,
                        DefultRating = e.DefultRating,
                        ConfRating = e.ConfRating,
                        Experience = e.Experience,
                        IsPrimary = e.IsPrimary,
                        WasConfirmed = e.WasConfirmed
                    })));
        }
    }
}