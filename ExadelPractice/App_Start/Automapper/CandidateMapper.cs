﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using ExadelPractice.DTOs;
using ExadelPractice.Models;

namespace ExadelPractice.Automapper
{
    public static class CandidateMapper
    {
        public static void Configure(IMapperConfigurationExpression config)
        {
            CandidateToCandidateDto(config);
            CandidateDtoToCandidate(config);
            CandidateToLuceneCandidate(config);
            CandidateToReducedCandidateDto(config);
            CandidateDtoToReducedCandidateDto(config);
            CandidateSkillDtoToCandidateSkill(config);
        }

        private static void CandidateSkillDtoToCandidateSkill(IMapperConfigurationExpression config)
        {
            config.CreateMap<CandidateSkillDTO, CandidateSkill>();
        }

        private static void CandidateToCandidateDto(IMapperConfigurationExpression config)
        {
            config.CreateMap<Candidate, CandidateDTO>()
                .ForMember(dest => dest.Skills, opt => opt.MapFrom(src => src.CandidateSkills.Select(
                    e => new CandidateSkillDTO
                    {
                        Experience = e.Experience,
                        IsPrimary = e.IsPrimary,
                        LastUsage = e.LastUsage,
                        Rating = e.Rating,
                        Id = e.SkillId,
                        WasConfirmed = e.WasConfirmed,
                        Name = e.Skill.Name
                    })))
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email.Select(
                    e => new EmailDTO
                    {
                        Email = e.Email,
                        Id = e.Id,
                        IsPrimary = e.IsPrimary
                    })))
                .ForMember(dest => dest.Profession, opt => opt.MapFrom(src =>
                    new ProfessionDTO
                    {
                        Id = src.CandidateProfession.ProfessionId,
                        Name = src.CandidateProfession.Profession.Name
                    }))
                .ForMember(dest => dest.Positions, opt => opt.MapFrom(src => src.CandidatePositions.Select(
                    e => new PositionDTO
                    {
                        Description = e.Position.Description,
                        Experience = e.Position.Experience,
                        Id = e.PositionId,
                        Header = e.Position.Header,
                        Skills = Mapper.Map<ICollection<PositionSkill>, ICollection<PositionSkillDTO>>(e.Position
                            .Skills)
                    })))
                .ForMember(dest => dest.Interviews, opt => opt.MapFrom(src => src.CandidateInterviews.Select(
                    e => new InterviewDTO
                    {
                        Id = e.Interview.Id,
                        Type = e.Interview.Type,
                        Date = e.Interview.Date,
                        Status = e.Interview.Status,
                        Header = e.Interview.Header,
                        Description = e.Interview.Description,
                    })))
                .ForMember(dest => dest.City, opt => opt.MapFrom(src =>
                    new CandidateCityDTO
                    {
                        Id = src.CandidateCity.CityId,
                        Name = src.CandidateCity.City.Name,
                        CandidateCityId = src.CandidateCity.Id
                    }))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src =>
                    new CandidateStatusDTO
                    {
                        Id = src.CandidateStatusLayer.CandidateStatus.Id,
                        Name = src.CandidateStatusLayer.CandidateStatus.Name,
                        IsActive = src.CandidateStatusLayer.CandidateStatus.IsActive
                    }));
        }

        private static void CandidateDtoToCandidate(IMapperConfigurationExpression config)
        {
            config.CreateMap<CandidateDTO, Candidate>()
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email.Select(
                    e => new CandidateEmail
                    {
                        Email = e.Email,
                        Id = e.Id,
                        IsPrimary = e.IsPrimary,
                        CandidateId = src.Id
                    })))
                .ForMember(dest => dest.CandidateCity, opt => opt.MapFrom(src =>
                    new CandidateCity
                    {
                        CityId = src.City.Id,
                        Id = src.City.CandidateCityId
                    }))
                .ForMember(dest => dest.CandidateProfession, opt => opt.MapFrom(src =>
                    new CandidateProfession
                    {
                        Id = src.Id,
                        ProfessionId = src.Profession.Id
                    }))
                .ForMember(dest => dest.CandidateSkills, opt => opt.MapFrom(src => src.Skills.Select(
                    e => new CandidateSkill
                    {
                        CandidateId = src.Id,
                        SkillId = e.Id,
                        LastUsage = e.LastUsage,
                        IsPrimary = e.IsPrimary,
                        Experience = e.Experience,
                        Rating = e.Rating,
                        WasConfirmed = e.WasConfirmed
                    })))
                .ForMember(dest => dest.CandidatePositions, opt => opt.MapFrom(src => src.Positions.Select(
                    e => new CandidatePosition
                    {
                        CandidateId = src.Id,
                        PositionId = e.Id
                    })))
                .ForMember(dest => dest.CandidateInterviews, opt => opt.MapFrom(src => src.Interviews.Select(
                    e => new CandidateInterview
                    {
                        CandidateId = src.Id,
                        InterviewId = e.Id
                    })))
                .ForMember(dest => dest.CandidateStatusLayer, opt => opt.MapFrom(src =>
                    new CandidateStatusLayer
                    {
                        Id = src.Id,
                        CandidateStatusId = src.Status.Id
                    }));
        }

        private static void CandidateToLuceneCandidate(IMapperConfigurationExpression config)
        {
            config.CreateMap<Candidate, LuceneCandidate>();
        }

        private static void CandidateToReducedCandidateDto(IMapperConfigurationExpression config)
        {
            config.CreateMap<Candidate, ReducedCandidateDTO>()
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src =>
                    new CandidateStatusDTO
                    {
                        Id = src.CandidateStatusLayer.CandidateStatus.Id,
                        Name = src.CandidateStatusLayer.CandidateStatus.Name,
                        IsActive = src.CandidateStatusLayer.CandidateStatus.IsActive
                    }))
                .ForMember(dest => dest.City, opt => opt.MapFrom(e =>
                    new CandidateCityDTO
                    {
                        Id = e.CandidateCity.CityId,
                        Name = e.CandidateCity.City.Name,
                        CandidateCityId = e.CandidateCity.Id
                    }))
                .ForMember(dest => dest.Profession, opt => opt.MapFrom(src =>
                    new ProfessionDTO
                    {
                        Id = src.CandidateProfession.ProfessionId,
                        Name = src.CandidateProfession.Profession.Name
                    }))
                .ForMember(dest => dest.Skills, opt => opt.MapFrom(src => src.CandidateSkills.Select(
                    e => new CandidateSkillDTO
                    {
                        Experience = e.Experience,
                        IsPrimary = e.IsPrimary,
                        LastUsage = e.LastUsage,
                        Rating = e.Rating,
                        Id = e.SkillId,
                        WasConfirmed = e.WasConfirmed,
                        Name = e.Skill.Name
                    })));
        }

        private static void CandidateDtoToReducedCandidateDto(IMapperConfigurationExpression config)
        {
            config.CreateMap<CandidateDTO, ReducedCandidateDTO>()
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src =>
                    new CandidateStatusDTO
                    {
                        Id = src.Status.Id,
                        IsActive = src.Status.IsActive,
                        Name = src.Status.Name
                    }))
                .ForMember(dest => dest.City, opt => opt.MapFrom(src =>
                    new CandidateCityDTO
                    {
                        Id = src.City.Id,
                        Name = src.City.Name,
                        CandidateCityId = src.City.CandidateCityId
                    }))
                .ForMember(dest => dest.Profession, opt => opt.MapFrom(src =>
                    new ProfessionDTO
                    {
                        Id = src.Profession.Id,
                        Name = src.Profession.Name
                    }))
                .ForMember(dest => dest.Skills, opt => opt.MapFrom(src => src.Skills.Select(
                    e => new CandidateSkillDTO
                    {
                        Experience = e.Experience,
                        IsPrimary = e.IsPrimary,
                        LastUsage = e.LastUsage,
                        Rating = e.Rating,
                        Id = e.Id,
                        WasConfirmed = e.WasConfirmed,
                        Name = e.Name
                    })));
        }
    }
}