﻿using AutoMapper;
using ExadelPractice.DTOs;
using ExadelPractice.Models;

namespace ExadelPractice.Automapper
{
    public static class UserMapper
    {
        public static void Configure(IMapperConfigurationExpression config)
        {
            UserToUserDto(config);
        }

        private static void UserToUserDto(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<User, UserDTO>()
                .ForMember(dest => dest.Email,
                    opt => opt.MapFrom(
                        src => src.UserName));
        }
    }
}