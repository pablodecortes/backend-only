﻿using System.Web.Http;
using System.Web.Http.Cors;

namespace ExadelPractice
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            EnableCors(config);

            AddRoutes(config);
        }

        private static void EnableCors(HttpConfiguration config)
        {
            var corsAttributes = new EnableCorsAttribute(AppConfig.GetCorsOrigin(), "*", "*")
            {
                SupportsCredentials = true
            };
            config.EnableCors(corsAttributes);
        }

        private static void AddRoutes(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();
            config.Routes.MapHttpRoute(
                "DefaultApi",
                "api/{controller}/{id}",
                new { id = RouteParameter.Optional }
            );
        }
    }
}
