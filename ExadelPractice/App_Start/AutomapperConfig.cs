﻿using AutoMapper;
using ExadelPractice.Automapper;

namespace ExadelPractice
{
    public static class AutomapperConfig
    {
        public static void Register()
        {
            Mapper.Initialize(config =>
            {
                UserMapper.Configure(config);
                CandidateMapper.Configure(config);
                ReportMapper.Configure(config);
                InterviewMapper.Configure(config);
                PositionMapper.Configure(config);
            });
        }
    }
}