﻿using ExadelPractice.Filters.AuthorizationFilters;
using ExadelPractice.SignalR;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Newtonsoft.Json;
using Owin;

namespace ExadelPractice
{
    public static class SignalRConfig
    {
        public static void Register(IAppBuilder app)
        {
            app.Map("/signalr", map =>
            {
                var hubConfiguration = new HubConfiguration
                {
                    EnableJavaScriptProxies = true,
                    EnableJSONP = true,
                    EnableDetailedErrors = true
                };

                ConfigureCamelCaseResolver();

                ConfigureAuthorization();

                //GlobalHost.Configuration.KeepAlive = null;

                map.RunSignalR(hubConfiguration);
            });
        }

        private static void ConfigureCamelCaseResolver()
        {
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new SignalRContractResolver()
            };
            var serializer = JsonSerializer.Create(settings);
            GlobalHost.DependencyResolver.Register(typeof(JsonSerializer), () => serializer);
        }

        private static void ConfigureAuthorization()
        {
            var authorizer = new QueryStringBearerAuthorizeAttribute();
            var module = new AuthorizeModule(authorizer, authorizer);
            GlobalHost.HubPipeline.AddModule(module);
        }
    }
}