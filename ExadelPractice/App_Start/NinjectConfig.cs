﻿using System.Web.Http;
using ExadelPractice.NinjectModules;
using Ninject;
using Ninject.Modules;
using Ninject.Web.Common.OwinHost;
using Ninject.Web.WebApi.OwinHost;
using Owin;

namespace ExadelPractice
{
    public static class NinjectConfig
    {
        public static void Register(IAppBuilder app, HttpConfiguration config, IKernel kernel)
        {
            app.UseNinjectMiddleware(() => kernel).UseNinjectWebApi(config);
        }

        public static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();

            kernel.Load(new INinjectModule[]
            {
                new ProviderNinjectModule(),
                new RepositoryNinjectModule(),
                new ServiceNinjectModule(),
                new ValidatorNinjectModule(),
                new ScopeNinjectModule(),
                new StorageNinjectModule(), 
                new HubNinjectModule(), 
                new FactoryNinjectModule(), 
            });

            return kernel;
        }
    }
}