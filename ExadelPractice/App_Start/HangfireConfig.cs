﻿using System.Linq;
using Hangfire;
using Hangfire.Dashboard;
using Ninject;
using Owin;

namespace ExadelPractice
{
    public static class HangfireConfig
    {
        public static void Register(IAppBuilder app, IKernel kernel)
        {
            var connectionString = AppConfig.GetConnectionString();
            GlobalConfiguration.Configuration.UseSqlServerStorage(connectionString);
            GlobalConfiguration.Configuration.UseNinjectActivator(kernel);

            var options = GetDashboardOptions(kernel);
            app.UseHangfireDashboard("/jobs", options);

            ConfigureHangfireServer(app);
        }

        private static DashboardOptions GetDashboardOptions(IKernel kernel)
        {
            var options = new DashboardOptions
            {
                Authorization = Enumerable.Empty<IDashboardAuthorizationFilter>(), // todo : insert "new[] { kernel.Get<HangfireAuthorizationFilter>()  }"
                AppPath = AppConfig.GetCorsOrigin()
            };
            return options;
        }

        private static void ConfigureHangfireServer(IAppBuilder app)
        {
            app.UseHangfireServer(new BackgroundJobServerOptions
            {
                HeartbeatInterval = new System.TimeSpan(0, 1, 0),
                ServerCheckInterval = new System.TimeSpan(0, 1, 0),
                SchedulePollingInterval = new System.TimeSpan(0, 1, 0)
            });
        }
    }
}