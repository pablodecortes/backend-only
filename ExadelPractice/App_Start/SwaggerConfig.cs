using System.Web.Http;
using Swashbuckle.Application;

namespace ExadelPractice
{
    public class SwaggerConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config
                .EnableSwagger(c =>
                {
                    c.SingleApiVersion("v1", "ExadelPractice");
                })
                .EnableSwaggerUi(c =>
                {
                });
        }
    }
}
