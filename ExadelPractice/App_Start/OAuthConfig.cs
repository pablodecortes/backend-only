﻿using System;
using ExadelPractice.Providers;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Ninject;
using Owin;

namespace ExadelPractice
{
    public static class OAuthConfig
    {
        public static OAuthAuthorizationServerOptions AuthServerOptions;

        static OAuthConfig()
        {
            AuthServerOptions = new OAuthAuthorizationServerOptions
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(365)
            };
        }

        public static void Register(IAppBuilder app, IKernel kernel)
        {
            AuthServerOptions.Provider = kernel.Get<SimpleAuthorizationServerProvider>();

            var authOptions = new OAuthBearerAuthenticationOptions();

            app.UseOAuthAuthorizationServer(AuthServerOptions);
            app.UseOAuthBearerAuthentication(authOptions);
        }
    }
}