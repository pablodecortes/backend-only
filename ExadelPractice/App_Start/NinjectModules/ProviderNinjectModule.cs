﻿using ExadelPractice.Providers;
using Microsoft.Owin.Security.OAuth;
using Ninject.Modules;

namespace ExadelPractice.NinjectModules
{
    public class ProviderNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IOAuthAuthorizationServerProvider>().To<SimpleAuthorizationServerProvider>();
        }
    }
}