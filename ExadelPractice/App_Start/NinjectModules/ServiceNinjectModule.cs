﻿using ExadelPractice.Abstract.Services;
using ExadelPractice.Implements.Services;
using ExadelPractice.Libraries;
using Ninject.Modules;

namespace ExadelPractice.NinjectModules
{
    public class ServiceNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IPositionService>().To<PositionService>();
            Bind<ICandidateService>().To<CandidateService>();
            Bind<ICalendarEventService>().To<CalendarEventService>();
            Bind<ICityService>().To<CityService>();
            Bind<IHistoryService>().To<HistoryService>();
            Bind<IProfessionService>().To<ProfessionService>();
            Bind<ISkillsService>().To<SkillService>();
            Bind<ICandidateStatusService>().To<CandidateStatusService>();
            Bind<ISkillsTreeable>().To<SkillTreeBuilderService>();
            Bind<IReportService>().To<ReportSevice>();
            Bind<IPositionStatusService>().To<PositionStatusService>();
            Bind<IExcelService>().To<ExcelService>();
            Bind<IPushNotificationService>().To<PushNotificationService>();
            Bind<IProjectService>().To<ProjectService>();
            Bind<IResumeService>().To<ResumeService>();
            Bind<IUserService>().To<UserService>();
            Bind<IAuthService>().To<AuthService>();
            Bind<IInterviewService>().To<InterviewService>();
            Bind<ICandidateUniqueVerification>().To<CandidateUniqueVerification>();
            Bind<ICandidateFilterService>().To<CandidateFilterService>();
            Bind<IPositionSearch>().To<LucenePositionSearch>();
            Bind<IPositionFilterService>().To<PositionFilterService>();
            Bind<IGoogleAuthorizationService>().To<GoogleAuthorizationService>();
        }
    }
}