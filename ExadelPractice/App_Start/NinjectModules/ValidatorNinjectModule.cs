﻿using ExadelPractice.DTOs;
using ExadelPractice.Filters.AuthorizationFilters;
using ExadelPractice.Filters.ControllersFilters;
using ExadelPractice.Implements.Validators;
using ExadelPractice.Validators;
using FluentValidation;
using Hangfire.Dashboard;
using Ninject.Modules;

namespace ExadelPractice.NinjectModules
{
    public class ValidatorNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IValidator<CandidateFilter>>().To<CandidateFilterValidator>();
            Bind<IValidator<PositionsFilter>>().To<PositionsFilterValidator>();
            Bind<IValidator<CandidateDTO>>().To<CandidateDTOValidator>();
            Bind<IValidator<PositionDTO>>().To<PositionDTOValidator>();
            Bind<IValidator<NotificationFilter>>().To<NotificationValidator>();
            Bind<IValidator<InterviewDTO>>().To<InterviewDTOValidator>();
            Bind<IDashboardAuthorizationFilter>().To<HangfireAuthorizationFilter>();
        }
    }
}