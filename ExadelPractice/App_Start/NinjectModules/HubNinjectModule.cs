﻿using ExadelPractice.Abstract.Hubs;
using ExadelPractice.Implements.Hubs;
using Ninject.Modules;

namespace ExadelPractice.NinjectModules
{
    public class HubNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IPushNotificationHub>().To<PushNotificationHub>();
        }
    }
}