﻿using ExadelPractice.Abstract.Factories;
using ExadelPractice.Implements.Factories;
using Ninject.Modules;

namespace ExadelPractice.NinjectModules
{
    public class FactoryNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Bind<INotificationFactory>().To<NotificationFactory>();
        }
    }
}