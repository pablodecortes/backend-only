﻿using ExadelPractice.Abstract.Services;
using ExadelPractice.Backgrounds;
using ExadelPractice.Implements.Services;
using ExadelPractice.Libraries;
using ExadelPractice.Models;
using Hangfire;
using Ninject.Modules;

namespace ExadelPractice.NinjectModules
{
    public class ScopeNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Bind<ExadelPracticeContext>().ToSelf().InTransientScope();
            Bind<IEmailService>().To<EmailService>().InBackgroundJobScope();
            Bind<LuceneCandidateSearch>().ToSelf().InSingletonScope();
            Bind<LuceneIndexer>().ToSelf().InSingletonScope();
            Bind<INotificationService>().To<NotificationService>().InBackgroundJobScope();
        }
    }
}