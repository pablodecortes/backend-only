﻿using ExadelPractice.Implements.Storages;
using Google.Apis.Util.Store;
using Ninject.Modules;

namespace ExadelPractice.NinjectModules
{
    public class StorageNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IDataStore>().To<DbStore>();
        }
    }
}