﻿using ExadelPractice.Abstract.Repositories;
using ExadelPractice.Implements.Repositories;
using Ninject.Modules;

namespace ExadelPractice.NinjectModules
{
    public class RepositoryNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IPositionRepository>().To<PositionRepository>();
            Bind<IAuthRepository>().To<AuthRepository>();
            Bind<ICandidateRepository>().To<CandidateRepository>();
            Bind<IHistoryRepository>().To<HistoryRepository>();
            Bind<ICityRepository>().To<CityRepository>();
            Bind<IProjectRepositoty>().To<ProjectRepository>();
            Bind<IProfessionRepository>().To<ProfessionRepository>();
            Bind<ISkillsRepository>().To<SkillsRepository>();
            Bind<ICandidateStatusRepository>().To<CandidateStatusRepository>();
            Bind<IPositionStatusRepository>().To<PositionStatusRepository>();
            Bind<IResumeRepository>().To<ResumeRepository>();
            Bind<IUserRepository>().To<UserRepository>();
            Bind<INotificationRepository>().To<NotificationRepository>();
            Bind<IInterviewRepository>().To<InterviewRepository>();
            Bind<IHistoryCacheRepository>().To<HistoryCacheRepository>();
            Bind<IDbStoreRepository>().To<DbStoreRepository>();
        }
    }
}