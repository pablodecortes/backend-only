﻿using System.Web.Configuration;

namespace ExadelPractice
{
    public static class AppConfig
    {
        public static string GetConnectionString()
        {
            var connectionString = WebConfigurationManager.ConnectionStrings["ExadelPracticeContext"].ConnectionString;

            return connectionString;
        }

        public static string GetGmailName()
        {
            var gmailName = WebConfigurationManager.AppSettings["GmailName"];

            return gmailName;
        }

        public static string GetGmailPass()
        {
            var gmailPass = WebConfigurationManager.AppSettings["GmailPass"];

            return gmailPass;
        }

        public static string GetGoogleId()
        {
            var googleId = WebConfigurationManager.AppSettings["GoogleID"];

            return googleId;
        }

        public static string GetGoogleSecret()
        {
            var googleSecret = WebConfigurationManager.AppSettings["GoogleSecret"];

            return googleSecret;
        }

        public static string GetCorsOrigin()
        {
            var corsOrigin = WebConfigurationManager.AppSettings["CorsOrigin"];

            return corsOrigin;
        }
    }
}